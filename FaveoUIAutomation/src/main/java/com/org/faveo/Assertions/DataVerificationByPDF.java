package com.org.faveo.Assertions;

import org.openqa.selenium.By;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class DataVerificationByPDF extends BaseClass implements AccountnSettingsInterface {

	public static void selectViewPropsal(){
		clickElement(By.xpath(viewProposalsDashborad_Xpath));
		System.out.println("View Proposals Opened");
		logger.log(LogStatus.PASS, "View Proposals Opened");
	}
	public static void setFromDate()throws Exception{
		Thread.sleep(3000);
		clickElement(By.xpath(fromDateViewPropsal_Xpath));
		enterText(By.xpath(fromDateViewPropsal_Xpath), String.valueOf("27/11/2018"));
		//System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup1);
		//logger.log(LogStatus.PASS, "Data Entered for Isnured 1 DOB : " + InsuredDOB_OfAgeGroup1);
	}
	
	public static void setToDate()throws Exception{
		waitForElement(By.xpath(toDateViewPropsal_Xpath));
		clickElement(By.xpath(toDateViewPropsal_Xpath));
		enterText(By.xpath(toDateViewPropsal_Xpath), String.valueOf("27/11/2018"));
		//System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup1);
		//logger.log(LogStatus.PASS, "Data Entered for Isnured 1 DOB : " + InsuredDOB_OfAgeGroup1);
	}
	
	public static void searchByType(){
		clickElement(By.xpath("//div[contains(@class,'tax_description_drop_search_body padding0')]//ul[contains(@class,'month_year_by')]//preceding-sibling::a"));
		
	}
	
	
	
	public static void searchPolicyDetails() throws Exception{
		selectViewPropsal();
		setFromDate();
		setToDate();
	}
	
	//Function for verify Policy Details in PDF
	public static void verifyPolicyDetailsInPdf() throws Exception{
		searchPolicyDetails();
	}
	
}
