package com.org.faveo.Assertions;

import java.io.UnsupportedEncodingException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class QuotationandProposalVerification extends BaseClass implements AccountnSettingsInterface
{

	private static String ProposalPremimPage_Value=null;
	private static String proposalSummarypremium_value=null;
	
	public static void PremiumAssertion()
	{

		try
		
		{
			
		// Verify TotalMembers from Excel and Quotation Page
		String TotalMemberPresentonQuotation = driver.findElement(By.xpath(TotalMemberPresentQuotaion_CareHNI_xpath)).getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);

		// Capture The Premium value
		ExplicitWait(By.xpath(Quotationpage_premium_xpath), 5);
		Thread.sleep(4000);
		String Quotationpremium_value = driver.findElement(By.xpath(Quotationpage_premium_xpath)).getText();
		System.out.println("Total Premium Value on Quotation Page is :" + " - "+ Quotationpremium_value.substring(1, Quotationpremium_value.length()));
		
		// Click on BuyNow Button
		clickElement(By.xpath(Button_Buynow_xpath));
		

		// Premium verification on Proposal Page
		/*Fluentwait(By.xpath("//p[@class='amount ng-binding']"), 60, "Unable to read Premium on Proposal Page.");*/
		ImplicitWait(20);
		String Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		ProposalPremimPage_Value = Proposalpremium_value.substring(1, Proposalpremium_value.length());
		System.out.println("Total Premium Value on Proposal Page is : " + " - "+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
	try {
			Assert.assertEquals(Quotationpremium_value, Proposalpremium_value);
			logger.log(LogStatus.INFO,"Quotation Premium and Proposal Premium is Verified and Both are Same : "+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
		} catch (AssertionError e) {
			System.out.println(Quotationpremium_value + " - failed");
			logger.log(LogStatus.FAIL, "Quotation Premium and Proposal Premium are not Same");
			
		}

		
		// Total Number of Member on Quotation and Proposal Page
		String TotalMemberProposal = driver.findElement(By.xpath(TotalMemberProposal_xpath)).getText();
		System.out.println("Total Members on Proposal Page : " + TotalMemberProposal);
		try {
			Assert.assertEquals(TotalMemberPresentonQuotation, TotalMemberProposal);
			logger.log(LogStatus.INFO,
					"Number Of Members Verified on Quotation and ProposalPage Both are Same : "
							+ TotalMemberProposal);
		} catch (AssertionError e) {
			logger.log(LogStatus.INFO, "Number Of Members are diffrent on Quotation and ProposalPage");
		}

		
		
	}catch(Exception e)
		{
		System.out.println(e);
		}
	}
	
	public static void VerifyPremiumIncrease_on_Proposalsummarypage() throws UnsupportedEncodingException, InterruptedException
	{
		
		proposalSummarypremium_value = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[@class='premium_amount ng-binding']"))).getText();
		System.out.println("Total premium value is: " + proposalSummarypremium_value);
		
		String PropPremium = ProposalPremimPage_Value;
		PropPremium = PropPremium.replace(",", "");
		System.out.println("PropPremium is : "+PropPremium);
		
		String PropSum = proposalSummarypremium_value;
		PropSum = PropSum.replace(",", "");
		System.out.println("PropSum is : "+PropSum);
	      
		 double a = Double.valueOf(PropPremium);
		 System.out.println("Value of a is : "+a);
	     double b = Long.valueOf(PropSum);
	     System.out.println("Value of b is : "+b);
	     double x = a+10;
	     System.out.println("Value of x is : "+x);
	     double y = a-10;
	     System.out.println("Value of y is : "+y);
	     System.out.println(b>y && b<x);
	
	     Thread.sleep(2000);
	     if(a==b || (b>y && b<x))
			{
				logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);	
				System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
			}
	     else 
			{
				String PremiumV = driver.findElement(By.xpath("//p[@class='styl_p padding-tb ng-binding']")).getText();
				logger.log(LogStatus.FAIL, "Test case is fail because -"+PremiumV);		
				System.out.println("Test case is fail because -"+PremiumV);
			}
		
	}

	public static void ThankyouPagemessageVerification()
	{
		String expectedTitle = "Your payment transaction is successful !";
		Fluentwait(By.xpath(ExpectedMessage_xpath));
	     String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
			try{
				Assert.assertEquals(expectedTitle,actualTitle);
		          logger.log(LogStatus.INFO, actualTitle);
		     }catch(AssertionError e){
		          System.out.println("Payment Failed");
		          String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
		          logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
		          TestResult = "Fail";
		     }
	}
	
	public static void PremiumVerificationonThankyouPage()
	{
		String Thankyoupagepremium_value = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
		System.out.println("Total premium value is:" + Thankyoupagepremium_value);

		try {
			Assert.assertEquals(proposalSummarypremium_value, Thankyoupagepremium_value);
			logger.log(LogStatus.INFO,"Proposal Summuary Premium and Thankyou page Premium is Verified and Both are Same i.e : "+ Thankyoupagepremium_value.substring(0, Thankyoupagepremium_value.length()));
		} catch (AssertionError e) {
			System.out.println(proposalSummarypremium_value + " - failed");
			logger.log(LogStatus.FAIL, "Proposal Summuary Premium and Thankyou page Premium are not Same");
			throw e;
		}

	}
}
