package com.org.faveo.Assertions;

import org.openqa.selenium.By;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class ErroronPage extends BaseClass implements AccountnSettingsInterface
{

	public static void QuotationPageErrorCapturing()
	{
			//verify Email id Error 
			if(driver.findElement(By.xpath(Enter_ValidEmail_Error_Xpath)).isDisplayed())
			{
				String Error = driver.findElement(By.xpath(Enter_ValidEmail_Error_Xpath)).getText();
				logger.log(LogStatus.WARNING, "Please enter valid Email Id");
			}
	}
	
}
