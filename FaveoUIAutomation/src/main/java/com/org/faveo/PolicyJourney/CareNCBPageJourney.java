package com.org.faveo.PolicyJourney;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Assertions.DataVerificationinDb;
import com.org.faveo.Assertions.PdfMatch;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.PayUPaymentFn;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.HealthQuestions;
import com.org.faveo.model.InsuredDetails;
import com.org.faveo.model.ProposerDetailsPageFn;
import com.org.faveo.model.QuotationPage;
import com.org.faveo.utility.WriteExcel;

public class CareNCBPageJourney extends BaseClass implements AccountnSettingsInterface
{
	
	public static String ProposalPremimPage_Value=null;
	public static String proposalSummarypremium_value=null;
	
	@Test(priority=1)
	public static void QuotationPageJourney(int Rownum) throws Exception
	{
		// Reading Proposer Name from Excel
		QuotationPage.ProposerName("Care_Quotation_Data", Rownum, 2);
					

		// Reading Emailfrom Excel Sheet
		QuotationPage.ProposerEmail("Care_Quotation_Data", Rownum, 3);

		// Reading Mobile Number from Excel
		QuotationPage.ProposerMobile("Care_Quotation_Data", Rownum, 4);
					
		// Enter The Value of Total members present in policy
		QuotationPage.SelectMembers("Care_Quotation_Data", Rownum, 5);
					
		// again call the dropdown
		QuotationPage.SelectAgeofMember("Care_Quotation_Data", "Insured_Details", Rownum, 5, 6,7,8);
		
		// Read the Value of Suminsured
		QuotationPage.SumInsured("Care_Quotation_Data", Rownum, 9);
		
		// Reading the value of Tenure from Excel
		QuotationPage.Tenure("Care_Quotation_Data", Rownum, 10);
		
		// Scroll Window Up
		BaseClass.scrollup();

		// NCB Super Addon Selection
		AddonsforProducts.CareWithNCBAddons("Care_Quotation_Data", Rownum, 11);
		
		//Assertion 
		QuotationandProposalVerification.PremiumAssertion();

		
	}
	
	@Test(priority=2,dependsOnMethods = { "QuotationPageJourney" })
	public static void ProposalPageJourney(int Rownum) throws Exception
	{
				// Reading Proposer Title from Excel
				ProposerDetailsPageFn.ProposerTitle("Care_Quotation_Data", Rownum, 1);

				// Entering DOB from Excel into dob field
				ProposerDetailsPageFn.ProposerDOB("Care_Quotation_Data", Rownum, 12);
				
				
				// Reading AddressLine 1 from Excel
				ProposerDetailsPageFn.ProposerAddressLine1("Care_Quotation_Data", Rownum, 13);
				ProposerDetailsPageFn.ProposerAddressLine2("Care_Quotation_Data", Rownum, 14);
				
				
				// Reading Pincode from Excel
				ProposerDetailsPageFn.Pincode("Care_Quotation_Data", Rownum, 15);
				
				// Reading Proposer Height in Feet from Excel
				ProposerDetailsPageFn.ProposerHeightFeet("Care_Quotation_Data", Rownum, 16);
				
				// Reading Proposer Height in Inch from Excel
				ProposerDetailsPageFn.ProposerHeightInch("Care_Quotation_Data", Rownum, 17);
				
				// Reading Weight of Proposer from Excel
				ProposerDetailsPageFn.ProposerWeight("Care_Quotation_Data", Rownum, 18);
				
				// Reading Nominee Name from Excel
				ProposerDetailsPageFn.ProposerNominee("Care_Quotation_Data", Rownum, 19);
				

				// Nominee Relation
				ProposerDetailsPageFn.ProposerNomineeRelation("Care_Quotation_Data", Rownum, 20);

							
				// Pan Card
				ProposerDetailsPageFn.PanCardNumber("Care_Quotation_Data", Rownum, 21);
			
				// Click on Next button
				clickElement(By.id(Button_NextProposer_id));
				System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
				
				InsuredDetails.InsuredDetail("Insured_Details");
							
				//Health Questionnarire Elements 
				HealthQuestions.CareNcbHealthQuestions("Care_Quotation_Data", "Care_QuestionSet", Rownum, 22);
			
				BaseClass.ErroronHelathquestionnaire();
		
	}

	@Test(priority=3,dependsOnMethods = { "ProposalPageJourney" })
	public static void ProposalSummaryPage(String TestCasenameSheet,int Rownum) throws Exception
	{
		QuotationandProposalVerification.VerifyPremiumIncrease_on_Proposalsummarypage();
		try
		{
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);'][contains(text(),'PAY ONLINE')]"))).click();
		}
		catch(Exception e)
		{
		clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);'][contains(text(),'PAY ONLINE')]"));	
		}
		
		waitForElement(By.xpath(payu_proposalnum_xpath));
		String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
		System.out.println("Pay U Proposal Number : "+PayuProposalNum);
		
		waitForElement(By.xpath(payuAmount_xpath));
		String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
		String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
		System.out.println("Pay U Premium Amount : "+FinalAmount);
		
			PayUPaymentFn.PayuPage_Credentials();
			
			BaseClass.scrolldown();
			
			QuotationandProposalVerification.ThankyouPagemessageVerification();
			
			String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p")).getText();
			System.out.println(ProposerName);
			
			DataVerificationinDb.DBVerification(PayuProposalNum);
			waitForElement(By.xpath(PolProp_xpath));
			
			String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
			String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
			
			PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);

			//WriteExcel.setCellData1(TestCasenameSheet, TestResult, ThankyoupageProposal_Pol_num, Rownum, 2, 3);
			
			WriteExcel.setCellData(TestCasenameSheet, ThankyoupageProposal_Pol_num, Rownum, 2);
			Thread.sleep(5000);

	}
	
}
