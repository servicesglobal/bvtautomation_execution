package com.org.faveo.PolicyJourney;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.faveo.Assertions.DataVerificationinDb;
import com.org.faveo.Assertions.PdfMatch;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.HealthQuestions;
import com.org.faveo.model.InsuredDetails;
import com.org.faveo.model.PayUPaymentFn;
import com.org.faveo.model.ProposerDetailsPageFn;
import com.org.faveo.model.QuotationPage;
import com.org.faveo.utility.WriteExcel;

public class CareSuperSaverPageJourney extends BaseClass implements AccountnSettingsInterface
{

	public static String ProposalPremimPage_Value=null;
	public static String proposalSummarypremium_value=null;

	public static void QuotationPageJourney(int Rownum) throws Exception
	{
				
		// Reading Proposer Name from Excel
		QuotationPage.ProposerName("SuperSaver_Quotation", Rownum, 2);
					

		// Reading Emailfrom Excel Sheet
		QuotationPage.ProposerEmail("SuperSaver_Quotation", Rownum, 3);

		// Reading Mobile Number from Excel
		QuotationPage.ProposerMobile("SuperSaver_Quotation", Rownum, 4);
					
		// Enter The Value of Total members present in policy
		QuotationPage.SelectMembers("SuperSaver_Quotation", Rownum, 5);
					
		// again call the dropdown
		QuotationPage.SelectAgeofMember("SuperSaver_Quotation", "SuperSaver_Insured_Details", Rownum, 5, 6,7,8);
		
		// Read the Value of Suminsured
		QuotationPage.SumInsured("SuperSaver_Quotation", Rownum, 9);
		
		// Reading the value of Tenure from Excel
		QuotationPage.Tenure("SuperSaver_Quotation", Rownum, 10);
		
		// Scroll Window Up
		BaseClass.scrollup();

		// NCB Super Addon Selection
		AddonsforProducts.CareSuperSaverAddons("SuperSaver_Quotation", Rownum, 11);
		
		//Assertion 
		QuotationandProposalVerification.PremiumAssertion();
		
	}
	
	public static void ProposalPageJourney(int Rownum) throws Exception
	{
				// Reading Proposer Title from Excel
				ProposerDetailsPageFn.ProposerTitle("SuperSaver_Quotation", Rownum, 1);

				// Entering DOB from Excel into dob field
				ProposerDetailsPageFn.ProposerDOB("SuperSaver_Quotation", Rownum, 12);
				
				
				// Reading AddressLine 1 from Excel
				ProposerDetailsPageFn.ProposerAddressLine1("SuperSaver_Quotation", Rownum, 13);
				ProposerDetailsPageFn.ProposerAddressLine2("SuperSaver_Quotation", Rownum, 14);
				
				
				// Reading Pincode from Excel
				ProposerDetailsPageFn.Pincode("SuperSaver_Quotation", Rownum, 15);
				
				// Reading Proposer Height in Feet from Excel
				ProposerDetailsPageFn.ProposerHeightFeet("SuperSaver_Quotation", Rownum, 16);
				
				// Reading Proposer Height in Inch from Excel
				ProposerDetailsPageFn.ProposerHeightInch("SuperSaver_Quotation", Rownum, 17);
				
				// Reading Weight of Proposer from Excel
				ProposerDetailsPageFn.ProposerWeight("SuperSaver_Quotation", Rownum, 18);
				
				// Reading Nominee Name from Excel
				ProposerDetailsPageFn.ProposerNominee("SuperSaver_Quotation", Rownum, 19);
				

				// Nominee Relation
				ProposerDetailsPageFn.ProposerNomineeRelation("SuperSaver_Quotation", Rownum, 20);

							
				// Pan Card
				ProposerDetailsPageFn.PanCardNumber("SuperSaver_Quotation", Rownum, 21);
			
				// Click on Next button
				clickElement(By.id(Button_NextProposer_id));
				System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
				
				InsuredDetails.InsuredDetail("SuperSaver_Insured_Details");
							
				//Health Questionnarire Elements 
				HealthQuestions.CareSuperSaverHealthQuestions("SuperSaver_Quotation", "SuperSaver_QuestionSet", Rownum, 22);
			
				BaseClass.ErroronHelathquestionnaire();
				
	}

	public static void ProposalSummaryPage(String TestCasenameSheet,int Rownum) throws Exception
	{
		QuotationandProposalVerification.VerifyPremiumIncrease_on_Proposalsummarypage();
		try
		{
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);'][contains(text(),'PAY ONLINE')]"))).click();
		}
		catch(Exception e)
		{
		clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);'][contains(text(),'PAY ONLINE')]"));	
		}
		
		waitForElement(By.xpath(payu_proposalnum_xpath));
		String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
		System.out.println("Pay U Proposal Number : "+PayuProposalNum);
		
		waitForElement(By.xpath(payuAmount_xpath));
		String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
		String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
		System.out.println("Pay U Premium Amount : "+FinalAmount);
		
			PayUPaymentFn.PayuPage_Credentials();
			
			BaseClass.scrolldown();
			
			QuotationandProposalVerification.ThankyouPagemessageVerification();
			
			String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p")).getText();
			System.out.println(ProposerName);
			
			DataVerificationinDb.DBVerification(PayuProposalNum);
			waitForElement(By.xpath(PolProp_xpath));
			
			String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
			String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
			
			PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);

			WriteExcel.setCellData(TestCasenameSheet,  ThankyoupageProposal_Pol_num, Rownum, 2);
			
			
	}
	
}


