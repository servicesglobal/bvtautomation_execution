package com.org.faveo.Bvt.TestClasses;



import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.fixedbenefitinsurance.Assure;
import com.org.faveo.fixedbenefitinsurance.Secure;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.healthinsurance.CareFreedomPolicy;
import com.org.faveo.healthinsurance.CareGlobalPolicy;
import com.org.faveo.healthinsurance.CareHNIPolicy;
import com.org.faveo.healthinsurance.CareSenior;
import com.org.faveo.healthinsurance.CareSmartSelectPolicy;
import com.org.faveo.healthinsurance.CareWithNCB;
import com.org.faveo.healthinsurance.Enhance;
import com.org.faveo.healthinsurance.PosCareFreedomPolicy;
import com.org.faveo.healthinsurance.PosCareWithNcbPolicy;
import com.org.faveo.healthinsurance.SuperSaverPolicy;
import com.org.faveo.travelInsurance.Explore;
import com.org.faveo.utility.ReadExcel;

public class BvtMainClass extends BaseClass implements AccountnSettingsInterface 

{
	@org.testng.annotations.BeforeTest
public static void BeforeTest() {
	System.out.println("Execution Start for BVT");
}
	@Test
	public void Execute() throws Exception
	{
		
		String[][] TestCase=BaseClass.excel_Files1("Execution_Sheet");
		ReadExcel fis = new ReadExcel(".\\TestData\\BVT_Sheet.xlsx");
		String TestDataPath=".\\TestData\\BVT_Sheet.xlsx";
		System.out.println(TestDataPath);
		int rowCount = fis.getRowCount("Execution_Sheet");
		System.out.println("Total Row Count in Execution Sheet : "+rowCount);
		
		for (int n = 1; n <=rowCount; n++) 
		{
		
			String Product_Name = TestCase[n][0].toString().trim();
			System.out.println("Product Name is :"+Product_Name);
			
			if(Product_Name.equalsIgnoreCase("Care With NCB"))
			{
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					CareWithNCB.CareNCB(TestDataPath,Workbookpath);
					driver.close();
					
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Care With NCB as per Excel sheet.");
				}			
			}
			else if(Product_Name.equalsIgnoreCase("Care Super Saver"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					SuperSaverPolicy.SuperSaver(TestDataPath,Workbookpath);
					
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Care Super Saver as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("Care Freedom"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					CareFreedomPolicy.CareFreedom(TestDataPath,Workbookpath);
					
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Care Freedom as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("Care Global"))
			{
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					CareGlobalPolicy.CareGlobalCase(TestDataPath,Workbookpath);
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Care Global as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("Care For HNI"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					
					CareHNIPolicy.CareHNI(TestDataPath,Workbookpath);
					driver.close();
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Care HNI as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("Care With Smart Select"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					
					CareSmartSelectPolicy.CaresmartSelect(TestDataPath,Workbookpath);
					driver.close();
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Care Select as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("Enhance"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					//String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					Enhance.EnhanceCases();
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Enhnace as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("Secure"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					//String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					Secure.SecureTestCases();
					
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Secure as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("Assure"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					
					Assure.AssureTestCases();

				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Assure as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("Travel"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					//String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					Explore.ExploreTravel();
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to Travel as per Excel sheet.");
				}
				
				
			}
			
			else if(Product_Name.equalsIgnoreCase("Pos Care With Freedom"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					//String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					PosCareFreedomPolicy.PosCareFreedom(TestDataPath,Workbookpath);
					
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to PosCare_Freedom as per Excel sheet.");
				}
				
				
			}else if(Product_Name.equalsIgnoreCase("Pos Care With NCB"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					
					PosCareWithNcbPolicy.CareNCB(TestDataPath,Workbookpath);
				
					
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to PosCare_NCB as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.equalsIgnoreCase("CareSenior"))
			{
				//String Execution_Status = TestCase[n][1].toString().trim();
				String[][] Workbookpath=BaseClass.excel_Files1("Test_Cases_Care");
				System.out.println("Workbook Path is : "+Workbookpath);
				String Execution_Status = TestCase[n][1].toString().trim();
				if(Execution_Status.equalsIgnoreCase("Yes"))
				{
					//String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					CareSenior.CareSeniorProduct(TestDataPath,Workbookpath);
				
					
				}else if(Execution_Status.equalsIgnoreCase("No"))
				{
					System.out.println("No Need to Execute to CareSenior as per Excel sheet.");
				}
				
				
			}
			continue;
		}
	}
	
	
}
