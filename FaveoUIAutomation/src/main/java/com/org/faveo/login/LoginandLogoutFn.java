package com.org.faveo.login;

import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class LoginandLogoutFn extends BaseClass 
{

	@Test
	public void LoginandLog() throws Exception
	{

		try
		{
			for(int i=1;i<=200;i++)
		{
		logger = extent.startTest("TC_"+i);
		LoginFn.LoginPage();
		LogoutFn.LogoutfromApp();
		driver.quit();
		}
	}catch(Exception e)
	{
		System.out.println(e.getMessage());
		logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
		logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
		logger.log(LogStatus.FAIL, "Test Case is Failed.");
		driver.quit();
	}
}
}
