package com.org.faveo.login;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class LoginEncaps extends BaseClass implements AccountnSettingsInterface
{
	private static String UserName=null;
	private static String Password=null;
	
	public String getUsername() 
	{
	return UserName;
	}

	public String getpassword() 
	{
	return Password;
	}
	
	
	public void setUsername(String newUsername) throws Exception
	{
		this.UserName = newUsername;
	}

	public void setPassword(String newPassword) throws Exception
	{
		
		this.Password = newPassword;
	}
	
	public void LoginwithCredendial(String UserName,String Password) throws Exception
	{
		try{
		
			//Entering Username on Login Page by Fecthing Test Data from Excel
			Fluentwait1(By.xpath(Textbox_UserName_xpath));
			enterText(By.xpath(Textbox_UserName_xpath),UserName);
			
			//Entering Password on Loigin Page by Fetching Test Data from Excel
			Fluentwait1(By.xpath(Textbox_Pasword_xpath));
			enterText(By.xpath(Textbox_Pasword_xpath), Password);
			
			//Clicking on Login Button from Login Page
			clickElement(By.xpath(Button_Signin_xpath));
			
		}catch(Exception e)
		{
			System.out.println(e);
			logger.log(LogStatus.FAIL, e);
		}
		
		
	}
	
	public static void VerifyLogin()
	{
		
		try{
			clickElement(By.xpath(Proposal_Dashboard_xpath));
			System.out.println("Login Successfully in Application.");
			logger.log(LogStatus.PASS, "Logged in Sucessfully in Faveo Application.");
		}
	catch(Exception e)
		{
			try{
				waitForElements(By.xpath("//div[@class='alert alert-danger text-center ng-binding ng-scope']"));
				String ErrorMessage = driver.findElement(By.xpath("//div[@class='alert alert-danger text-center ng-binding ng-scope']")).getText();
				System.out.println(ErrorMessage);
				logger.log(LogStatus.WARNING, "Getting Error on Login Page : "+ErrorMessage);
				}
			catch(Exception E1)
			{
					waitForElement(By.xpath(WebServiceDown_xpath));
					String ErrorMessage1 = driver.findElement(By.xpath(WebServiceDown_xpath)).getText();
					logger.log(LogStatus.WARNING, "Test Case is Failed Because : " +ErrorMessage1);
				}
		}
		
	}
}

