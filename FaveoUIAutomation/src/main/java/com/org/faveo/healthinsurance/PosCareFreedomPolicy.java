package com.org.faveo.healthinsurance;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.PolicyJourney.CareNCBPageJourney;
import com.org.faveo.PolicyJourney.PosCareFreedomPageJourney;
import com.org.faveo.login.LoginFn;
import com.org.faveo.login.LogoutFn;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class PosCareFreedomPolicy extends BaseClass implements AccountnSettingsInterface
{

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");

	@Test
	public static void PosCareFreedom(String TestDataFilePath,String[][] Workbookpath) throws Exception 
	{
		/*String[][] TestCase = BaseClass.excel_Files("Test_Case_PosFreedom");

		ReadExcel fis = new ReadExcel("D:\\Test_Data_Faveo_Automation\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Test_Case_PosFreedom");
		System.out.println("Total Number of Row in Sheet : " + rowCount);*/
		String[][] TestCase = Workbookpath;	
		System.out.println("Test Case Path is : "+ TestCase);
		ReadExcel read = new ReadExcel(TestDataFilePath);
		System.out.println("TestDataFilePath is : "+TestDataFilePath);
		int rowCount = read.getRowCount("Test_Cases_Care");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (int n = 23; n <24; n++) 
		{
		try 
		{
		
			String Execution_Status=TestCase[n][4].toString().trim();

			if(Execution_Status.equalsIgnoreCase("Execute"))
			{
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Pos Care Freedom -" + TestCaseName);
		System.out.println("Pos Care Freedom -" + TestCaseName);
		
		// Login with the help of Login Case class
		LoginFn.LoginPage();

		// Clicking on CAREHNI Product dropdown
		HealthInsuranceDropDown.PosCareFreedom();
		
		PosCareFreedomPageJourney.QuotationPageJourney(n);
		
		PosCareFreedomPageJourney.ProposalPageJourney(n);
		
		PosCareFreedomPageJourney.ProposalSummaryPage("Test_Case_PosFreedom",n);
		
		LogoutFn.LogoutfromApp();
		
		driver.quit();
			}
		else{
			System.out.println("No Need to Execute Scenario Number : "+n);
			WriteExcel.setCellData("Test_Cases_Care", "Skip", n, 3);
		}

		} catch (Exception e) 
		{
					WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					driver.quit();
					
		}
				 continue; 
			
		}
	}
}
