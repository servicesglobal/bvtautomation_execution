package com.org.faveo.healthinsurance;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.PolicyJourney.CareNCBPageJourney;
import com.org.faveo.PolicyJourney.CareSeniorJourney;
import com.org.faveo.login.LoginFn;
import com.org.faveo.login.LogoutFn;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareSenior extends BaseClass implements AccountnSettingsInterface {

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");

	

	@Test
	public static void CareSeniorProduct(String TestDataFilePath,String[][] Workbookpath) throws Exception 
	{
		/*String[][] TestCase = BaseClass.excel_Files("Test_Cases_Senior");

		
		ReadExcel read = new ReadExcel("D:\\Test_Data_Faveo_Automation\\Favio_Framework.xlsx");
		int rowCount = read.getRowCount("Test_Cases_Senior");
		System.out.println("Total Number of Row in Sheet : " + rowCount);*/
		String[][] TestCase = Workbookpath;	
		System.out.println("Test Case Path is : "+ TestCase);
		ReadExcel read = new ReadExcel(TestDataFilePath);
		System.out.println("TestDataFilePath is : "+TestDataFilePath);
		int rowCount = read.getRowCount("Test_Cases_Care");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (int n = 25; n <26; n++) 
		{
		try 
		{
		
			String Execution_Status=TestCase[n][4].toString().trim();

			if(Execution_Status.equalsIgnoreCase("Execute"))
			
			{
				
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Care Senior -" + TestCaseName);
		System.out.println("Care Senior -" + TestCaseName);
		
		// Login with the help of Login Case class
		LoginFn.LoginPage();

		// Clicking on CAREHNI Product dropdown
		Thread.sleep(10000);
		HealthInsuranceDropDown.CareSenior();

		CareSeniorJourney.QuotationPageJourney(n);
		
		CareSeniorJourney.ProposalPageJourney(n);
		
		CareSeniorJourney.ProposalSummaryPage("Test_Cases_Senior",n);
		
		LogoutFn.LogoutfromApp();
		
		driver.quit();
			}
		else if(Execution_Status.equalsIgnoreCase("Skip")){
			System.out.println("No Need to Execute Scenario Number : "+n);
			WriteExcel.setCellData("Test_Cases_Care", "Skip", n, 3);
		}

		} catch (Exception e) 
		{
					WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					driver.quit();
					
		}
				 continue; 
			
		}
	}
}
