package com.org.faveo.healthinsurance;
import org.testng.annotations.Test;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.PolicyJourney.SuperMediclaimCancerPolicyJourney;
import com.org.faveo.login.LoginFn;
import com.org.faveo.model.HealthQuestionSuperMediClaim;
import com.org.faveo.utility.ReadExcel;
import com.relevantcodes.extentreports.LogStatus;

public class SuperMediclaimCancer extends BaseClass implements AccountnSettingsInterface {
	
   
	@Test(enabled=false, priority=1)
	public void superMediclaimWithCancer() throws Exception{
		
       String[][] TestCase = BaseClass.excel_Files("SuperMediClainCancerTestCase");
     
		ReadExcel read = new ReadExcel("D:\\Test_Data_Faveo_Automation\\Favio_Framework.xlsx");
		int rowCount = read.getRowCount("SuperMediClainCancerTestCase");
		
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (int n =1; n <rowCount; n++) 
		{
		
		try 
		{
		String Execution_Status=TestCase[n][4].toString().trim();
		if(Execution_Status.equalsIgnoreCase("Execute"))
		{
	   String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer -" + TestCaseName);
		System.out.println("Super Medicliam Cancer -" + TestCaseName);
		LoginFn.LoginPage1("Super Mediclaim");
		Thread.sleep(5000);
		HealthInsuranceDropDown.superMediClaimWithCancer();
		SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);
		SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
		SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
		HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);
		SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);
		//DataVerificationByPDF.verifyPolicyDetailsInPdf();
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
       
		//LogoutFn.LogoutfromApp();
        //driver.quit();
		
		}
		else{
		System.out.println("No Need to Execute Scenario Number : " + n);
		//WriteExcel.setCellData("Test_Cases_Care", "Skip", n, 3);
		}
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			//driver.quit();
			
		}
		continue; 
	}
	}
	
	//*********************** Test case for Share Quotation *****************************************
	
	//Test Case 1 -  Verify that details the share quotation details in Quotes Tracker
	@Test(enabled=true, priority=2)
	public void verifyDataInQuotationTracker_ForShareQuotation_InSuperMediclaimCancer() throws Exception{
		int n= 1;
		 String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		 String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share quotation -" + TestCaseName);
		System.out.println(TestCaseName);
		try 
		{
		LoginFn.LoginPage1("Super Mediclaim");
		Thread.sleep(5000);
		HealthInsuranceDropDown.superMediClaimWithCancer();
		DataVerificationShareQuotationPage.QuotationPageJourney(n);
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
		DataVerificationShareQuotationPage.verifyDataInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(n);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
       
		//LogoutFn.LogoutfromApp();
        driver.quit();
		
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			driver.quit();
			
	}
	}
	
	//Test Case - 2 -  Verify the Quotation details in the Mail
	@Test(enabled=true, priority=3)
	public void verifyDataInEmail_ForShareQuotation_InSuperMediclaimCancer() throws Exception{
		int n= 2;
		 String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		 String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share quotation -" + TestCaseName);
		System.out.println(TestCaseName);
		
		try 
		{
		//Step 1 - Open Email and delete old Emails
		System.out.println("Open Email and delete old Email");
		logger.log(LogStatus.PASS, "Open Email and delete old Email");
		
		LaunchBrowser();
		ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit();
		
		//Step 2 - Open application and Share a quotation
		System.out.println("Open Application and Share a Quote");
		logger.log(LogStatus.PASS, "Open Application and Share a Quote");
		
		LoginFn.LoginPage1("Super Mediclaim");
		Thread.sleep(5000);
		HealthInsuranceDropDown.superMediClaimWithCancer();
		DataVerificationShareQuotationPage.QuotationPageJourney(n);
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
		driver.quit();
		
		//Step 3 - Again open the email and verify the Shared quotation data in email
		System.out.println("Again open the email and verify the Shared quotation data in email");
		logger.log(LogStatus.PASS, "Again open the email and verify the Shared quotation data in email");
		
		LaunchBrowser();
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForSuperMediclaimCancer(n);

		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
       
		//LogoutFn.LogoutfromApp();
        driver.quit();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			driver.quit();
			
		
	}
	}
	
	//Test case - 3 -  Verify the Proposal Generated GUID should be reusable. 
	@Test(enabled=true, priority=4)
	public void VerifyThePropoalGeneratedGUID_ShouldBeReusable_ForSharedQuotation_InSuperMediclaimCancer() throws Exception{
	int n = 3;
       String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
	   String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share quotation -" + TestCaseName);
		System.out.println("Super Medicliam Cancer Share quotation -" + TestCaseName);
		
		try 
		{
		//Step 1 - Open Email and delete old emails
		System.out.println("Open Email and delete old Email");
		logger.log(LogStatus.PASS, "Open Email and delete old Email");
		
		LaunchBrowser();
		ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit();
		
		//Step 2 - Login the application and send Share Quotation
		System.out.println("Open Application and Share a Quote");
		logger.log(LogStatus.PASS, "Open Application and Share a Quote");
		
		LoginFn.LoginPage1("Super Mediclaim");
		Thread.sleep(5000);
		HealthInsuranceDropDown.superMediClaimWithCancer();
		DataVerificationShareQuotationPage.QuotationPageJourney(n);
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
		driver.quit();
		
		//Step 3 - Open Email and Click on Buy Now button and punch the policy
		
		System.out.println("Open Email and Click on Buy Now button and punch the policy");
		logger.log(LogStatus.PASS, "Open Email and Click on Buy Now button and punch the policy");
		
		LaunchBrowser();
		ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
		SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
		SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
		HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);
		SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);
		driver.quit();
		
		//Step 4- Again Open the email and verify again Buy now button should be work/GUID should be reusable
		System.out.println("Again Open the email and verify again Buy now button should be work/GUID should be reusable");
		logger.log(LogStatus.PASS, "Again Open the email and verify again Buy now button should be work/GUID should be reusable");
		
		LaunchBrowser();
		ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
		
		//Verify the Page Title
		DataVerificationShareQuotationPage.verifyPageTitle();
		
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
       
		//LogoutFn.LogoutfromApp();
        driver.quit();
		
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			driver.quit();
			
		}
		
	}
	
	//Test case - 4 - Verify after punching the policy through Shared Quotation, the Proposal should exit from the Quotes Tracker Page
	@Test(enabled=true, priority=5)
	public void VerifyafterpunchingthepolicythroughSharedQuotationtheProposalshouldexitfromtheQuotesTrackerPage_InSuperMediclaimCancer() throws Exception{
		
		int n= 4;
		 String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		 String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share quotation -" + TestCaseName);
		System.out.println(TestCaseName);
		
		try 
		{
		//Step 1 - Launch the Browser
		System.out.println("Open Email and delete old Email");
		logger.log(LogStatus.PASS, "Open Email and delete old Email");
		LaunchBrowser();
		ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit(); //Closed the browser
		
		//Step 2 - Moved to Religare Site and Share a Quotation
		System.out.println("Open Application and Share a Quote");
		logger.log(LogStatus.PASS, "Open Application and Share a Quote");
		
		LoginFn.LoginPage1("Super Mediclaim"); 
		Thread.sleep(5000);
		HealthInsuranceDropDown.superMediClaimWithCancer(); 
		DataVerificationShareQuotationPage.QuotationPageJourney(n);
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
		DataVerificationShareQuotationPage.verifyDataInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(n);
		driver.quit();
		
		//Step 3 - Again Open Mail and Click on Buy Now Button from Email and punch the Policy
		System.out.println("Again Open Mail and Click on Buy Now Button from Email and punch the Policy");
		logger.log(LogStatus.PASS, "Again Open Mail and Click on Buy Now Button from Email and punch the Policy");
		
		LaunchBrowser();
		ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
		SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n); 
		SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
		HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);
		SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);
		driver.quit();
		 
		//Again Login the application and verify after punch the policy data should be not available in quotation Tracker
		
		System.out.println("Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
		logger.log(LogStatus.PASS, "Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
		
		LoginFn.LoginPage1("Super Mediclaim");
		DataVerificationShareQuotationPage.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(n);
		
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
       
		//LogoutFn.LogoutfromApp();
        driver.quit();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			driver.quit();
			
		}	
	
	}
	
	
//******************************** Test Case for Shared Proposals ******************************************
	//Test Case 1 -  Verify that details the share Proposal details in Quotes Tracker
		@Test(enabled=false, priority=6)
		public void verifyDataInQuotationTracker_ForShraeProposal_InSuperMediclaimCancer() throws Exception{
			int n= 1;
			 String[][] TestCase = BaseClass.excel_Files("ShareProposal");
			 String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
			logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
			System.out.println(TestCaseName);
			try 
			{
			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			HealthInsuranceDropDown.superMediClaimWithCancer();
			SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);
			SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
			SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			DataVerificationShareProposalPage.verifyDataInOfShareProposalInDraftHistory_ForSuperMediclaimCancer(n);
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
	       
			//LogoutFn.LogoutfromApp();
	        //driver.quit();
			
			}
			catch(Exception e){
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//driver.quit();
				
		}
		}
		
	/*Test Case 2 -  Verify the share quotation proposal details in Draft History and if it is avilable Click on Resume Policy and punch 
	the policy and verify That policy should be exit from draft */
	@Test(enabled = false, priority = 7)
	public void verifyDataInQuotationTrackerAndPunchThePolicyOnClickResumeProposal_ForShraeProposal_InSuperMediclaimCancer() throws Exception {
		int n = 2;
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
		try {
			
			//Step 1 - Open the Application and share a proposal
			System.out.println("Open the Application and share a proposal");
			logger.log(LogStatus.PASS, "Open the Application and share a proposal");
			
			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			HealthInsuranceDropDown.superMediClaimWithCancer();
			SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);
			SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
			SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			
			//Step 2 - Go to Draft History and verify Shared Proposal Data should be available and complete punch the policy on Clicking Resume Policy link
			System.out.println("Go to Draft History and verify Shared Proposal Data should be available and complete punch the policy on Clicking Resume Policy link");
			logger.log(LogStatus.PASS, "Go to Draft History and verify Shared Proposal Data should be available and complete punch the policy on Clicking Resume Policy link");
			
			DataVerificationShareProposalPage.verifyDataInOfShareProposalInDrafthistoryAndPunchThePolicyOnClickResumePolicy_ForSuperMediclaimCancer(n);
			HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);
			SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);
			driver.quit(); //Close the browser
			
			//Step 3 - Again Login the application and verify shared proposal should not be available in Draft History
			System.out.println("Again Login the application and verify shared proposal should not be available in Draft History");
			logger.log(LogStatus.PASS, "Again Login the application and verify shared proposal should not be available in Draft History");
			
			LoginFn.LoginPage1("Super Mediclaim");
			DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCancer(n);
			
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");

			// LogoutFn.LogoutfromApp();
			// driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			// driver.quit();

		}
	}
	
	/*Test Case 3 -   Verify the Share Proposal details in the Mail */
	@Test(enabled = false, priority = 8)
	public void verifyDataInEmail_ForShareProposal_InSuperMediclaimCancer() throws Exception {
		int n = 3;
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
		try {
			
			//Step 1 - Open Email and Delete Old Email
			System.out.println("Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit();
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Go to Application and share a proposal");
			
			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			HealthInsuranceDropDown.superMediClaimWithCancer();
			SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);
			SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
			SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			driver.quit(); 
			
			//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
			System.out.println("Again Open the email and verify the data of Share Proposal in Email Body");
			logger.log(LogStatus.PASS, "Again Open the email and verify the data of Share Proposal in Email Body");
			
		    LaunchBrowser();
		    DataVerificationShareProposalPage.verifyDataOfShareQuotationInMail_ForSuperMediclaimCancer(n);
			
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			// LogoutFn.LogoutfromApp();
			// driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			// driver.quit();

		}
	}
	
	/*Test Case 4 - Verify the Proposal Generated GUID should be reusable */
	@Test(enabled = false, priority = 8)
	public void verifyProposalGeneratedGUIDShouldBeReusable_ForShareProposal_InSuperMediclaimCancer() throws Exception {
		int n = 4;
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
		try {
			
			//Step 1 - Open Email and Delete Old Email
			System.out.println("Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit();
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Go to Application and share a proposal");
			
			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			HealthInsuranceDropDown.superMediClaimWithCancer();
			SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);
			SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
			SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			driver.quit(); 
			
			//Step 3 - Open Email and Click on Buy Now button and punch the policy
			System.out.println("Open Email and Click on Buy Now button and punch the policy");
			logger.log(LogStatus.PASS, "Open Email and Click on Buy Now button and punch the policy");
			
			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
			DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
			DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
			HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);
			SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);
			driver.quit();
			
			//Step 4 - Again Open the Email and click on Buy Now button and Verify Buy Now link button should be work/GUID should be reusable
			System.out.println("Again Open the Email and click on Buy Now button and Verify Buy Now link button should be work/GUID should be reusable");
			logger.log(LogStatus.PASS, "Again Open the Email and click on Buy Now button and Verify Buy Now link button should be work/GUID should be reusable");
			
			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
			
			//Verify the Page Title
			DataVerificationShareProposalPage.verifyPageTitle();
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			// LogoutFn.LogoutfromApp();
			// driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			// driver.quit();

		}
	}
	
	/*Test Case 4 - Verify after punching the policy through Shared Quotation, the Proposal should exit from the Quotes Tracker Page */
	@Test(enabled = false, priority = 8)
	public void VerifyafterpunchingthepolicythroughSharedProposaltheProposalshouldexitfromtheDraftHistoryPage_ForShareProposal_InSuperMediclaimCancer() throws Exception {
		int n = 5;
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
		try {
			
			//Step 1 - Open Email and Delete Old Email
			System.out.println("Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit();
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Go to Application and share a proposal");
			
			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			HealthInsuranceDropDown.superMediClaimWithCancer();
			SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);
			SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
			SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			DataVerificationShareProposalPage.verifyDataInOfShareProposalInDraftHistory_ForSuperMediclaimCancer(n);
			driver.quit(); 
			
			//Step 3 - Open Email and Click on Buy Now button and punch the policy
			System.out.println("Open Email and Click on Buy Now button and punch the policy");
			logger.log(LogStatus.PASS, "Open Email and Click on Buy Now button and punch the policy");
			
			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
			DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
			DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
			HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);
			SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);
			driver.quit();
			
			//Step 4 - Again Moved to application and Verify Shared Proposal Data should be not available in Draft History
			System.out.println("Again Moved to application and Verify Shared Proposal Data should be not available in Draft History");
			logger.log(LogStatus.PASS, "Again Moved to application and Verify Shared Proposal Data should be not available in Draft History");
			
			LoginFn.LoginPage1("Super Mediclaim");
			DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCancer(n);
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			// LogoutFn.LogoutfromApp();
			// driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			// driver.quit();

		}
	}
}
