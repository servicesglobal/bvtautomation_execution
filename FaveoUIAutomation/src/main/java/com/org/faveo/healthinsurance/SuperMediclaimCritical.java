package com.org.faveo.healthinsurance;

import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.SuperMediclaimCriticalFunctions;
import com.org.faveo.login.LoginFn;
import com.org.faveo.utility.ReadExcel;

public class SuperMediclaimCritical extends SuperMediclaimCriticalFunctions{
	
	//public static int rowc;
	
	@Test(priority = 6, enabled=true)
	public static void supermediclaimcritical() throws Exception {

		System.out.println("Super Medical : Critical Script Execution Started");
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");
		System.out.println("rowCount is " + rowCount1);
		System.out.println("Total number of rows : " + BaseClass.rowCount1);
		 int rowc = rowCount1;
        
		for(n=1;n<=rowc;n++) {
	/*	for (int n = 1; n <= rowc; n++) {*/
			SuperMediclaimCriticalFunctions.LaunchBrowser1();
			Thread.sleep(5000);
			SuperMediclaimCriticalFunctions.login();

			try {
				System.out.println("Total number of rows : " + rowCount1);
				SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
				SuperMediclaimCriticalFunctions.criticaldropdown();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Tenurecritical();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Monthly_frequency();
				SuperMediclaimCriticalFunctions.critical_verify_premium();
				SuperMediclaimCriticalFunctions.Critical_proposerDetails();
				SuperMediclaimCriticalFunctions.critical_insuredDetails();
				// Reusable.critical_questionset();
				SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
				SuperMediclaimCriticalFunctions.Critical_Payment();
				driver.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			continue;
		}

		}
	

}
