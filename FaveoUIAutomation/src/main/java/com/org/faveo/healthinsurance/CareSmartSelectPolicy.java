package com.org.faveo.healthinsurance;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.PolicyJourney.CareNCBPageJourney;
import com.org.faveo.login.LoginFn;
import com.org.faveo.login.LogoutFn;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareSmartSelectPolicy extends BaseClass implements AccountnSettingsInterface {

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");

	@Test
	public static void CaresmartSelect(String TestDataFilePath,String[][] Workbookpath) throws Exception {

		
		
	/*	ReadExcel fis = new ReadExcel("D:\\Test_Data_Faveo_Automation\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Test_Cases_SmartSelect");
		System.out.println("Total Number of Row in Sheet : " + rowCount);
		
		String[][] TestCase = BaseClass.excel_Files("Test_Cases_SmartSelect");*/
		String[][] TestCase = Workbookpath;	
		System.out.println("Test Case Path is : "+ TestCase);
		ReadExcel read = new ReadExcel(TestDataFilePath);
		System.out.println("TestDataFilePath is : "+TestDataFilePath);
		int rowCount = read.getRowCount("Test_Cases_Care");
		System.out.println("Total Number of Row in Sheet : " + rowCount);
		
		for (int n = 11; n <=12; n++) 
		{
		try 
		{
		
			
			String Execution_Status=TestCase[n][4].toString().trim();

			if(Execution_Status.equalsIgnoreCase("Execute"))
			{
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Care Smart Select -" + TestCaseName);
		System.out.println("Care Smart Select -" + TestCaseName);
		
		// Login with the help of Login Case class
		LoginFn.LoginPage();

		// Clicking on CAREHNI Product dropdown
		Thread.sleep(7000);
		HealthInsuranceDropDown.CareSmartSelectPolicy();

		CareNCBPageJourney.QuotationPageJourney(n);
		
		CareNCBPageJourney.ProposalPageJourney(n);
		
		CareNCBPageJourney.ProposalSummaryPage("Test_Cases_Care",n);
		WriteExcel.setCellData("Test_Cases_Care", "Pass", n, 3);
		
		
		LogoutFn.LogoutfromApp();
		
		//driver.quit();
			}
		else{
			System.out.println("No Need to Execute Scenario Number : "+n);
			WriteExcel.setCellData("Test_Cases_Care", "Skip", n, 3);
		}

		} catch (Exception e) {
					WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
					//logger.log(LogStatus.FAIL, "Test Case is Fail");
					
					
				}
				 continue; 
			
		}
	}
}
