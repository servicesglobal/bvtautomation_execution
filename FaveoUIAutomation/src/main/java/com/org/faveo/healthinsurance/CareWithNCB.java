package com.org.faveo.healthinsurance;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.PolicyJourney.CareNCBPageJourney;
import com.org.faveo.login.LoginFn;
import com.org.faveo.login.LogoutFn;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareWithNCB extends BaseClass implements AccountnSettingsInterface {

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");

	

	@Test
	public static void CareNCB(String TestDataFilePath,String[][] Workbookpath) throws Exception 
	{
		String[][] TestCase = Workbookpath;	
		System.out.println("Test Case Path is : "+ TestCase);
		ReadExcel read = new ReadExcel(TestDataFilePath);
		System.out.println("TestDataFilePath is : "+TestDataFilePath);
		int rowCount = read.getRowCount("Test_Cases_Care");
		System.out.println("Total Number of Row in Sheet : " + rowCount);
    
		for (int n =1; n <=2; n++) 
			
		{
			
		try 
		{
			String Execution_Status=TestCase[n][4].toString().trim();
			System.out.println("Execution Status is  "+Execution_Status);
			if(Execution_Status.equalsIgnoreCase("Execute"))
		{
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Care With NCB -" + TestCaseName);
		System.out.println("Care With NCB -" + TestCaseName);
		
		// Login with the help of Login Case class
		LoginFn.LoginPage();
		System.out.println("Product 1");

		// Clicking on CAREHNI Product dropdown
		Thread.sleep(7000);
		HealthInsuranceDropDown.CareWithNCBPolicy();


		CareNCBPageJourney.QuotationPageJourney(n);
		
		CareNCBPageJourney.ProposalPageJourney(n);
		
		CareNCBPageJourney.ProposalSummaryPage("Test_Cases_Care",n);
		
		WriteExcel.setCellData("Test_Cases_Care", "Pass", n, 3);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
		LogoutFn.LogoutfromApp();
		
		
		//driver.quit();
			/*}
		else{
			System.out.println("No Need to Execute Scenario Number : "+n);
			WriteExcel.setCellData("Test_Cases_Care", "Skip", n, 3);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
			
		}*/

		 }
			else{
				System.out.println("No Need to Execute Scenario Number : "+n);
				WriteExcel.setCellData("Test_Cases_Care", "Skip", n, 3);
			}
			
		}catch (Exception e) {
			WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed");
			driver.quit();
		}
		continue;
	}

}


}
