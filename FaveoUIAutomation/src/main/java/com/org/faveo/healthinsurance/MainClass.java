package com.org.faveo.healthinsurance;

import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.fixedbenefitinsurance.Assure;
import com.org.faveo.fixedbenefitinsurance.Secure;
import com.org.faveo.travelInsurance.Explore;
import com.org.faveo.utility.ReadExcel;

public class MainClass extends BaseClass implements AccountnSettingsInterface 

{

	//TestClass
	@Test
	public void Execute() throws Exception
	{
		
		String[][] TestCase=BaseClass.excel_Files("Execution_Sheet");
		ReadExcel fis = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Execution_Sheet");
		String TestDataPath=".\\TestData\\BVT_Sheet.xlsx";
		
		for (int n = 1; n <rowCount; n++) 
		{
		
			String Product_Name = TestCase[n][0].toString().trim();
			
			if(Product_Name.contains("Care With NCB"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					CareWithNCB.CareNCB(TestDataPath,Workbookpath);
					
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care With NCB as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Care Super Saver"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					SuperSaverPolicy.SuperSaver(TestDataPath,Workbookpath);
					
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care Super Saver as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Care Freedom"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					CareFreedomPolicy.CareFreedom(TestDataPath,Workbookpath);
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care Freedom as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Care Global"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					CareGlobalPolicy.CareGlobalCase(TestDataPath,Workbookpath);
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care Global as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Care For HNI"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					CareHNIPolicy.CareHNI(TestDataPath,Workbookpath);
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care HNI as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Care With Smart Select"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					CareSmartSelectPolicy.CaresmartSelect(TestDataPath,Workbookpath);
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care Select as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Enhance"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					Enhance.EnhanceCases();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Enhnace as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Secure"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					Secure.SecureTestCases();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Secure as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Assure"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					Assure.AssureTestCases();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Assure as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Travel"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					Explore.ExploreTravel();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Travel as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("PosCare_NCB"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					PosCareWithNcbPolicy.CareNCB(TestDataPath,Workbookpath);
					
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to PosCare_NCB as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("PosCare_Freedom"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					PosCareFreedomPolicy.PosCareFreedom(TestDataPath,Workbookpath);
					
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to PosCare_Freedom as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("CareSenior"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					String[][] Workbookpath=BaseClass.excel_Files("Test_Cases_Care");
					CareSenior.CareSeniorProduct(TestDataPath,Workbookpath);
					
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to CareSenior as per Excel sheet.");
				}
				
				
			}
			
		}
	}
	
	
}
