package com.org.faveo.fixedbenefitinsurance;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;


public class Sikuli extends BaseClass implements AccountnSettingsInterface {
	
	@Test
	public void LoginwithSikuli() throws Exception
	{
	
		for(int i = 1 ; i<=10; i++)
		{
			
	try
			{
		Screen s = new Screen();
		String filepath = "D:\\New Faveo UI Automation Code\\FaveoUIAutomation\\SikulliImages\\";
		Pattern pattern = new Pattern(filepath + "UserID.png");
		Pattern pattern1 = new Pattern(filepath + "Password.PNG");
		Pattern pattern2 = new Pattern(filepath + "SignInButton.PNG");
		Pattern pattern3 = new Pattern(filepath + "MyProfile.PNG");
		Pattern pattern4 = new Pattern(filepath + "Logout.PNG");
		Pattern pattern5 = new Pattern(filepath + "LoggedOutMessage.PNG");
		
		BaseClass.LaunchBrowser();
		
		System.out.println("Navigating to Faveo URL");
		
		s.wait(pattern, 30);
	    
		System.out.println("Name From Here : ");
		
		s.wait(pattern, 10);
		s.type(pattern, "20572113");
		s.type(pattern1, "abcd1111");
		s.click(pattern2);
		s.wait(pattern3, 30);
		s.click(pattern3);
		//s.wait(pattern, 5);
		s.click(pattern4);
		s.click(pattern5);
		System.out.println("User Logged Out Scucessfully...");
	    driver.close();

		i++;
			}
			catch(Exception e){
				driver.close();
				System.out.println("Test Case is Fail");
			}
	}
		
	}
}
