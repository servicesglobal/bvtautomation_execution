package com.org.faveo.model;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class PayUPaymentFn extends BaseClass implements AccountnSettingsInterface
{

	public static void PayuPage_Credentials() throws Exception
	{
		String[][] carddetails=excel_Files("Credentials");
		
		
		
		try
	{
	clickElement(By.id("drop_image_1"));
	Thread.sleep(1000);
	clickElement(By.xpath("//small[contains(text(),'Visa | Master')]"));
	}
	catch(Exception e)
		{
		clickElement(By.xpath("//input[@id='credit-card']"));
		System.out.println("Card Selection Option is not available");
		}
		
		
	String CardNumber = carddetails[1][0].toString().trim();
	try{

	Fluentwait(By.id("ccard_number"), 60, "Unable to Find Card Number Field in Payu.");
	clearTextfield(By.id("ccard_number"));
	ExplicitWait(By.xpath("//input[@name='ccard_number']"), 2);
	enterText(By.xpath("//input[@name='ccard_number']"), String.valueOf(CardNumber));
	System.out.println("Entered Credit Card Number : "+CardNumber);
	}
	catch(Exception e)
	{
		System.out.println("Credit Card Number is not Entered.");
	}

	
	//Reading Name on Card
	waitForElement(By.id("cname_on_card"));
	String NameOnCard = carddetails[1][1].toString().trim();
	Fluentwait(By.id("cname_on_card"), 60, "Unable to Fin Name on Card Text Field on Payu Page.");
	enterText(By.id("cname_on_card"), NameOnCard);
	System.out.println("Entered Card Name : "+NameOnCard);
	

	
	//Read CVV Number from Excel
	waitForElement(By.id("ccvv_number"));
	String CVVNum = carddetails[1][2].toString().trim();
	Fluentwait(By.id("ccvv_number"), 60, "Unable to Find CVV Number Field on Payu page.");
	enterText(By.id("ccvv_number"), CVVNum);
	System.out.println("Entered CVV Number : "+CVVNum);
	
	//Reading Exp Month from Excel
	String Month = carddetails[1][3].toString().trim();
	waitForElement(By.id("cexpiry_date_month"));
	clickElement(By.id("cexpiry_date_month"));
	clickElement(By.xpath("//select[@id='cexpiry_date_month']//option[contains(text()," + "'"+ Month + "'"+")]"));
	System.out.println("Entered Credit card Expiry Month : "+Month);
	
	//Reading Exp Year from Excel
	waitForElement(By.id("cexpiry_date_year"));
	String ExpYear = carddetails[1][4].toString().trim();
	clickElement(By.xpath("//select[@id='cexpiry_date_year']"));
	clickElement(By.xpath("//select[@id='cexpiry_date_year']//option[contains(text(),"+ExpYear+")]"));
	System.out.println("Entered Credit Card Expiry Year : "+ExpYear);
	Thread.sleep(1000);
	(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();

	
/*	//Verifying Invalid credit card number Error
	try{
		Fluentwait(By.xpath(TransNumMessage_xpath), 60, "Unable to Find Transaction Number.");
		System.out.println("Transaction Number is Found.");
		}
	catch(Exception e){
		for(int i=1;i<=20;i++)
		{
			String InvalidCardError = driver.findElement(By.xpath("//label[@for='ccard_number'][contains(text(),'Invalid credit card number.')]")).getText();
			if(InvalidCardError.contains("Invalid credit card number."))
			{
				Fluentwait(By.id("ccard_number"), 60, "Unable to Find Card Number Field in Payu.");
				clearTextfield(By.id("ccard_number"));
				ExplicitWait(By.xpath("//input[@name='ccard_number']"), 2);
				enterText(By.xpath("//input[@name='ccard_number']"), String.valueOf(CardNumber));
				Thread.sleep(1000);
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();

				continue;	
			}
			else
		{
			System.out.println("Valid Credit Card Number.");
			break;
		}
			
			
		}

	}*/
	
	
	//Verifying Invalid credit card number Error
	try{
		for(int i=1;i<=100;i++)
		{
			String InvalidCardError = driver.findElement(By.xpath("//label[@for='ccard_number'][contains(text(),'Invalid credit card number.')]")).getText();
			if(InvalidCardError.contains("Invalid credit card number."))
			{

		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='ccard_number']")).clear();
		Thread.sleep(2000);		
		clickElement(By.xpath("//input[@name='ccard_number']"));		
		Thread.sleep(2000);
		enterText(By.xpath("//input[@name='ccard_number']"), String.valueOf(CardNumber));
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();
		continue;	
		}
			else
		{
			System.out.println("Valid Credit Card Number.");
			break;
		}
			
			
		}
	}
	catch(Exception e){
	System.out.println("Valid Credit Card Number");
	}
	
	
	try{
		Fluentwait1(By.xpath(TransNumMessage_xpath));
		System.out.println("Transaction Number is Found.");

		}
	catch(Exception e)
	{
			waitForElement(By.id("ccvv_number"));
			Fluentwait(By.id("ccvv_number"), 60, "Unable to Find CVV Number Field on Payu page.");
			enterText(By.id("ccvv_number"), CVVNum);
			Thread.sleep(1000);
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();

	}
	
	}
	
	

	
}
