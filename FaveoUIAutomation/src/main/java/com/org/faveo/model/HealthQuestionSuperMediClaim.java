package com.org.faveo.model;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class HealthQuestionSuperMediClaim extends BaseClass implements AccountnSettingsInterface {

	//Note: ********** This functions we can Reuse for another submodule of SuperMediclaims if Xpath Will Be Same ******************************
	
	
	//************************  Functions Start for Question Set 1 ****************************** 
	
	//************************  Existing Since Date Entered of Sub Question1 ****************************** 
		// Function for Click on Check Box for First Sub question which are available under first question for Member 1
		public static void setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber1(String SheetName, int Rownum, int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateforMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateforMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber1_Xpath));
				System.out.println("'Recurrent cough, hoarseness of voice for 15 days' is Selected");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber1_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber1_Xpath), String.valueOf(DOB));

				System.out.println("Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 1: " + DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 1: " + DOB);
			} else {
				System.out.println("'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 1");
				logger.log(LogStatus.FAIL, "'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 1");
			}

		}
		
		//Function for Click on Check Box for First Sub question which are available under first question for Member 2
		public static void setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			
			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();
			
			if(sinceExistingCheckBox.equalsIgnoreCase("Yes")){
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber2_Xpath));
				System.out.println("Select 'Recurrent cough, hoarseness of voice for 15 days' is Selected");
				
				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber2_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber2_Xpath), String.valueOf(DOB));
				
				System.out.println("Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 2: " + DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 2: " + DOB);
			}
			else{
				System.out.println("'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 2");
				logger.log(LogStatus.FAIL, "'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 2");
			}
			
		}
		
		//Function for Click on Check Box for First Sub question which are available under first question for Member 3
			public static void setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber3(String SheetName, int Rownum, int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception{
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);
				
				String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
				String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();
				
				if(sinceExistingCheckBox.equalsIgnoreCase("Yes")){
					clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber3_Xpath));
					System.out.println("Select 'Recurrent cough, hoarseness of voice for 15 days' is Selected");
					
					clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber3_Xpath));
					enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber3_Xpath), String.valueOf(DOB));
					
					System.out.println("Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 3: " + DOB);
					logger.log(LogStatus.PASS, "Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 3: " + DOB);
				}
				else{
					System.out.println("'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 3");
					logger.log(LogStatus.FAIL, "'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 3");
				}
				
			}
			
		// Function for Click on Check Box for First Sub question which are available under first question for Member 4
		public static void setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,  int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber4_Xpath));
				System.out.println("Select 'Recurrent cough, hoarseness of voice for 15 days' is Selected");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber4_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber4_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 4: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 4: " + DOB);
			} else {
				System.out.println("'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 4");
				logger.log(LogStatus.FAIL, "'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 3");
			}

		}
		
		// Function for Click on Check Box for First Sub question which are available under first question for Member 5
			public static void setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);

				String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
				String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

				if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
					clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber5_Xpath));
					System.out.println("Select 'Recurrent cough, hoarseness of voice for 15 days' is Selected");

					clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber5_Xpath));
					enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber5_Xpath),
							String.valueOf(DOB));

					System.out.println(
							"Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 5: "
									+ DOB);
					logger.log(LogStatus.PASS, "Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 5: " + DOB);
				} else {
					System.out.println("'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 5");
					logger.log(LogStatus.FAIL, "'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 5");
				}

			}
			
		// Function for Click on Check Box for First Sub question which are available under first question for Member 6
		public static void setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber6_Xpath));
				System.out.println("Select 'Recurrent cough, hoarseness of voice for 15 days' is Selected");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber6_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber6_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 6: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for Recurrent cough, hoarseness of voice for 15 days'for Member 6: " + DOB);
			} else {
				System.out.println("'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 6");
				logger.log(LogStatus.FAIL, "'Recurrent cough, hoarseness of voice for 15 days' not selected for Member 6");
			}

		}
		
	// ************************ Existing Since Date Entered of Sub Question 2 ******************************
	// Function for Click on Check Box for First Sub question 2 which are available under first question for Member 1
	public static void setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber1(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateforMember1) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String DOB = TestCaseData[Rownum][ExistingDiseaseDateforMember1].toString().trim();

		if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber1_Xpath));
			System.out.println("Select 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'");

			clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber1_Xpath));
			enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber1_Xpath),
					String.valueOf(DOB));

			System.out.println(
					"Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' for Member 1: "
							+ DOB);
			logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' for Member 1: "+ DOB);
		} else {
			System.out.println("'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 1");
			logger.log(LogStatus.FAIL,"'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 1");
		}

	}

	// Function for Click on Check Box for First Sub question which are available under first question for Member 2
	public static void setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
			int ExistingDiseaseDateMember2) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember2].toString().trim();

		if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
			
			WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
			clickByJS(e1);
			
			//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
			System.out.println("Select 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'");
			
			
			waitForElement(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
			//clickByJS(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
			clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
			enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath),
					String.valueOf(DOB));

			System.out.println(
					"Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'for Member 2: "
							+ DOB);
			logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' for Member 2: "+ DOB);
		} else {
			System.out.println("'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 2");
			logger.log(LogStatus.FAIL,"'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 2");
		}

	}

	// Function for Click on Check Box for First Sub question which are available under first question for Member 3
	public static void setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber3(String SheetName, int Rownum, int subQuestionCheckBoxValueColNum,
			int ExistingDiseaseDateMember1) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

		if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
           WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
			System.out.println("Select 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'");

			clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
			enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber3_Xpath),
					String.valueOf(DOB));

			System.out.println(
					"Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'for Member 3: "
							+ DOB);
			logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' for Member 3: "+ DOB);
		} else {
			System.out.println("'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 3");
			logger.log(LogStatus.FAIL,"'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 3");
		}

	}

	// Function for Click on Check Box for First Sub question which are available under first question for Member 4
	public static void setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
			int ExistingDiseaseDateMember1) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

		if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
			WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
			System.out.println("Select 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'");

			clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
			enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber4_Xpath),
					String.valueOf(DOB));

			System.out.println(
					"Existing Since Date Entered for'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'for Member 4: "
							+ DOB);
			logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' for Member 4: "+ DOB);
		} else {
			System.out.println("'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 4");
			logger.log(LogStatus.FAIL,"'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 4");
		}

	}

	// Function for Click on Check Box for First Sub question which are available under first question for Member 5
	public static void setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
			int ExistingDiseaseDateMember1) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

		if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
			WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
			System.out.println("Select 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'");

			clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
			enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber5_Xpath),
					String.valueOf(DOB));

			System.out.println(
					"Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'for Member 5: "
							+ DOB);
			logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' for Member 5: "+ DOB);
		} else {
			System.out.println("'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 5");
			logger.log(LogStatus.FAIL,"'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 5");
		}

	}

	// Function for Click on Check Box for First Sub question which are
	// available under first question for Member 6
	public static void setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
			int ExistingDiseaseDateMember1) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

		if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
			WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
			System.out.println("Select 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'");

			clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
			enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber6_Xpath),
					String.valueOf(DOB));

			System.out.println(
					"Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?'for Member 6: "
							+ DOB);
			logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' for Member 6: "+ DOB);
		} else {
			System.out.println("'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 6");
			logger.log(LogStatus.FAIL,"'Persistent indigestion or difficulty or obstruction in swallowing for a continuous period of 15 days?' not selected for Member 6");
		}

	}
	
	// ************************ Existing Since Date Entered of Sub Question 3 ******************************
		// Function for Click on Check Box for First Sub question 3 which are available under first question for Member 1
		public static void setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber1(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateforMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateforMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber1_Xpath));
				
				System.out.println("Select 'Unusual bleeding or discharge of any kind from anybody opening?'");
				logger.log(LogStatus.PASS, "Select 'Unusual bleeding or discharge of any kind from anybody opening?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber1_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber1_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?' for Member 1: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?' for Member 1: "+ DOB);
			} else {
				System.out.println("'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 1");
				logger.log(LogStatus.FAIL, "'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 1");
				
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 2
		public static void setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember2) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember2].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber2_Xpath));
				clickByJS(e1);
				
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				System.out.println("Select 'Unusual bleeding or discharge of any kind from anybody opening?'");
				logger.log(LogStatus.PASS, "Select 'Unusual bleeding or discharge of any kind from anybody opening?'");
				
				
				waitForElement(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber2_Xpath));
				//clickByJS(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber2_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber2_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?'for Member 1: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?' for Member 2: "+ DOB);
			} else {
				System.out.println("'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 2");
				logger.log(LogStatus.FAIL, "'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 2");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 3
		public static void setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber3(String SheetName, int Rownum, int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
	           WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber3_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
				System.out.println("Select 'Unusual bleeding or discharge of any kind from anybody opening?'");
				logger.log(LogStatus.PASS, "Select 'Unusual bleeding or discharge of any kind from anybody opening?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber3_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber3_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?''for Member 3: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?' for Member 3: "+ DOB);
			} else {
				System.out.println("'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 3");
				logger.log(LogStatus.FAIL, "'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 3");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 4
		public static void setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber4_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
				System.out.println("Select 'Unusual bleeding or discharge of any kind from anybody opening?'");
				logger.log(LogStatus.PASS, "Select 'Unusual bleeding or discharge of any kind from anybody opening?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber4_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber4_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for'Unusual bleeding or discharge of any kind from anybody opening?'for Member 4: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?' for Member 4: "+ DOB);
			} else {
				System.out.println("'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 4");
				logger.log(LogStatus.FAIL, "'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 4");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 5
		public static void setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber5_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
				System.out.println("Select 'Unusual bleeding or discharge of any kind from anybody opening?'");
				logger.log(LogStatus.PASS, "Select 'Unusual bleeding or discharge of any kind from anybody opening?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber5_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber5_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?'for Member 5: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?' for Member 5: "+ DOB);
			} else {
				System.out.println("'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 5");
				logger.log(LogStatus.FAIL, "'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 5");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 6
		public static void setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber6_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
				System.out.println("Select 'Unusual bleeding or discharge of any kind from anybody opening?'");
				logger.log(LogStatus.PASS, "Select 'Unusual bleeding or discharge of any kind from anybody opening?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber6_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber6_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?'for Member 6: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Unusual bleeding or discharge of any kind from anybody opening?' for Member 6: "+ DOB);
			} else {
				System.out.println("'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 6");
				logger.log(LogStatus.FAIL, "'Unusual bleeding or discharge of any kind from anybody opening?' not selected for Member 6");
			}

		}
		
		// ************************ Existing Since Date Entered of Sub Question 4 ******************************
		// Function for Click on Check Box for First Sub question 4 which are available under first question for Member 1
		public static void setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber1(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateforMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateforMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber1_Xpath));
				System.out.println("Select 'Weight loss more than 5 kg in the last 3 months'");
				logger.log(LogStatus.PASS, "Select 'Weight loss more than 5 kg in the last 3 months'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber1_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber1_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months' for Member 1: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months' for Member 1: "+ DOB);
			} else {
				System.out.println("'Weight loss more than 5 kg in the last 3 months' not selected for Member 1");
				logger.log(LogStatus.FAIL, "'Weight loss more than 5 kg in the last 3 months' not selected for Member 1");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 2
		public static void setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember2) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember2].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber2_Xpath));
				clickByJS(e1);
				
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				System.out.println("Select 'Weight loss more than 5 kg in the last 3 months'");
				logger.log(LogStatus.PASS, "Select 'Weight loss more than 5 kg in the last 3 months'");
				
				
				waitForElement(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber2_Xpath));
				//clickByJS(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber2_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber2_Xpath),
						String.valueOf(DOB));
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months' for Member 2: "+ DOB);

				System.out.println(
						"Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months'for Member 2: "
								+ DOB);
				
			} else {
				System.out.println("'Weight loss more than 5 kg in the last 3 months' not selected for Member 2");
				logger.log(LogStatus.FAIL, "'Weight loss more than 5 kg in the last 3 months' not selected for Member 2");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 3
		public static void setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber3(String SheetName, int Rownum, int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
	           WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber3_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
				System.out.println("Select 'Weight loss more than 5 kg in the last 3 months'");
				logger.log(LogStatus.PASS, "Select 'Weight loss more than 5 kg in the last 3 months'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber3_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber3_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months'for Member 3: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months' for Member 3: "+ DOB);
			} else {
				System.out.println("'Weight loss more than 5 kg in the last 3 months' not selected for Member 3");
				logger.log(LogStatus.FAIL, "'Weight loss more than 5 kg in the last 3 months' not selected for Member 3");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 4
		public static void setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber4_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
				System.out.println("Select 'Weight loss more than 5 kg in the last 3 months'");
				logger.log(LogStatus.PASS, "Select 'Weight loss more than 5 kg in the last 3 months'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber4_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber4_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for'Weight loss more than 5 kg in the last 3 months'for Member 4: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months' for Member 4: "+ DOB);
			} else {
				System.out.println("'Weight loss more than 5 kg in the last 3 months' not selected for Member 4");
				logger.log(LogStatus.FAIL, "'Weight loss more than 5 kg in the last 3 months' not selected for Member 4");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 5
		public static void setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber5_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
				System.out.println("Select 'Weight loss more than 5 kg in the last 3 months'");
				logger.log(LogStatus.PASS, "Select 'Weight loss more than 5 kg in the last 3 months'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber5_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber5_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months'for Member 5: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months' for Member 5: "+ DOB);
			} else {
				System.out.println("'Weight loss more than 5 kg in the last 3 months' not selected for Member 5");
				logger.log(LogStatus.FAIL, "'Weight loss more than 5 kg in the last 3 months' not selected for Member 5");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 6
		public static void setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,
				int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber6_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
				System.out.println("Select 'Weight loss more than 5 kg in the last 3 months'");
				logger.log(LogStatus.PASS, "Select 'Weight loss more than 5 kg in the last 3 months'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber6_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber6_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months'for Member 6: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Weight loss more than 5 kg in the last 3 months' for Member 6: "+ DOB);
			} else {
				System.out.println("'Weight loss more than 5 kg in the last 3 months' not selected for Member 6");
				logger.log(LogStatus.FAIL, "'Weight loss more than 5 kg in the last 3 months' not selected for Member 6");
			}

		}
	
	
	
	// ************************ Existing Since Date Entered of Sub Question 5 ******************************
		// Function for Click on Check Box for First Sub question 5 which are available under first question for Member 1
		public static void setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber1(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateforMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateforMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber1_Xpath));
				System.out.println("Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");
				logger.log(LogStatus.PASS, "Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber1_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber1_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' for Member 1: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' for Member 1: "+ DOB);
			} else {
				System.out.println("'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 1");
				logger.log(LogStatus.FAIL, "'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 1");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 2
		public static void setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber2(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember2) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember2].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {

				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber2_Xpath));
				clickByJS(e1);

				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				System.out.println("Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");
				logger.log(LogStatus.PASS, "Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");

				waitForElement(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber2_Xpath));
				// clickByJS(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber2_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber2_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'for Member 1: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' for Member 2: "+ DOB);
			} else {
				System.out.println("'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 2");
				logger.log(LogStatus.FAIL, "'Weight loss more than 5 kg in the last 3 months' not selected for Member 2");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 3
		public static void setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber3(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber3_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
				System.out.println("Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");
				logger.log(LogStatus.PASS, "Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber3_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber3_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'for Member 3: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' for Member 3: "+ DOB);
			} else {
				System.out.println("'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 3");
				logger.log(LogStatus.FAIL, "'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 3");
			
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 4
		public static void setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber4(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber4_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
				System.out.println("Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");
				logger.log(LogStatus.PASS, "Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber4_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber4_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' for Member 4: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' for Member 4: "+ DOB);
			} else {
				System.out.println("'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 4");
				logger.log(LogStatus.FAIL, "'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 4");
				
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 5
		public static void setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber5(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber5_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
				System.out.println("Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");
				logger.log(LogStatus.PASS, "Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber5_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber5_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'for Member 5: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' for Member 5: "+ DOB);
			} else {
				System.out.println("'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 5");
				logger.log(LogStatus.FAIL, "'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 5");
				
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 6
		public static void setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber6(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber6_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
				System.out.println("Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");
				logger.log(LogStatus.PASS, "Select 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber6_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber6_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?'for Member 6: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' for Member 6: "+ DOB);
			} else {
				System.out.println("'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 6");
				logger.log(LogStatus.FAIL, "'Any growth, cyst, tumor, lump, skin lesion, sarcoma, cancer, in any part of the body?' not selected for Member 6");
			}

		}
		
		// ************************ Existing Since Date Entered of Sub Question 6 ******************************
		// Function for Click on Check Box for First Sub question 6 which are available under first question for Member 1
		public static void setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber1(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateforMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateforMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber1_Xpath));
				System.out.println("Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");
				logger.log(LogStatus.PASS, "Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber1_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber1_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' for Member 1: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' for Member 1: "+ DOB);
			} else {
				System.out.println("'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 1");
				logger.log(LogStatus.FAIL, "'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 1");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 2
		public static void setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber2(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember2) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember2].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {

				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber2_Xpath));
				clickByJS(e1);

				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				System.out.println("Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");
				logger.log(LogStatus.PASS, "Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");

				waitForElement(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber2_Xpath));
				// clickByJS(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber2_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber2_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'for Member 2: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' for Member 2: "+ DOB);
			} else {
				System.out.println("'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 2");
				logger.log(LogStatus.FAIL, "'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 2");
			
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 3
		public static void setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber3(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber3_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
				System.out.println("Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");
				logger.log(LogStatus.PASS, "Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber3_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber3_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'for Member 3: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' for Member 3: "+ DOB);
			} else {
				System.out.println("'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 3");
				logger.log(LogStatus.FAIL, "'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 3");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 4
		public static void setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber4(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber4_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
				System.out.println("Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");
				logger.log(LogStatus.PASS, "Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber4_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber4_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' for Member 4: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' for Member 4: "+ DOB);
			} else {
				System.out.println("'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 4");
				logger.log(LogStatus.FAIL, "'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 4");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 5
		public static void setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber5(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber5_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
				System.out.println("Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");
				logger.log(LogStatus.PASS, "Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber5_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber5_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'for Member 5: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' for Member 5: "+ DOB);
			} else {
				System.out.println("'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 5");
				logger.log(LogStatus.FAIL, "'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 5");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 6
		public static void setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber6(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber6_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
				System.out.println("Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");
				logger.log(LogStatus.PASS, "Select 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber6_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber6_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?'for Member 6: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' for Member 6: "+ DOB);
			} else {
				System.out.println("'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 6");
				logger.log(LogStatus.FAIL, "'Any persistent headache, epileptic fits, sudden vision loss or hearing loss?' not selected for Member 6");
			}

		}
		
		// ************************ Existing Since Date Entered of Sub Question 7 ******************************
		// Function for Click on Check Box for First Sub question 7 which are available under first question for Member 1
		public static void setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber1(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateforMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateforMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber1_Xpath));
				System.out.println("Select 'Any change in usual bowel or bladder habits'");
				logger.log(LogStatus.PASS, "Select 'Any change in usual bowel or bladder habits'");


				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber1_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber1_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any change in usual bowel or bladder habits' for Member 1: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any change in usual bowel or bladder habits' for Member 1: "+ DOB);
			} else {
				System.out.println("'Any change in usual bowel or bladder habits' not selected for Member 1");
				logger.log(LogStatus.FAIL, "'Any change in usual bowel or bladder habits' not selected for Member 1");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 2
		public static void setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber2(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember2) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember2].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {

				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber2_Xpath));
				clickByJS(e1);

				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				System.out.println("Select 'Any change in usual bowel or bladder habits'");
				logger.log(LogStatus.PASS, "Select 'Any change in usual bowel or bladder habits'");

				waitForElement(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber2_Xpath));
				// clickByJS(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber2_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber2_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any change in usual bowel or bladder habits'for Member 1: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any change in usual bowel or bladder habits' for Member 2: "+ DOB);
			} else {
				System.out.println("'Any change in usual bowel or bladder habits' not selected for Member 2");
				logger.log(LogStatus.FAIL, "'Any change in usual bowel or bladder habits' not selected for Member 2");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 3
		public static void setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber3(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber3_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
				System.out.println("Select 'Any change in usual bowel or bladder habits'");
				logger.log(LogStatus.PASS, "Select 'Any change in usual bowel or bladder habits'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber3_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber3_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any change in usual bowel or bladder habits'for Member 3: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any change in usual bowel or bladder habits' for Member 3: "+ DOB);
			} else {
				System.out.println("'Any change in usual bowel or bladder habits' not selected for Member 3");
				logger.log(LogStatus.FAIL, "'Any change in usual bowel or bladder habits' not selected for Member 3");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 4
		public static void setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber4(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber4_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
				System.out.println("Select 'Any change in usual bowel or bladder habits'");
				logger.log(LogStatus.PASS, "Select 'Any change in usual bowel or bladder habits'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber4_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber4_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for'Any change in usual bowel or bladder habits' for Member 4: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any change in usual bowel or bladder habits' for Member 4: "+ DOB);
			} else {
				System.out.println("'Any change in usual bowel or bladder habits' not selected for Member 4");
				logger.log(LogStatus.FAIL, "'Any change in usual bowel or bladder habits' not selected for Member 4");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 5
		public static void setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber5(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber5_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
				System.out.println("Select 'Any change in usual bowel or bladder habits'");
				logger.log(LogStatus.PASS, "Select 'Any change in usual bowel or bladder habits'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber5_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber5_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any change in usual bowel or bladder habits'for Member 5: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any change in usual bowel or bladder habits' for Member 5: "+ DOB);
			} else {
				System.out.println("'Any change in usual bowel or bladder habits not selected for Member 5");
				logger.log(LogStatus.FAIL, "'Any change in usual bowel or bladder habits' not selected for Member 5");
			}

		}

		// Function for Click on Check Box for First Sub question which are available under first question for Member 6
		public static void setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber6(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

			if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver
						.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber6_Xpath));
				clickByJS(e1);
				// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
				System.out.println("Select 'Any change in usual bowel or bladder habits'");
				logger.log(LogStatus.PASS, "Select 'Any change in usual bowel or bladder habits'");

				clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber6_Xpath));
				enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber6_Xpath),
						String.valueOf(DOB));

				System.out.println(
						"Existing Since Date Entered for 'Any change in usual bowel or bladder habits'for Member 6: "
								+ DOB);
				logger.log(LogStatus.PASS, "Existing Since Date Entered for 'Any change in usual bowel or bladder habits' for Member 6: "+ DOB);
			} else {
				System.out.println("'Any change in usual bowel or bladder habits' not selected for Member 6");
				logger.log(LogStatus.FAIL, "'Any change in usual bowel or bladder habits' not selected for Member 6");
			}

		}
	
		// ************************ Existing Since Date Entered of Sub Question 8 ******************************
				// Function for Click on Check Box for First Sub question 7 which are available under first question for Member 1
				public static void setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber1(String SheetName, int Rownum,
						int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateforMember1, int ProvideDetailsColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String DOB = TestCaseData[Rownum][ExistingDiseaseDateforMember1].toString().trim();
					String provideDetailsValue = TestCaseData[Rownum][ProvideDetailsColNum].toString().trim();

					if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
						clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber1_Xpath));
						System.out.println("Select Any other disease / health adversity / injury/ condition / treatment not mentioned above'");
						logger.log(LogStatus.PASS, "Select Any other disease / health adversity / injury/ condition / treatment not mentioned above'");

						clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber1_Xpath));
						enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber1_Xpath),
								String.valueOf(DOB));

						System.out.println(
								"Existing Since Date Entered for 'Any other disease / health adversity / injury/ condition / treatment not mentioned above' for Member 1: "
										+ DOB);
						enterText(By.xpath(ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber1_Xpath),String.valueOf(provideDetailsValue));
						System.out.println("Details Provide: " + provideDetailsValue);
						logger.log(LogStatus.PASS, "Details Provide: " + provideDetailsValue);
						
					} else {
						System.out.println("'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 1");
						logger.log(LogStatus.FAIL, "'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 1");
					}

				}

				// Function for Click on Check Box for First Sub question which are available under first question for Member 2
				public static void setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber2(String SheetName, int Rownum,
						int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember2, int ProvideDetailsColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember2].toString().trim();
					String provideDetailsValue = TestCaseData[Rownum][ProvideDetailsColNum].toString().trim();

					if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {

						WebElement e1 = driver
								.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber2_Xpath));
						clickByJS(e1);

						// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
						System.out.println("Select 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'");
						logger.log(LogStatus.PASS, "Select Any other disease / health adversity / injury/ condition / treatment not mentioned above'");

						waitForElement(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber2_Xpath));
						// clickByJS(By.xpath(sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath));
						clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber2_Xpath));
						enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber2_Xpath),
								String.valueOf(DOB));

						System.out.println(
								"Existing Since Date Entered for 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'for Member 1: "
										+ DOB);
						enterText(By.xpath(ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber2_Xpath),String.valueOf(provideDetailsValue));
						System.out.println("Details Provided: " + provideDetailsValue);
						logger.log(LogStatus.PASS, "Details Provide: " + provideDetailsValue);
						
					} else {
						System.out.println("'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 2");
					}

				}

				// Function for Click on Check Box for First Sub question which are available under first question for Member 3
				public static void setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber3(String SheetName, int Rownum,
						int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1, int ProvideDetailsColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();
					String provideDetailsValue = TestCaseData[Rownum][ProvideDetailsColNum].toString().trim();

					if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
						WebElement e1 = driver
								.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber3_Xpath));
						clickByJS(e1);
						// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath));
						System.out.println("Select 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'");
						logger.log(LogStatus.PASS, "Select Any other disease / health adversity / injury/ condition / treatment not mentioned above'");

						clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber3_Xpath));
						enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber3_Xpath),
								String.valueOf(DOB));

						System.out.println(
								"Existing Since Date Entered for 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'for Member 3: "
										+ DOB);
						enterText(By.xpath(ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber3_Xpath),String.valueOf(provideDetailsValue));
						System.out.println("Details Provided: " + provideDetailsValue);
						logger.log(LogStatus.PASS, "Details Provide: " + provideDetailsValue);
						
					} else {
						System.out.println("'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 3");
						logger.log(LogStatus.FAIL, "'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 3");
					}

				}

				// Function for Click on Check Box for First Sub question which are available under first question for Member 4
				public static void setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber4(String SheetName, int Rownum,
						int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1, int ProvideDetailsColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();
					String provideDetailsValue = TestCaseData[Rownum][ProvideDetailsColNum].toString().trim();

					if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
						WebElement e1 = driver
								.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber4_Xpath));
						clickByJS(e1);
						// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath));
						System.out.println("Select 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'");
						logger.log(LogStatus.PASS, "Select Any other disease / health adversity / injury/ condition / treatment not mentioned above'");

						clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber4_Xpath));
						enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber4_Xpath),
								String.valueOf(DOB));

						System.out.println(
								"Existing Since Date Entered for'Any other disease / health adversity / injury/ condition / treatment not mentioned above' for Member 4: "
										+ DOB);
						enterText(By.xpath(ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber4_Xpath),String.valueOf(provideDetailsValue));
						System.out.println("Details Provided: " + provideDetailsValue);
						logger.log(LogStatus.PASS, "Details Provide: " + provideDetailsValue);
					} else {
						System.out.println("'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 4");
						logger.log(LogStatus.FAIL, "'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 4");
					}

				}

				// Function for Click on Check Box for First Sub question which are available under first question for Member 5
				public static void setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber5(String SheetName, int Rownum,
						int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1, int ProvideDetailsColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();

					String provideDetailsValue = TestCaseData[Rownum][ProvideDetailsColNum].toString().trim();
					
					if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
						WebElement e1 = driver
								.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber5_Xpath));
						clickByJS(e1);
						// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath));
						System.out.println("Select 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'");
						logger.log(LogStatus.PASS, "Select Any other disease / health adversity / injury/ condition / treatment not mentioned above'");

						clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber5_Xpath));
						enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber5_Xpath),
								String.valueOf(DOB));

						System.out.println(
								"Existing Since Date Entered for 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'for Member 5: "
										+ DOB);
						
						enterText(By.xpath(ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber5_Xpath),String.valueOf(provideDetailsValue));
						System.out.println("Details Provided: " + provideDetailsValue);
						logger.log(LogStatus.PASS, "Details Provide: " + provideDetailsValue);
					} else {
						System.out.println("'Any other disease / health adversity / injury/ condition / treatment not mentioned above");
						logger.log(LogStatus.FAIL, "'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 5");
					}

				}

				// Function for Click on Check Box for First Sub question which are available under first question for Member 6
				public static void setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber6(String SheetName, int Rownum,
						int subQuestionCheckBoxValueColNum, int ExistingDiseaseDateMember1, int ProvideDetailsColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String sinceExistingCheckBox = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String DOB = TestCaseData[Rownum][ExistingDiseaseDateMember1].toString().trim();
					
					String provideDetailsValue = TestCaseData[Rownum][ProvideDetailsColNum].toString().trim();

					if (sinceExistingCheckBox.equalsIgnoreCase("Yes")) {
						WebElement e1 = driver
								.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber6_Xpath));
						clickByJS(e1);
						// clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath));
						System.out.println("Select 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'");
						logger.log(LogStatus.PASS, "Select Any other disease / health adversity / injury/ condition / treatment not mentioned above'");

						clickElement(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber6_Xpath));
						enterText(By.xpath(sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber6_Xpath),
								String.valueOf(DOB));

						System.out.println(
								"Existing Since Date Entered for 'Any other disease / health adversity / injury/ condition / treatment not mentioned above'for Member 6: "
										+ DOB);
						
						enterText(By.xpath(ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber6_Xpath),String.valueOf(provideDetailsValue));
						System.out.println("Details Provided: " + provideDetailsValue);
						logger.log(LogStatus.PASS, "Details Provide: " + provideDetailsValue);
						
					} else {
						System.out.println("'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 6");
						logger.log(LogStatus.FAIL, "'Any other disease / health adversity / injury/ condition / treatment not mentioned above' not selected for Member 6");
					}

				}
			
	//***************************  End *********************************************************************
	//******************   Set Data for Sub Questions ***************************************
	
	//Function for set Data for Sub question 1 under Question 1  
	public static void setDatafor_Question1(String SheetName, int rowNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String question1 = TestCaseData[rowNum][1].toString().trim(); //Question Type (Yes/No) Selected
		
		String[][] TestCaseData2 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
		String noOfMembers = TestCaseData2[rowNum][5].toString().trim();   //Total Number of Count,  Members Selected from Quotation Page (First Page) based on Number we will enter Existing Disease Date
		
		int CheckBoxValueColNum_subQuestion1=2;
		int ExistingDiseaseDatefor_Subquuestion1_Of_Member1 =3;
		int ExistingDiseaseDatefor_Subquuestion1_Of_Member2 =4;
		int ExistingDiseaseDatefor_Subquuestion1_Of_Member3 =5;
		int ExistingDiseaseDatefor_Subquuestion1_Of_Member4 =6;
		int ExistingDiseaseDatefor_Subquuestion1_Of_Member5 =7;
		int ExistingDiseaseDatefor_Subquuestion1_Of_Member6 =8;
		
		int CheckBoxValueColNum_subQuestion2=9;
		int ExistingDiseaseDatefor_Subquuestion2_Of_Member1 =10;
		int ExistingDiseaseDatefor_Subquuestion2_Of_Member2 =11;
		int ExistingDiseaseDatefor_Subquuestion2_Of_Member3 =12;
		int ExistingDiseaseDatefor_Subquuestion2_Of_Member4 =13;
		int ExistingDiseaseDatefor_Subquuestion2_Of_Member5 =14;
		int ExistingDiseaseDatefor_Subquuestion2_Of_Member6 =15;
		
		int CheckBoxValueColNum_subQuestion3=16;
		int ExistingDiseaseDatefor_Subquuestion3_Of_Member1 =17;
		int ExistingDiseaseDatefor_Subquuestion3_Of_Member2 =18;
		int ExistingDiseaseDatefor_Subquuestion3_Of_Member3 =19;
		int ExistingDiseaseDatefor_Subquuestion3_Of_Member4 =20;
		int ExistingDiseaseDatefor_Subquuestion3_Of_Member5 =21;
		int ExistingDiseaseDatefor_Subquuestion3_Of_Member6 =22;
		
		int CheckBoxValueColNum_subQuestion4=23;
		int ExistingDiseaseDatefor_Subquuestion4_Of_Member1 =24;
		int ExistingDiseaseDatefor_Subquuestion4_Of_Member2 =25;
		int ExistingDiseaseDatefor_Subquuestion4_Of_Member3 =26;
		int ExistingDiseaseDatefor_Subquuestion4_Of_Member4 =27;
		int ExistingDiseaseDatefor_Subquuestion4_Of_Member5 =28;
		int ExistingDiseaseDatefor_Subquuestion4_Of_Member6 =29;
		
		int CheckBoxValueColNum_subQuestion5=30;
		int ExistingDiseaseDatefor_Subquuestion5_Of_Member1 =31;
		int ExistingDiseaseDatefor_Subquuestion5_Of_Member2 =32;
		int ExistingDiseaseDatefor_Subquuestion5_Of_Member3 =33;
		int ExistingDiseaseDatefor_Subquuestion5_Of_Member4 =34;
		int ExistingDiseaseDatefor_Subquuestion5_Of_Member5 =35;
		int ExistingDiseaseDatefor_Subquuestion5_Of_Member6 =36;
		
		int CheckBoxValueColNum_subQuestion6=37;
		int ExistingDiseaseDatefor_Subquuestion6_Of_Member1 =38;
		int ExistingDiseaseDatefor_Subquuestion6_Of_Member2 =39;
		int ExistingDiseaseDatefor_Subquuestion6_Of_Member3 =40;
		int ExistingDiseaseDatefor_Subquuestion6_Of_Member4 =41;
		int ExistingDiseaseDatefor_Subquuestion6_Of_Member5 =42;
		int ExistingDiseaseDatefor_Subquuestion6_Of_Member6 =43;
		
		int CheckBoxValueColNum_subQuestion7=44;
		int ExistingDiseaseDatefor_Subquuestion7_Of_Member1 =45;
		int ExistingDiseaseDatefor_Subquuestion7_Of_Member2 =46;
		int ExistingDiseaseDatefor_Subquuestion7_Of_Member3 =47;
		int ExistingDiseaseDatefor_Subquuestion7_Of_Member4 =48;
		int ExistingDiseaseDatefor_Subquuestion7_Of_Member5 =49;
		int ExistingDiseaseDatefor_Subquuestion7_Of_Member6 =50;
		
		//Sub Question 8
		int CheckBoxValueColNum_subQuestion8=51;
		
		int ExistingDiseaseDatefor_Subquuestion8_Of_Member1 =52;
		int ProvideDetailsfor_Subquuestion8_Of_Member1 =53;
		
		int ExistingDiseaseDatefor_Subquuestion8_Of_Member2 =54;
		int ProvideDetailsfor_Subquuestion8_Of_Member2 =55;
		
		int ExistingDiseaseDatefor_Subquuestion8_Of_Member3 =56;
		int ProvideDetailsfor_Subquuestion8_Of_Member3 =57;
		
		int ExistingDiseaseDatefor_Subquuestion8_Of_Member4 =58;
		int ProvideDetailsfor_Subquuestion8_Of_Member4  =59;
		
		int ExistingDiseaseDatefor_Subquuestion8_Of_Member5 =60;
		int ProvideDetailsfor_Subquuestion8_Of_Member5 =61;
		
		int ExistingDiseaseDatefor_Subquuestion8_Of_Member6 =62;
		int ProvideDetailsfor_Subquuestion8_Of_Member6 =63;
		
		//This If Using When User select Option 'Yes' from Question 1
		if(question1.equalsIgnoreCase("Yes")){
			
			 waitForElements(By.xpath(superMediclaimCancer_Quest1_YesButton_Xpath));
			 clickElement(By.xpath(superMediclaimCancer_Quest1_YesButton_Xpath));
			 
			 //This Switch Logic for Based on Number of Members, Existing Disease date Entered
			 switch(noOfMembers){
			 case "1": 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member1); //SubQuestion 1
				 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member1); //SubQuestion 2
				 
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member1); //SubQuestion 3				
				 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member1); //SubQuestion 4
				 
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member1); //SubQuestion 5
				 
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member1); //SubQuestion 6
				 
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member1); //SubQuestion 7
				 
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member1, ProvideDetailsfor_Subquuestion8_Of_Member1); //SubQuestion 8
				 
				 break;
			 case "2": 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member1); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member2); 
				
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member1); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member2); 
				
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member1); 
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member2);
				 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member1); 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member2);
				
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member1); //SubQuestion 5
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member2);
				
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member1); //SubQuestion 6
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member2);
				
				 
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member1); //SubQuestion 7
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member2);
				 
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member1, ProvideDetailsfor_Subquuestion8_Of_Member1); //SubQuestion 8
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member2, ProvideDetailsfor_Subquuestion8_Of_Member2);
				 break;
				 
			 case "3": 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member1); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member2); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member3); 
				
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member1); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member2); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member3);
				 
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member1); 
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member2);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member3);
				
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member1); 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member2);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member3);
				
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member1); //SubQuestion 5
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member2);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member3);
				
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member1); //SubQuestion 6
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member2);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member3);
				
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member1); //SubQuestion 7
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member2);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member3);
				 
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member1, ProvideDetailsfor_Subquuestion8_Of_Member1); //SubQuestion 8
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member2, ProvideDetailsfor_Subquuestion8_Of_Member2);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member3, ProvideDetailsfor_Subquuestion8_Of_Member3);
				 break;
				 
			 case "4": 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member1); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member2); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member3); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member4); 
				
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member1); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member2); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member3); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member4); 
				
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member1); 
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member2);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member3);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member4);
				
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member1); 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member2);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member3);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member4);
				
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member1); //SubQuestion 5
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member2);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member3);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member4);
				
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member1); //SubQuestion 6
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member2);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member3);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member4);
				 
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member1); //SubQuestion 7
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member2);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member3);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member4);
				 
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member1, ProvideDetailsfor_Subquuestion8_Of_Member1); //SubQuestion 8
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member2, ProvideDetailsfor_Subquuestion8_Of_Member2);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member3, ProvideDetailsfor_Subquuestion8_Of_Member3);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member4, ProvideDetailsfor_Subquuestion8_Of_Member4);
				 break;
				 
			 case "5": 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member1); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member2); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member3); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member4); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member5); 
				
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member1); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member2); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member3); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member4); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member5); 
				
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member1); 
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member2);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member3);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member4);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member5); 
				 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member1); 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member2);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member3);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member4);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member5); 
				
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member1); //SubQuestion 5
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member2);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member3);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member4);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member5); 
				
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member1); //SubQuestion 6
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member2);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member3);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member4);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member5); 
				
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member1); //SubQuestion 7
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member2);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member3);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member4);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member5); 
				 
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member1, ProvideDetailsfor_Subquuestion8_Of_Member1); //SubQuestion 8
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member2, ProvideDetailsfor_Subquuestion8_Of_Member2);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member3, ProvideDetailsfor_Subquuestion8_Of_Member3);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member4, ProvideDetailsfor_Subquuestion8_Of_Member4);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member5, ProvideDetailsfor_Subquuestion8_Of_Member5); 
				 break;
			 
			 case "6": 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member1); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member2); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member3); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member4); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member5); 
				 setExistingSinceDate_SubQues1_UnderQuestion1_ForMemeber6(SheetName, rowNum,CheckBoxValueColNum_subQuestion1, ExistingDiseaseDatefor_Subquuestion1_Of_Member6); 
				
				 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber1(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member1); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member2); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member3); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member4); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member5); 
				 setExistingSinceDate_SubQues2_UnderQuestion1_ForMemeber6(SheetName, rowNum,CheckBoxValueColNum_subQuestion2, ExistingDiseaseDatefor_Subquuestion2_Of_Member6); 
				
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member1); 
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member2);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member3);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member4);
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member5); 
				 setExistingSinceDate_SubQues3_UnderQuestion1_ForMemeber6(SheetName, rowNum,CheckBoxValueColNum_subQuestion3, ExistingDiseaseDatefor_Subquuestion3_Of_Member6); 
				 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member1); 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member2);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member3);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member4);
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member5); 
				 setExistingSinceDate_SubQues4_UnderQuestion1_ForMemeber6(SheetName, rowNum,CheckBoxValueColNum_subQuestion4, ExistingDiseaseDatefor_Subquuestion4_Of_Member6); 
				
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member1); //SubQuestion 5
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member2);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member3);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member4);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member5);
				 setExistingSinceDate_SubQues5_UnderQuestion1_ForMemeber6(SheetName, rowNum,CheckBoxValueColNum_subQuestion5, ExistingDiseaseDatefor_Subquuestion5_Of_Member6); 
				
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member1); //SubQuestion 6
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member2);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member3);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member4);
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member5); 
				 setExistingSinceDate_SubQues6_UnderQuestion1_ForMemeber6(SheetName, rowNum,CheckBoxValueColNum_subQuestion6, ExistingDiseaseDatefor_Subquuestion6_Of_Member6); 
				 
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member1); //SubQuestion 7
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member2);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member3);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member4);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member5);
				 setExistingSinceDate_SubQues7_UnderQuestion1_ForMemeber6(SheetName, rowNum,CheckBoxValueColNum_subQuestion7, ExistingDiseaseDatefor_Subquuestion7_Of_Member6); 
				 
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber1(SheetName, rowNum, CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member1, ProvideDetailsfor_Subquuestion8_Of_Member1); //SubQuestion 8
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber2(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member2, ProvideDetailsfor_Subquuestion8_Of_Member2);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber3(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member3, ProvideDetailsfor_Subquuestion8_Of_Member3);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber4(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member4, ProvideDetailsfor_Subquuestion8_Of_Member4);
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber5(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member5, ProvideDetailsfor_Subquuestion8_Of_Member5); 
				 setExistingSinceDate_SubQues8_UnderQuestion1_ForMemeber6(SheetName, rowNum,CheckBoxValueColNum_subQuestion8, ExistingDiseaseDatefor_Subquuestion8_Of_Member6, ProvideDetailsfor_Subquuestion8_Of_Member6); 
				 break;
			 }
					
		}
		else{
			
			//waitForElements(By.xpath(superMediclaimCancer_Quest1_NoButton_Xpath));
			clickElement(By.xpath(superMediclaimCancer_Quest1_NoButton_Xpath));      //This function called when User select Option 'No' from Question 1
		}
	}
	
	 //*************************************** Functions End of Question Set 1 ***************************************************************************************
	
	//*************************************** Functions Start for Question Set 2 ***************************************************************************************
	
	//Question Set Type 2 Disease Message
	public static String QuestionSet2Message="Have you ever suffered from or been treated for any form of symptoms of (a) Cancer (b) Heart disease or heart attack (c) Stroke (d) Chest and/or heart surgery, or have been advised medically to undergo chest and/or heart surgery in the future (e) Kidney disease (f) Liver disease including hepatitis (g) Kidney and / or liver failure (h) Paralysis or paraplegia (l) Major organ transplantation, or have been advised to undergo a major organ transplantation (such as for example heart, lung, liver or kidney etc) in the future, (j) Any neurological or nervous disorders (k) HIV infections, AIDS or venereal diseases (k) Disorder of the bones, spine or muscle Cancer, tumor, polyp or cyst";
	
	//Member 1
	public static void setDetailsOfQuestionSet2_forMemeber1(String sheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(sheetName);
		String question2 = TestCaseData[rowNum][colNum].toString().trim();
		
		if(question2.equalsIgnoreCase("Yes")){
			
		WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues2_ForMemeber1_Xpath));
		clickByJS(e1);
		//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber1_Xpath));
		 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1");
		 logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1");
		 
		}
		else{
			 System.out.println("Not selected Question Set 2 Type of disease for Member 1");
			 logger.log(LogStatus.FAIL, "Not selected Question Set 2 Type of disease for Member 1");
		}
	
	   
	}
	//Member 2
	public static void setDetailsOfQuestionSet2_forMemeber2(String sheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(sheetName);
		String question2 = TestCaseData[rowNum][colNum].toString().trim();
		
		if(question2.equalsIgnoreCase("Yes")){
			
			WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues2_ForMemeber2_Xpath));
			clickByJS(e1);
		//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber2_Xpath));
		 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 2");
		 logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1");
		}
		else{
			 System.out.println("Not selected Question Set 2 Type of disease for Member 2");
			 logger.log(LogStatus.FAIL, "Not selected Question Set 2 Type of disease for Member 1");
		}
	
	   
	}
	
	//Member 3
		public static void setDetailsOfQuestionSet2_forMemeber3(String sheetName, int rowNum, int colNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();
			
			if(question2.equalsIgnoreCase("Yes")){
			WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues2_ForMemeber3_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber3_Xpath));
			 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 3");
			 logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 3");
			}
			else{
				 System.out.println("Not selected Question Set 2 Type of disease for Member 3");
				 logger.log(LogStatus.FAIL, "Not selected Question Set 2 Type of disease for Member 3");
			}
		
		   
		}
		
	// Member 4
	public static void setDetailsOfQuestionSet2_forMemeber4(String sheetName, int rowNum, int colNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(sheetName);
		String question2 = TestCaseData[rowNum][colNum].toString().trim();

		if (question2.equalsIgnoreCase("Yes")) {
			WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues2_ForMemeber4_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber4_Xpath));
			System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 4");
			logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 4");
		} else {
			System.out.println("Not selected Question Set 2 Type of disease for Member 4");
			 logger.log(LogStatus.FAIL, "Not selected Question Set 2 Type of disease for Member 4");
		}

	}
	
	// Member 5
		public static void setDetailsOfQuestionSet2_forMemeber5(String sheetName, int rowNum, int colNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();

			if (question2.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues2_ForMemeber5_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber5_Xpath));
				System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 5");
				logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 5");
			} else {
				System.out.println("Not selected Question Set 2 Type of disease for Member 5");
				 logger.log(LogStatus.FAIL, "Not selected Question Set 2 Type of disease for Member 5");
			}

		}
		
	// Member 6
	public static void setDetailsOfQuestionSet2_forMemeber6(String sheetName, int rowNum, int colNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(sheetName);
		String question2 = TestCaseData[rowNum][colNum].toString().trim();

		if (question2.equalsIgnoreCase("Yes")) {
			WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues2_ForMemeber6_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber6_Xpath));
			System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 6");
			logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 6");
		} else {
			System.out.println("Not selected Question Set 2 Type of disease for Member 6");
			logger.log(LogStatus.FAIL, "Not selected Question Set 2 Type of disease for Member 6");
		}

	}

	//********** Main Function for Set Data of Question 2 ***********************************
	public static void setDataforQuestion2(String SheetName, int rowNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String question2 = TestCaseData[rowNum][64].toString().trim();
		
		String[][] TestCaseData2 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
		String noOfMembers = TestCaseData2[rowNum][5].toString().trim();   //Total Number of Count,  Members Selected from Quotation Page (First Page) based on Number we will enter Existing Disease Date

		int colNumforMemeber1=65;
		int colNumforMemeber2=66;
		int colNumforMemeber3=67;
		int colNumforMemeber4=68;
		int colNumforMemeber5=69;
		int colNumforMemeber6=70;
		
		if (question2.equalsIgnoreCase("Yes")) {
			 waitForElements(By.xpath(superMediclaimCancer_Quest2_YesButton_Xpath));
			 clickElement(By.xpath(superMediclaimCancer_Quest2_YesButton_Xpath));
			 
			 switch(noOfMembers){
			 case "1":
				 setDetailsOfQuestionSet2_forMemeber1(SheetName ,rowNum, colNumforMemeber1);
				 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1");
				 break;
			 
			 case "2":
				 setDetailsOfQuestionSet2_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1 and 2");
				 break;
			 
			 case "3":
				 setDetailsOfQuestionSet2_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber3(SheetName,rowNum, colNumforMemeber3);
				 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1, 2 and 3");
				 break;
			 
			 case "4":
				 setDetailsOfQuestionSet2_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber3(SheetName,rowNum, colNumforMemeber3);
				 setDetailsOfQuestionSet2_forMemeber4(SheetName,rowNum, colNumforMemeber4);
				 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1, 2, 3 and 4");
			     break;
			 case"5":
				 setDetailsOfQuestionSet2_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber3(SheetName,rowNum, colNumforMemeber3);
				 setDetailsOfQuestionSet2_forMemeber4(SheetName,rowNum, colNumforMemeber4);
				 setDetailsOfQuestionSet2_forMemeber5(SheetName,rowNum, colNumforMemeber5);
				 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1, 2, 3, 4 and 5");
				 break;
			 case "6":
				 setDetailsOfQuestionSet2_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber3(SheetName,rowNum, colNumforMemeber3);
				 setDetailsOfQuestionSet2_forMemeber4(SheetName,rowNum, colNumforMemeber4);
				 setDetailsOfQuestionSet2_forMemeber5(SheetName,rowNum, colNumforMemeber5);
				 setDetailsOfQuestionSet2_forMemeber6(SheetName,rowNum, colNumforMemeber6);
				 System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1, 2, 3, 4 , 5 and 6");
				 break;	
			 
			 }

		} else {

			waitForElements(By.xpath(superMediclaimCancer_Quest2_NoButton_Xpath));
			clickElement(By.xpath(superMediclaimCancer_Quest2_NoButton_Xpath));
			System.out.println("No Question Set 2 Type of disease is selected for any Member");
			logger.log(LogStatus.PASS, "No Question Set 2 Type of disease is selected for any Member");
		}
	}
	
	//********** End Function for Set Data of Question 2 ******************************************************
	
	//********** Start Function for Set Data of Question 3 ******************************************************
	
	//Question Set Type 2 Disease Message
		public static String QuestionSet3Message="Has any of your parents, brothers or sisters been diagnosed of heart ailment, cancer, Hereditary disease prior to age 60 or any hereditary or chronic disorder?";
		
		//Member 1
		public static void setDetailsOfQuestionSet3_forMemeber1(String sheetName, int rowNum, int colNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();
			
			if(question2.equalsIgnoreCase("Yes")){
				
			WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues3_ForMemeber1_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber1_Xpath));
			System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1");
			logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 1");
			}
			else{
				 System.out.println("Not selected Question Set 3 Type of disease for Member 1");
				 logger.log(LogStatus.FAIL, "Not selected Question Set 3 Type of disease for Member 1");
				 
			}
		
		   
		}
		//Member 2
		public static void setDetailsOfQuestionSet3_forMemeber2(String sheetName, int rowNum, int colNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();
			
			if(question2.equalsIgnoreCase("Yes")){
				
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues3_ForMemeber2_Xpath));
				clickByJS(e1);
			//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber2_Xpath));
			// System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 2");
				System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 2");
				logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 2");
			}
			else{
				 System.out.println("Not selected Question Set 3 Type of disease for Member 2");
				 logger.log(LogStatus.FAIL, "Not selected Question Set 3 Type of disease for Member 2");
			}
		
		   
		}
		
		//Member 3
			public static void setDetailsOfQuestionSet3_forMemeber3(String sheetName, int rowNum, int colNum) throws Exception{
				String[][] TestCaseData = BaseClass.excel_Files(sheetName);
				String question2 = TestCaseData[rowNum][colNum].toString().trim();
				
				if(question2.equalsIgnoreCase("Yes")){
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues3_ForMemeber3_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber3_Xpath));
				 //System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 2");
				System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 3");
				logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 3");
				}
				else{
					 System.out.println("Not selected Question Set 3 Type of disease for Member 3");
					 logger.log(LogStatus.FAIL, "Not selected Question Set 3 Type of disease for Member 3");
				}
			
			   
			}
			
		// Member 4
		public static void setDetailsOfQuestionSet3_forMemeber4(String sheetName, int rowNum, int colNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();

			if (question2.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues3_ForMemeber4_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber4_Xpath));
				System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 4");
				logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 4");
			} else {
				System.out.println("Not selected Question Set 3 Type of disease for Member 4");
				 logger.log(LogStatus.FAIL, "Not selected Question Set 3 Type of disease for Member 4");
			}

		}
		
		// Member 5
			public static void setDetailsOfQuestionSet3_forMemeber5(String sheetName, int rowNum, int colNum) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(sheetName);
				String question2 = TestCaseData[rowNum][colNum].toString().trim();

				if (question2.equalsIgnoreCase("Yes")) {
					WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues3_ForMemeber5_Xpath));
					clickByJS(e1);
					//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber5_Xpath));
					System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 5");
					logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 5");
				} else {
					System.out.println("Not selected Question Set 3 Type of disease for Member 5");
					logger.log(LogStatus.FAIL, "Not selected Question Set 3 Type of disease for Member 5");
				}

			}
			
		// Member 6
		public static void setDetailsOfQuestionSet3_forMemeber6(String sheetName, int rowNum, int colNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();

			if (question2.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues3_ForMemeber6_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber6_Xpath));
				System.out.println(QuestionSet2Message + "selected Question Set 2 Type of disease for Member 6");
				logger.log(LogStatus.PASS, QuestionSet2Message + "selected Question Set 2 Type of disease for Member 6");
			} else {
				System.out.println("Not selected Question Set 3 Type of disease for Member 6");
				logger.log(LogStatus.PASS, "Not selected Question Set 3 Type of disease for Member 5");
			}

		}

	
	// Function for set question 3
	public static void setDataforQuestion3(String SheetName, int rowNum) throws Exception {
		
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String question3 = TestCaseData[rowNum][71].toString().trim();
		
		String[][] TestCaseData2 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
		String noOfMembers = TestCaseData2[rowNum][5].toString().trim();   //Total Number of Count,  Members Selected from Quotation Page (First Page) based on Number we will enter Existing Disease Date

		int colNumforMemeber1=72;
		int colNumforMemeber2=73;
		int colNumforMemeber3=74;
		int colNumforMemeber4=75;
		int colNumforMemeber5=76;
		int colNumforMemeber6=77;
		
		if (question3.equalsIgnoreCase("Yes")) {
			waitForElements(By.xpath(superMediclaimCancer_Quest3_YesButton_Xpath));
			clickElement(By.xpath(superMediclaimCancer_Quest3_YesButton_Xpath));
			 
			 switch(noOfMembers){
			 case "1":
				 setDetailsOfQuestionSet3_forMemeber1(SheetName ,rowNum, colNumforMemeber1);
				 System.out.println(QuestionSet3Message + "selected Question Set 2 Type of disease for Member 1");
				 break;
			 
			 case "2":
				 setDetailsOfQuestionSet3_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet2_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 System.out.println(QuestionSet3Message + "selected Question Set 2 Type of disease for Member 1 and 2");
				 break;
			 
			 case "3":
				 setDetailsOfQuestionSet3_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet3_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet3_forMemeber3(SheetName,rowNum, colNumforMemeber3);
				 System.out.println(QuestionSet3Message + "selected Question Set 2 Type of disease for Member 1, 2 and 3");
				 break;
			 
			 case "4":
				 setDetailsOfQuestionSet3_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet3_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet3_forMemeber3(SheetName,rowNum, colNumforMemeber3);
				 setDetailsOfQuestionSet3_forMemeber4(SheetName,rowNum, colNumforMemeber4);
				 System.out.println(QuestionSet3Message + "selected Question Set 2 Type of disease for Member 1, 2, 3 and 4");
			     break;
			 case"5":
				 setDetailsOfQuestionSet3_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet3_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet3_forMemeber3(SheetName,rowNum, colNumforMemeber3);
				 setDetailsOfQuestionSet3_forMemeber4(SheetName,rowNum, colNumforMemeber4);
				 setDetailsOfQuestionSet3_forMemeber5(SheetName,rowNum, colNumforMemeber5);
				 System.out.println(QuestionSet3Message + "selected Question Set 2 Type of disease for Member 1, 2, 3, 4 and 5");
				 break;
			 case "6":
				 setDetailsOfQuestionSet3_forMemeber1(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet3_forMemeber2(SheetName,rowNum, colNumforMemeber2);
				 setDetailsOfQuestionSet3_forMemeber3(SheetName,rowNum, colNumforMemeber3);
				 setDetailsOfQuestionSet3_forMemeber4(SheetName,rowNum, colNumforMemeber4);
				 setDetailsOfQuestionSet3_forMemeber5(SheetName,rowNum, colNumforMemeber5);
				 setDetailsOfQuestionSet3_forMemeber6(SheetName,rowNum, colNumforMemeber6);
				 System.out.println(QuestionSet3Message + "selected Question Set 2 Type of disease for Member 1, 2, 3, 4 , 5 and 6");
				 break;	
			 
			 }

		} else {

			waitForElements(By.xpath(superMediclaimCancer_Quest3_NoButton_Xpath));
			clickElement(By.xpath(superMediclaimCancer_Quest3_NoButton_Xpath));
			System.out.println("No Question Set 3 Type of disease is selected for any Member");
			logger.log(LogStatus.PASS, "No Question Set 3 Type of disease is selected for any Member");
		}
	}
	
	//********** End Function for Set Data of Question 3 ******************************************************
	
	//************************  Start Functions for Question Set 4 ****************************** 
	
	// Sub Question 1 for Member 1
	public static void setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber1(String SheetName, int Rownum,
			int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues1_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

		if (Questtion4_SybQues1_CheckBoxValue.equalsIgnoreCase("Yes")) {
			
			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber1_Xpath));
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 1");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 1");
			
		} else {
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 1");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 1");
		}

	}
			
	// Sub Question 1 for Member 2
	public static void setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues1_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

		if (Questtion4_SybQues1_CheckBoxValue.equalsIgnoreCase("Yes")) {
			
			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber2_Xpath));
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture Selected for Member 1");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 2");

		} else {
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 2");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 2");
		}

	}

	// Sub Question 1 for Member 3
	public static void setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber3(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues1_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

		if (Questtion4_SybQues1_CheckBoxValue.equalsIgnoreCase("Yes")) {
			
			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber3_Xpath));
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture Selected for Member 3");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 3");

		} else {
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 3");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 3");
		}

	}
				
	// Sub Question 1 for Member 4
	public static void setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues1_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

		if (Questtion4_SybQues1_CheckBoxValue.equalsIgnoreCase("Yes")) {
			
			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber4_Xpath));
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture Selected for Member 4");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 4");

		} else {
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 4");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 4");
		}

	}
	// Sub Question 1 for Member 5
	public static void setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues1_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

		if (Questtion4_SybQues1_CheckBoxValue.equalsIgnoreCase("Yes")) {
			
			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber5_Xpath));
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture Selected for Member 5");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 5");

		} else {
			System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 5");
			logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 5");
		}

	}
				
	// Sub Question 1 for Member 6
		public static void setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String Questtion4_SybQues1_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

			if (Questtion4_SybQues1_CheckBoxValue.equalsIgnoreCase("Yes")) {
				
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber6_Xpath));
				System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture Selected for Member 6");
				logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 6");

			} else {
				System.out.println("'Been continuously hospitalized for more than 7 days other than minor fracture' not selected for Member 6");
				logger.log(LogStatus.PASS, "'Been continuously hospitalized for more than 7 days other than minor fracture selected for Member 6");
			}

		}
		
			
			//*************************** Sub Question 2
			// Sub Question 2 for Member 1
			public static void setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber1(String SheetName, int Rownum,
					int subQuestionCheckBoxValueColNum) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);

				String Questtion4_SybQues2_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

				if (Questtion4_SybQues2_CheckBoxValue.equalsIgnoreCase("Yes")) {
					
					clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber1_Xpath));
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 1");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 1");
				} else {
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 1");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 1");
				}

			}
		
			// Sub Question 2 for Member 2
			public static void setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);

				String Questtion4_SybQues2_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

				if (Questtion4_SybQues2_CheckBoxValue.equalsIgnoreCase("Yes")) {
					
					clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber2_Xpath));
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 2");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 2");
				
				} else {
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 2");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 2");
				}

			}

			// Sub Question 2 for Member 3
			public static void setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber3(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);

				String Questtion4_SybQues2_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

				if (Questtion4_SybQues2_CheckBoxValue.equalsIgnoreCase("Yes")) {
					
					clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber3_Xpath));
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 3");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 3");
				} else {
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 3");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 3");
				}

			}
						
			// Sub Question 2 for Member 4
			public static void setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);

				String Questtion4_SybQues2_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

				if (Questtion4_SybQues2_CheckBoxValue.equalsIgnoreCase("Yes")) {
					
					clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber4_Xpath));
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 4");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 4");
					
				} else {
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 4");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 3");
				}

			}
			// Sub Question 2 for Member 5
			public static void setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);

				String Questtion4_SybQues2_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

				if (Questtion4_SybQues2_CheckBoxValue.equalsIgnoreCase("Yes")) {
					
					clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber5_Xpath));
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 5");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 5");

				} else {
					System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 5");
					logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 5");
				}

			}
						
			// Sub Question 2 for Member 6
				public static void setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String Questtion4_SybQues2_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();

					if (Questtion4_SybQues2_CheckBoxValue.equalsIgnoreCase("Yes")) {
						
						clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber6_Xpath));
						System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 6");
                       logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 6");

					} else {
						System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 6");
						logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 6");
					}

				}
				
	// *************************** Sub Question 3
	// Sub Question 3 for Member 1
	public static void setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber1(String SheetName, int Rownum,
			int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues3_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString()
				.trim();

		if (Questtion4_SybQues3_CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber1_Xpath));
			System.out.println("'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 1");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 1");

		} else {
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 1");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes'not selected for Member 1");
		}

	}

	// Sub Question 3 for Member 2
	public static void setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber2(String SheetName, int Rownum,
			int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues3_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString()
				.trim();

		if (Questtion4_SybQues3_CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber2_Xpath));
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 2");

			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 2");
		} else {
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 2");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 2");
		}

	}

	// Sub Question 3 for Member 3
	public static void setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber3(String SheetName, int Rownum,
			int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues3_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString()
				.trim();

		if (Questtion4_SybQues3_CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber3_Xpath));
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 3");

			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 3");
		} else {
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 3");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 3");
		}

	}

	// Sub Question 3 for Member 4
	public static void setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber4(String SheetName, int Rownum,
			int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues3_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString()
				.trim();

		if (Questtion4_SybQues3_CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber4_Xpath));
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 4");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 4");

		} else {
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 4");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 4");
		}

	}

	// Sub Question 3 for Member 5
	public static void setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber5(String SheetName, int Rownum,
			int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues3_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString()
				.trim();

		if (Questtion4_SybQues3_CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber5_Xpath));
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 5");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 5");

		} else {
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 5");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 5");
		}

	}

	// Sub Question 3 for Member 6
	public static void setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber6(String SheetName, int Rownum,
			int subQuestionCheckBoxValueColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String Questtion4_SybQues3_CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString()
				.trim();

		if (Questtion4_SybQues3_CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber6_Xpath));
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' Selected for Member 6");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' selected for Member 6");

		} else {
			System.out.println(
					"'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 6");
			logger.log(LogStatus.PASS, "'Undergone any investigations(including basic radiological & blood test),other than normal health check-ups ,Insurance medicals or for visa purposes' not selected for Member 1");
		}

	}
	
	// Function for set question 4
	public static void setDataforQuestion4(String SheetName, int rowNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String question4 = TestCaseData[rowNum][78].toString().trim();
		
		String[][] TestCaseData2 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
		String noOfMembers = TestCaseData2[rowNum][5].toString().trim();   //Total Number of Count,  Members Selected from Quotation Page (First Page) based on Number we will enter Existing Disease Date

		if (question4.equalsIgnoreCase("Yes")) {
			waitForElements(By.xpath(superMediclaimCancer_Quest4_YesButton_Xpath));
			clickElement(By.xpath(superMediclaimCancer_Quest4_YesButton_Xpath));
			
			int subQues1_colNumforMemeber1=79;
			int subQues1_colNumforMemeber2=80;
			int subQues1_colNumforMemeber3=81;
			int subQues1_colNumforMemeber4=82;
			int subQues1_colNumforMemeber5=83;
			int subQues1_colNumforMemeber6=84;
			
			int subQues2_colNumforMemeber1=85;
			int subQues2_colNumforMemeber2=86;
			int subQues2_colNumforMemeber3=87;
			int subQues2_colNumforMemeber4=88;
			int subQues2_colNumforMemeber5=89;
			int subQues2_colNumforMemeber6=90;
			
			int subQues3_colNumforMemeber1=91;
			int subQues3_colNumforMemeber2=92;
			int subQues3_colNumforMemeber3=93;
			int subQues3_colNumforMemeber4=94;
			int subQues3_colNumforMemeber5=95;
			int subQues3_colNumforMemeber6=96;
			
			switch(noOfMembers){
			case "1":
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues1_colNumforMemeber1);
				
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues2_colNumforMemeber1);
				
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues3_colNumforMemeber1);
				break;
			case "2":
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues1_colNumforMemeber1);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues1_colNumforMemeber2);
				
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues2_colNumforMemeber1);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues2_colNumforMemeber2);
				
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues3_colNumforMemeber1);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues3_colNumforMemeber2);
				break;
				case "3":
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues1_colNumforMemeber1);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues1_colNumforMemeber2);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues1_colNumforMemeber3);
				
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues2_colNumforMemeber1);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues2_colNumforMemeber2);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues2_colNumforMemeber3);
				
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues3_colNumforMemeber1);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues3_colNumforMemeber2);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues3_colNumforMemeber3);
			    break; 
			case "4":
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues1_colNumforMemeber1);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues1_colNumforMemeber2);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues1_colNumforMemeber3);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues1_colNumforMemeber4);
				
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues2_colNumforMemeber1);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues2_colNumforMemeber2);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues2_colNumforMemeber3);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues2_colNumforMemeber4);
				
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues3_colNumforMemeber1);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues3_colNumforMemeber2);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues3_colNumforMemeber3);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues3_colNumforMemeber4);
				break;
			case "5":
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues1_colNumforMemeber1);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues1_colNumforMemeber2);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues1_colNumforMemeber3);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues1_colNumforMemeber4);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber5(SheetName, rowNum, subQues1_colNumforMemeber5);
				
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues2_colNumforMemeber1);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues2_colNumforMemeber2);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues2_colNumforMemeber3);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues2_colNumforMemeber4);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber5(SheetName, rowNum, subQues2_colNumforMemeber5);
				
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues3_colNumforMemeber1);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues3_colNumforMemeber2);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues3_colNumforMemeber3);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues3_colNumforMemeber4);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber5(SheetName, rowNum, subQues3_colNumforMemeber5);
	            break;
			case "6":
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues1_colNumforMemeber1);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues1_colNumforMemeber2);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues1_colNumforMemeber3);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues1_colNumforMemeber4);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber5(SheetName, rowNum, subQues1_colNumforMemeber5);
				setExistingSinceDate_SubQues1_UnderQuestion4_ForMemeber6(SheetName, rowNum, subQues1_colNumforMemeber6);
				
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues2_colNumforMemeber1);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues2_colNumforMemeber2);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues2_colNumforMemeber3);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues2_colNumforMemeber4);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber5(SheetName, rowNum, subQues2_colNumforMemeber5);
				setExistingSinceDate_SubQues2_UnderQuestion4_ForMemeber6(SheetName, rowNum, subQues2_colNumforMemeber6);
				
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber1(SheetName, rowNum, subQues3_colNumforMemeber1);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber2(SheetName, rowNum, subQues3_colNumforMemeber2);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber3(SheetName, rowNum, subQues3_colNumforMemeber3);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber4(SheetName, rowNum, subQues3_colNumforMemeber4);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber5(SheetName, rowNum, subQues3_colNumforMemeber5);
				setExistingSinceDate_SubQues3_UnderQuestion4_ForMemeber6(SheetName, rowNum, subQues3_colNumforMemeber6);
				break;
			
			}
			
		}
		else{
			waitForElements(By.xpath(superMediclaimCancer_Quest4_NoButton_Xpath));
			clickElement(By.xpath(superMediclaimCancer_Quest4_NoButton_Xpath));
			logger.log(LogStatus.PASS, "No Question Set 4 Type of disease is selected for any Member");
		}
    } 

	//*********************  End Functions for Question Set 4 ***********************************************************
	
	//************************  Start Functions for Question Set 5 ****************************** 
	
		// Sub Question 1 for Member 1
		public static void setData_SubQues1_UnderQuestion5_ForMemeber1(String SheetName, int Rownum,
				int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

			if (CheckBoxValue.equalsIgnoreCase("Yes")) {
				
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber1_Xpath));
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' selected for Member 1");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' selected for Member 1");
				
				enterText(By.xpath(textAra_forSubQuest1_UnderQues5_ForMemeber1_Xpath),String.valueOf(txtAraValue));
				System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
				logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

			} else {
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 1");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 1");
			}

		}
				
		// Sub Question 1 for Member 2
		public static void setData_SubQues1_UnderQuestion5_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum,int textAreaColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			
			String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

			if (CheckBoxValue.equalsIgnoreCase("Yes")) {
				
				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber2_Xpath));
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 2");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' selected for Member 2");
				
				enterText(By.xpath(textAra_forSubQuest1_UnderQues5_ForMemeber2_Xpath),String.valueOf(txtAraValue));
				System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
				logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

			} else {
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 2");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 2");
			}

		}

	// Sub Question 1 for Member 3
	public static void setData_SubQues1_UnderQuestion5_ForMemeber3(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

		if (CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber3_Xpath));
			System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 3");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' selected for Member 3");

			enterText(By.xpath(textAra_forSubQuest1_UnderQues5_ForMemeber3_Xpath), String.valueOf(txtAraValue));
			System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
			logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

		} else {
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 3");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 3");
		}

	}

	// Sub Question 1 for Member 4
		public static void setData_SubQues1_UnderQuestion5_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

			if (CheckBoxValue.equalsIgnoreCase("Yes")) {

				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber4_Xpath));
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 4");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' selected for Member 4");

				enterText(By.xpath(textAra_forSubQuest1_UnderQues5_ForMemeber4_Xpath), String.valueOf(txtAraValue));
				System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
				logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

			} else {
				System.out.println(
						"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 4");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 4");
			}

		}

		// Sub Question 1 for Member 5
		public static void setData_SubQues1_UnderQuestion5_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

			if (CheckBoxValue.equalsIgnoreCase("Yes")) {

				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber5_Xpath));
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 5");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' selected for Member 5");

				enterText(By.xpath(textAra_forSubQuest1_UnderQues5_ForMemeber5_Xpath), String.valueOf(txtAraValue));
				System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
				logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

			} else {
				System.out.println(
						"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 5");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 5");
			}

		}
	
		// Sub Question 1 for Member 4
		public static void setData_SubQues1_UnderQuestion5_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

			if (CheckBoxValue.equalsIgnoreCase("Yes")) {

				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber6_Xpath));
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 6");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' selected for Member 5");

				enterText(By.xpath(textAra_forSubQuest1_UnderQues5_ForMemeber6_Xpath), String.valueOf(txtAraValue));
				System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
				logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

			} else {
				System.out.println(
						"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 6");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 6");
			}

		}
				
	// *************************** Sub Question 2
	// Sub Question 2 for Member 1
		public static void setData_SubQues2_UnderQuestion5_ForMemeber1(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

			if (CheckBoxValue.equalsIgnoreCase("Yes")) {

				//clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber1_Xpath));
				WebElement e1=driver.findElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber1_Xpath));
				clickByJS(e1);
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 1");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 1");

				enterText(By.xpath(textAra_forSubQuest2_UnderQues5_ForMemeber1_Xpath), String.valueOf(txtAraValue));
				System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
				logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

			} else {
				System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 1");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not Selected for Member 1");
				
			}

		}
			
		// Sub Question 2 for Member 2
				public static void setData_SubQues2_UnderQuestion5_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

					if (CheckBoxValue.equalsIgnoreCase("Yes")) {

						clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber2_Xpath));
						System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 2");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 2");

						enterText(By.xpath(textAra_forSubQuest2_UnderQues5_ForMemeber2_Xpath), String.valueOf(txtAraValue));
						System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
						logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

					} else {
						System.out.println(
								"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 2");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not Selected for Member 2");
					}

				}

				// Sub Question 2 for Member 3
				public static void setData_SubQues2_UnderQuestion5_ForMemeber3(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

					if (CheckBoxValue.equalsIgnoreCase("Yes")) {

						clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber3_Xpath));
						System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 3");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 3");

						enterText(By.xpath(textAra_forSubQuest2_UnderQues5_ForMemeber3_Xpath), String.valueOf(txtAraValue));
						System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
						logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

					} else {
						System.out.println(
								"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 3");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not Selected for Member 3");
					}

				}
							
				// Sub Question 2 for Member 4
				public static void setData_SubQues2_UnderQuestion5_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

					if (CheckBoxValue.equalsIgnoreCase("Yes")) {

						clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber4_Xpath));
						System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 4");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 4");

						enterText(By.xpath(textAra_forSubQuest2_UnderQues5_ForMemeber4_Xpath), String.valueOf(txtAraValue));
						System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
						logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

					} else {
						System.out.println(
								"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 4");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not Selected for Member 4");
					}

				}
				// Sub Question 2 for Member 5
				public static void setData_SubQues2_UnderQuestion5_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

					if (CheckBoxValue.equalsIgnoreCase("Yes")) {

						clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber5_Xpath));
						System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 5");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 5");

						enterText(By.xpath(textAra_forSubQuest2_UnderQues5_ForMemeber5_Xpath), String.valueOf(txtAraValue));
						System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
						logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

					} else {
						System.out.println(
								"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 5");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not Selected for Member 5");
					}

				}
							
				// Sub Question 2 for Member 6
				public static void setData_SubQues2_UnderQuestion5_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

					if (CheckBoxValue.equalsIgnoreCase("Yes")) {

						clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber6_Xpath));
						System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 6");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 6");

						enterText(By.xpath(textAra_forSubQuest2_UnderQues5_ForMemeber6_Xpath), String.valueOf(txtAraValue));
						System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
						logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

					} else {
						System.out.println(
								"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 6");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not Selected for Member 6");
					}

				}
					
		// *************************** Sub Question 3
		// Sub Question 3 for Member 1
				// Sub Question 2 for Member 4
				public static void setData_SubQues3_UnderQuestion5_ForMemeber1(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);

					String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
					String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

					if (CheckBoxValue.equalsIgnoreCase("Yes")) {

						clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber1_Xpath));
						System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 1");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 1");

						enterText(By.xpath(textAra_forSubQuest3_UnderQues5_ForMemeber1_Xpath), String.valueOf(txtAraValue));
						System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
						logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

					} else {
						System.out.println("'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 1");
						logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 1");
					}

				}

	// Sub Question 3 for Member 2
	public static void setData_SubQues3_UnderQuestion5_ForMemeber2(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

		if (CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber2_Xpath));
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 2");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 2");

			enterText(By.xpath(textAra_forSubQuest3_UnderQues5_ForMemeber2_Xpath), String.valueOf(txtAraValue));
			System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
			logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

		} else {
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 2");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 2");
		}

	}

	// Sub Question 3 for Member 3
	public static void setData_SubQues3_UnderQuestion5_ForMemeber3(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

		if (CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber3_Xpath));
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 3");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 3");

			enterText(By.xpath(textAra_forSubQuest3_UnderQues5_ForMemeber3_Xpath), String.valueOf(txtAraValue));
			System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
			logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

		} else {
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 3");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 3");
		}

	}

	// Sub Question 3 for Member 4
	public static void setData_SubQues3_UnderQuestion5_ForMemeber4(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

		if (CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber4_Xpath));
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 4");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 4");

			enterText(By.xpath(textAra_forSubQuest3_UnderQues5_ForMemeber4_Xpath), String.valueOf(txtAraValue));
			System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
			logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

		} else {
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 4");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 4");
		}

	}

	// Sub Question 3 for Member 5
	public static void setData_SubQues3_UnderQuestion5_ForMemeber5(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);

		String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
		String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

		if (CheckBoxValue.equalsIgnoreCase("Yes")) {

			clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber5_Xpath));
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 5");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 5");

			enterText(By.xpath(textAra_forSubQuest3_UnderQues5_ForMemeber5_Xpath), String.valueOf(txtAraValue));
			System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
			logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

		} else {
			System.out.println(
					"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 5");
			logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 5");
		}

	}

	// Sub Question 3 for Member 6
		public static void setData_SubQues3_UnderQuestion5_ForMemeber6(String SheetName, int Rownum,int subQuestionCheckBoxValueColNum, int textAreaColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);

			String CheckBoxValue = TestCaseData[Rownum][subQuestionCheckBoxValueColNum].toString().trim();
			String txtAraValue = TestCaseData[Rownum][textAreaColNum].toString().trim();

			if (CheckBoxValue.equalsIgnoreCase("Yes")) {

				clickElement(By.xpath(sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber6_Xpath));
				System.out.println(
						"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 6");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' Selected for Member 6");

				enterText(By.xpath(textAra_forSubQuest3_UnderQues5_ForMemeber6_Xpath), String.valueOf(txtAraValue));
				System.out.println("Please specify quantity per day(in Grams).: " + txtAraValue);
				
				logger.log(LogStatus.PASS, "Please specify quantity per day(in Grams).: " + txtAraValue);

			} else {
				System.out.println(
						"'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 5");
				logger.log(LogStatus.PASS, "'Does the insured member(s) use gutka, tobacco, pan masala or any recreational drugs?' not selected for Member 5");
			}

		}
		
		// Function for set question 5
		public static void setDataforQuestion5(String SheetName, int rowNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String question4 = TestCaseData[rowNum][97].toString().trim();
			
			String[][] TestCaseData2 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
			String noOfMembers = TestCaseData2[rowNum][5].toString().trim();   //Total Number of Count,  Members Selected from Quotation Page (First Page) based on Number we will enter Existing Disease Date

			if (question4.equalsIgnoreCase("Yes")) {
				waitForElements(By.xpath(superMediclaimCancer_Quest5_YesButton_Xpath));
				clickElement(By.xpath(superMediclaimCancer_Quest5_YesButton_Xpath));
				//sub Ques 1
				int subQues1_CheckBox_colNumforMemeber1=98;
				int subQues1_TextAra_colNumforMemeber1=99;
				
				int subQues1_CheckBox_colNumforMemeber2=100;
				int subQues1_TextAra_colNumforMemeber2=101;
				
				int subQues1_CheckBox_colNumforMemeber3=102;
				int subQues1_TextAra_colNumforMemeber3=103;
				
				int subQues1_CheckBox_colNumforMemeber4=104;
				int subQues1_TextAra_colNumforMemeber4=105;
				
				int subQues1_CheckBox_colNumforMemeber5=106;
				int subQues1_TextAra_colNumforMemeber5=107;
				
				int subQues1_CheckBox_colNumforMemeber6=108;
				int subQues1_TextAra_colNumforMemeber6=109;
				
			
				//Sub ques 2
				int subQues2_CheckBox_colNumforMemeber1=110;
				int subQues2_TextAra_colNumforMemeber1=111;
				
				int subQues2_CheckBox_colNumforMemeber2=112;
				int subQues2_TextAra_colNumforMemeber2=113;
				
				int subQues2_CheckBox_colNumforMemeber3=114;
				int subQues2_TextAra_colNumforMemeber3=115;
				
				int subQues2_CheckBox_colNumforMemeber4=116;
				int subQues2_TextAra_colNumforMemeber4=117;
				
				int subQues2_CheckBox_colNumforMemeber5=118;
				int subQues2_TextAra_colNumforMemeber5=119;
				
				int subQues2_CheckBox_colNumforMemeber6=120;
				int subQues2_TextAra_colNumforMemeber6=121;
			
				//Sub Ques 3
				int subQues3_CheckBox_colNumforMemeber1=122;
				int subQues3_TextAra_colNumforMemeber1=123;
				
				int subQues3_CheckBox_colNumforMemeber2=124;
				int subQues3_TextAra_colNumforMemeber2=125;
				
				int subQues3_CheckBox_colNumforMemeber3=126;
				int subQues3_TextAra_colNumforMemeber3=127;
				
				int subQues3_CheckBox_colNumforMemeber4=128;
				int subQues3_TextAra_colNumforMemeber4=129;
				
				int subQues3_CheckBox_colNumforMemeber5=130;
				int subQues3_TextAra_colNumforMemeber5=131;
				
				int subQues3_CheckBox_colNumforMemeber6=132;
				int subQues3_TextAra_colNumforMemeber6=133;
				
				
				switch(noOfMembers){
				case "1":
					setData_SubQues1_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber1, subQues1_TextAra_colNumforMemeber1);
					
					setData_SubQues2_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber1, subQues2_TextAra_colNumforMemeber1);
					
					setData_SubQues3_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber1, subQues3_TextAra_colNumforMemeber1);
					break;
				case "2":
					setData_SubQues1_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber1, subQues1_TextAra_colNumforMemeber1);
					setData_SubQues1_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber2, subQues1_TextAra_colNumforMemeber2);
					
					setData_SubQues2_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber1, subQues2_TextAra_colNumforMemeber1);
					setData_SubQues2_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber2, subQues2_TextAra_colNumforMemeber2);
					
					setData_SubQues3_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber1, subQues3_TextAra_colNumforMemeber1);
					setData_SubQues3_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber2, subQues3_TextAra_colNumforMemeber2);
					break;
					case "3":
					setData_SubQues1_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber1, subQues1_TextAra_colNumforMemeber1);
					setData_SubQues1_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber2, subQues1_TextAra_colNumforMemeber2);
					setData_SubQues1_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber3, subQues1_TextAra_colNumforMemeber3);
					
					setData_SubQues2_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber1, subQues2_TextAra_colNumforMemeber1);
					setData_SubQues2_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber2, subQues2_TextAra_colNumforMemeber2);
					setData_SubQues2_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber3, subQues2_TextAra_colNumforMemeber3);
					
					setData_SubQues3_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber1, subQues3_TextAra_colNumforMemeber1);
					setData_SubQues3_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber2, subQues3_TextAra_colNumforMemeber2);
					setData_SubQues3_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber3, subQues3_TextAra_colNumforMemeber3);
				    break; 
				case "4":
					setData_SubQues1_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber1, subQues1_TextAra_colNumforMemeber1);
					setData_SubQues1_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber2, subQues1_TextAra_colNumforMemeber2);
					setData_SubQues1_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber3, subQues1_TextAra_colNumforMemeber3);
					setData_SubQues1_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber4, subQues1_TextAra_colNumforMemeber4);
					
					setData_SubQues2_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber1, subQues2_TextAra_colNumforMemeber1);
					setData_SubQues2_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber2, subQues2_TextAra_colNumforMemeber2);
					setData_SubQues2_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber3, subQues2_TextAra_colNumforMemeber3);
					setData_SubQues2_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber4, subQues2_TextAra_colNumforMemeber4);
					
					setData_SubQues3_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber1, subQues3_TextAra_colNumforMemeber1);
					setData_SubQues3_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber2, subQues3_TextAra_colNumforMemeber2);
					setData_SubQues3_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber3, subQues3_TextAra_colNumforMemeber3);
					setData_SubQues3_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber4, subQues3_TextAra_colNumforMemeber4);
					break;
				case "5":
					setData_SubQues1_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber1, subQues1_TextAra_colNumforMemeber1);
					setData_SubQues1_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber2, subQues1_TextAra_colNumforMemeber2);
					setData_SubQues1_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber3, subQues1_TextAra_colNumforMemeber3);
					setData_SubQues1_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber4, subQues1_TextAra_colNumforMemeber4);
					setData_SubQues1_UnderQuestion5_ForMemeber5(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber5, subQues1_TextAra_colNumforMemeber5);
					
					setData_SubQues2_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber1, subQues2_TextAra_colNumforMemeber1);
					setData_SubQues2_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber2, subQues2_TextAra_colNumforMemeber2);
					setData_SubQues2_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber3, subQues2_TextAra_colNumforMemeber3);
					setData_SubQues2_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber4, subQues2_TextAra_colNumforMemeber4);
					setData_SubQues2_UnderQuestion5_ForMemeber5(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber5, subQues2_TextAra_colNumforMemeber5);
					
					setData_SubQues3_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber1, subQues3_TextAra_colNumforMemeber1);
					setData_SubQues3_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber2, subQues3_TextAra_colNumforMemeber2);
					setData_SubQues3_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber3, subQues3_TextAra_colNumforMemeber3);
					setData_SubQues3_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber4, subQues3_TextAra_colNumforMemeber4);
					setData_SubQues3_UnderQuestion5_ForMemeber5(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber5, subQues3_TextAra_colNumforMemeber5);
		            break;
				case "6":
					setData_SubQues1_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber1, subQues1_TextAra_colNumforMemeber1);
					setData_SubQues1_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber2, subQues1_TextAra_colNumforMemeber2);
					setData_SubQues1_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber3, subQues1_TextAra_colNumforMemeber3);
					setData_SubQues1_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber4, subQues1_TextAra_colNumforMemeber4);
					setData_SubQues1_UnderQuestion5_ForMemeber5(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber5, subQues1_TextAra_colNumforMemeber5);
					setData_SubQues1_UnderQuestion5_ForMemeber6(SheetName, rowNum, subQues1_CheckBox_colNumforMemeber6, subQues1_TextAra_colNumforMemeber6);
					
					setData_SubQues2_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber1, subQues2_TextAra_colNumforMemeber1);
					setData_SubQues2_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber2, subQues2_TextAra_colNumforMemeber2);
					setData_SubQues2_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber3, subQues2_TextAra_colNumforMemeber3);
					setData_SubQues2_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber4, subQues2_TextAra_colNumforMemeber4);
					setData_SubQues2_UnderQuestion5_ForMemeber5(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber5, subQues2_TextAra_colNumforMemeber5);
					setData_SubQues2_UnderQuestion5_ForMemeber6(SheetName, rowNum, subQues2_CheckBox_colNumforMemeber6, subQues2_TextAra_colNumforMemeber6);
					
					setData_SubQues3_UnderQuestion5_ForMemeber1(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber1, subQues3_TextAra_colNumforMemeber1);
					setData_SubQues3_UnderQuestion5_ForMemeber2(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber2, subQues3_TextAra_colNumforMemeber2);
					setData_SubQues3_UnderQuestion5_ForMemeber3(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber3, subQues3_TextAra_colNumforMemeber3);
					setData_SubQues3_UnderQuestion5_ForMemeber4(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber4, subQues3_TextAra_colNumforMemeber4);
					setData_SubQues3_UnderQuestion5_ForMemeber5(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber5, subQues3_TextAra_colNumforMemeber5);
					setData_SubQues3_UnderQuestion5_ForMemeber6(SheetName, rowNum, subQues3_CheckBox_colNumforMemeber6, subQues3_TextAra_colNumforMemeber6);
					break;
				
				}
				
			}
			else{
				waitForElements(By.xpath(superMediclaimCancer_Quest5_NoButton_Xpath));
				clickElement(By.xpath(superMediclaimCancer_Quest5_NoButton_Xpath));
				logger.log(LogStatus.PASS, "No Question Set 5 Type of disease is selected for any Member");
			}
	    } 
	
	//*************** End Functions for Question Set 5 ***************************************************

		//*************** Start Functions for Question Set 6 ***************************************************
		public static String QuestionSet6Message="Are you or anyone of your family member (1st blood relationship) suffering from any of the following conditions:- Down's Syndrome / Turner's syndrome / Sickle Cell Anemia / Thalassemia Major / G6P Deficiency";
		
		//Member 1
		public static void setDetailsOfQuestionSet6_forMemeber1(String sheetName, int rowNum, int colNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();
			
			if(question2.equalsIgnoreCase("Yes")){
				
			WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues6_ForMemeber1_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber1_Xpath));
			System.out.println(QuestionSet6Message + "selected Question Set 2 Type of disease for Member 1");
			logger.log(LogStatus.PASS, QuestionSet6Message + "selected Question Set 2 Type of disease for Member 1");
			}
			else{
				 System.out.println("Not selected Question Set 6 Type of disease for Member 1");
				 logger.log(LogStatus.PASS, "Not selected Question Set 6 Type of disease for Member 1");
			}
		
		   
		}
		//Member 2
		public static void setDetailsOfQuestionSet6_forMemeber2(String sheetName, int rowNum, int colNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();
			
			if(question2.equalsIgnoreCase("Yes")){
				
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues6_ForMemeber2_Xpath));
				clickByJS(e1);
			//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber2_Xpath));
				System.out.println(QuestionSet6Message + "selected Question Set 2 Type of disease for Member 2");
				logger.log(LogStatus.PASS, QuestionSet6Message + "selected Question Set 2 Type of disease for Member 2");
			}
			else{
				 System.out.println("Not selected Question Set 6 Type of disease for Member 2");
				 logger.log(LogStatus.PASS, "Not selected Question Set 6 Type of disease for Member 2");
			}
		
		   
		}
		
		//Member 3
			public static void setDetailsOfQuestionSet6_forMemeber3(String sheetName, int rowNum, int colNum) throws Exception{
				String[][] TestCaseData = BaseClass.excel_Files(sheetName);
				String question2 = TestCaseData[rowNum][colNum].toString().trim();
				
				if(question2.equalsIgnoreCase("Yes")){
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues6_ForMemeber3_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber3_Xpath));
				System.out.println(QuestionSet6Message + "selected Question Set 2 Type of disease for Member 3");
				logger.log(LogStatus.PASS, QuestionSet6Message + "selected Question Set 2 Type of disease for Member 3");
				}
				else{
					 System.out.println("Not selected Question Set 6 Type of disease for Member 3");
					 logger.log(LogStatus.PASS, "Not selected Question Set 6 Type of disease for Member 3");
				}
			
			   
			}
			
		// Member 4
		public static void setDetailsOfQuestionSet6_forMemeber4(String sheetName, int rowNum, int colNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();

			if (question2.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues6_ForMemeber4_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber4_Xpath));
				System.out.println(QuestionSet6Message + "selected Question Set 2 Type of disease for Member 4");
				logger.log(LogStatus.PASS, QuestionSet6Message + "selected Question Set 2 Type of disease for Member 4");
			} else {
				System.out.println("Not selected Question Set 6 Type of disease for Member 4");
				logger.log(LogStatus.PASS, "Not selected Question Set 6 Type of disease for Member 4");
			}

		}
		
		// Member 5
			public static void setDetailsOfQuestionSet6_forMemeber5(String sheetName, int rowNum, int colNum) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(sheetName);
				String question2 = TestCaseData[rowNum][colNum].toString().trim();

				if (question2.equalsIgnoreCase("Yes")) {
					WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues6_ForMemeber5_Xpath));
					clickByJS(e1);
					//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber5_Xpath));
					System.out.println(QuestionSet6Message + "selected Question Set 2 Type of disease for Member 5");
					logger.log(LogStatus.PASS, QuestionSet6Message + "selected Question Set 2 Type of disease for Member 5");
				} else {
					System.out.println("Not selected Question Set 6 Type of disease for Member 5");
					logger.log(LogStatus.PASS, "Not selected Question Set 6 Type of disease for Member 5");
				}

			}
			
		// Member 6
		public static void setDetailsOfQuestionSet6_forMemeber6(String sheetName, int rowNum, int colNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(sheetName);
			String question2 = TestCaseData[rowNum][colNum].toString().trim();

			if (question2.equalsIgnoreCase("Yes")) {
				WebElement e1 = driver.findElement(By.xpath(CheckBox_UnderQues6_ForMemeber6_Xpath));
				clickByJS(e1);
				//clickElement(By.xpath(CheckBox_UnderQues2_ForMemeber6_Xpath));
				System.out.println(QuestionSet6Message + "selected Question Set 2 Type of disease for Member 6");
				logger.log(LogStatus.PASS, QuestionSet6Message + "selected Question Set 2 Type of disease for Member 6");
			} else {
				System.out.println("Not selected Question Set 6 Type of disease for Member 6");
				logger.log(LogStatus.PASS, "Not selected Question Set 6 Type of disease for Member 6");
			}

		}
		
		//********** Main Function for Set Data of Question 6 ***********************************
		public static void setDataforQuestion6(String SheetName, int rowNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String question2 = TestCaseData[rowNum][134].toString().trim();
			
			String[][] TestCaseData2 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
			String noOfMembers = TestCaseData2[rowNum][5].toString().trim();   //Total Number of Count,  Members Selected from Quotation Page (First Page) based on Number we will enter Existing Disease Date

			int colNumforMemeber1=135;
			int colNumforMemeber2=136;
			int colNumforMemeber3=137;
			int colNumforMemeber4=138;
			int colNumforMemeber5=139;
			int colNumforMemeber6=140;
			
			if (question2.equalsIgnoreCase("Yes")) {
				 waitForElements(By.xpath(superMediclaimCancer_Quest6_YesButton_Xpath));
				 clickElement(By.xpath(superMediclaimCancer_Quest6_YesButton_Xpath));
				 
				 switch(noOfMembers){
				 case "1":
					 setDetailsOfQuestionSet6_forMemeber1(SheetName ,rowNum, colNumforMemeber1);
					 System.out.println(QuestionSet6Message + "selected Question Set 6 Type of disease for Member 1");
					 break;
				 
				 case "2":
					 setDetailsOfQuestionSet6_forMemeber1(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber2(SheetName,rowNum, colNumforMemeber2);
					 System.out.println(QuestionSet6Message + "selected Question Set 6 Type of disease for Member 1 and 2");
					 break;
				 
				 case "3":
					 setDetailsOfQuestionSet6_forMemeber1(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber2(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber3(SheetName,rowNum, colNumforMemeber3);
					 System.out.println(QuestionSet6Message + "selected Question Set 6 Type of disease for Member 1, 2 and 3");
					 break;
				 
				 case "4":
					 setDetailsOfQuestionSet6_forMemeber1(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber2(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber3(SheetName,rowNum, colNumforMemeber3);
					 setDetailsOfQuestionSet6_forMemeber4(SheetName,rowNum, colNumforMemeber4);
					 System.out.println(QuestionSet6Message + "selected Question Set 6 Type of disease for Member 1, 2, 3 and 4");
				     break;
				 case"5":
					 setDetailsOfQuestionSet6_forMemeber1(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber2(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber3(SheetName,rowNum, colNumforMemeber3);
					 setDetailsOfQuestionSet6_forMemeber4(SheetName,rowNum, colNumforMemeber4);
					 setDetailsOfQuestionSet6_forMemeber5(SheetName,rowNum, colNumforMemeber5);
					 System.out.println(QuestionSet6Message + "selected Question Set 6 Type of disease for Member 1, 2, 3, 4 and 5");
					 break;
				 case "6":
					 setDetailsOfQuestionSet6_forMemeber1(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber2(SheetName,rowNum, colNumforMemeber2);
					 setDetailsOfQuestionSet6_forMemeber3(SheetName,rowNum, colNumforMemeber3);
					 setDetailsOfQuestionSet6_forMemeber4(SheetName,rowNum, colNumforMemeber4);
					 setDetailsOfQuestionSet6_forMemeber5(SheetName,rowNum, colNumforMemeber5);
					 setDetailsOfQuestionSet6_forMemeber6(SheetName,rowNum, colNumforMemeber6);
					 System.out.println(QuestionSet6Message + "selected Question Set 6 Type of disease for Member 1, 2, 3, 4 , 5 and 6");
					 break;	
				 
				 }

			} else {

				waitForElements(By.xpath(superMediclaimCancer_Quest6_NoButton_Xpath));
				clickElement(By.xpath(superMediclaimCancer_Quest6_NoButton_Xpath));
				System.out.println("No Question Set 6 Type of disease is selected for any Member");
				logger.log(LogStatus.PASS, "No Question Set 6 Type of disease is selected for any Member");
			}
		}

	//****************** End Functions for Question Set 6  ***************************************
    
		public static void setDetailsOfTermsAndConditions(){
			//Select Terms and Conditions check box
			clickElement(By.xpath(termsAndConditions1_Xpath));
			System.out.println("I hereby agree to the terms & conditions* of the purchase of this policy. *, Terms and Conditions Selected");
			logger.log(LogStatus.PASS, "I hereby agree to the terms & conditions* of the purchase of this policy. *, Terms and Conditions Selected");
			clickElement(By.xpath(termsAndConditions2_Xpath));
			System.out.println("Proposer is only authorized to pay / proposal deposit / premium *, Terms and Conditions Selected");
			logger.log(LogStatus.PASS, "Proposer is only authorized to pay / proposal deposit / premium *, Terms and Conditions Selected");
			clickElement(By.xpath(termsAndConditions3_Xpath));
			System.out.println("Receive Service SMS and E-mail Alerts, Terms and Conditions Selected");
			logger.log(LogStatus.PASS, "Receive Service SMS and E-mail Alerts, Terms and Conditions Selected");
			clickElement(By.xpath(termsAndConditions4_Xpath));
			System.out.println("I would also like to add Standing Instruction on my credit card for automatic future renewal premiums. Terms & conditions* , Terms and Conditions Selected");
			logger.log(LogStatus.PASS, "I would also like to add Standing Instruction on my credit card for automatic future renewal premiums. Terms & conditions* , Terms and Conditions Selected");
		}
		
		public static void setProceedToPaybutton(){
			waitForElements(By.xpath(proceed_to_pay_xpath)); 
			WebElement e1=driver.findElement(By.xpath(proceed_to_pay_xpath));
			pointToElement(e1);
			
			clickElement(By.xpath(proceed_to_pay_xpath));
			
			System.out.println("Clicked on Proceed to Pay Button");
			logger.log(LogStatus.PASS, "Clicked on Proceed to Pay Button");
			
		}

	public static void setSuperMediclaimCancerHealthQuestions(int rowNum) throws Exception{
		//Called the functions for Select Health Related Questions
		setDatafor_Question1("SuperMediClaimCancerQuestionSet",rowNum);
		setDataforQuestion2("SuperMediClaimCancerQuestionSet",rowNum);
		setDataforQuestion3("SuperMediClaimCancerQuestionSet",rowNum);
		setDataforQuestion4("SuperMediClaimCancerQuestionSet",rowNum);
		setDataforQuestion5("SuperMediClaimCancerQuestionSet",rowNum);
		setDataforQuestion6("SuperMediClaimCancerQuestionSet",rowNum);
		
		BaseClass.scrolldown();  //Scroll Down function
		setDetailsOfTermsAndConditions();
		setProceedToPaybutton();	
		
		Thread.sleep(30000);		
		try{
		
		WebElement e1=driver.findElement(By.xpath("//div[@class='col-md-12 text-center last-child']//h5[2]"));
		waitForElements(By.xpath("//div[@class='col-md-12 text-center last-child']//h5[2]"));
		String str=e1.getText();
		System.out.println(str);
		
		if(str.contains("PAN card is required")){
			waitForElements(By.xpath(closePanError_Xpath));
			clickElement(By.xpath(closePanError_Xpath)); //close the popup error Pan Card Deatils
			
			waitForElements(By.xpath(pancardTextField_Xpath)); 
			enterText(By.xpath(pancardTextField_Xpath),String.valueOf("BTAPK0588R"));//Enter Pan card Details
			logger.log(LogStatus.PASS, "Data Entered for Pan Card: " + "BTAPK0588R");
			
			clickElement(By.xpath(nextButton_QuotationPage_Xpath)); //Click on next button from Quotation page
			waitForElements(By.xpath(nextButtonSuperMediclaim_Xpath)); 
			clickElement(By.xpath(nextButtonSuperMediclaim_Xpath));//Click on next  button from Insured Details Page
			BaseClass.scrolldown();
			
			setProceedToPaybutton();	
			
		}
		
		}
		catch(Exception e){
			System.out.println("Moved to Payment Page");
		}
	} 
	
	
}
