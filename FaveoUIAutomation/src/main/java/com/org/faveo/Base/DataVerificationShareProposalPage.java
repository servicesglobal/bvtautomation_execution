package com.org.faveo.Base;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.InsuredDetails;
import com.relevantcodes.extentreports.LogStatus;

public class DataVerificationShareProposalPage extends BaseClass implements AccountnSettingsInterface {
	
	public static String expectedCreationDateValue;

	public static void clickOnShareProposal(){
		getCurrentDateValue();
		scrolldown();
		clickElement(By.xpath(shareProposal_Xpath));
		System.out.println("Clicked on Share Proposal Button");
		logger.log(LogStatus.PASS, "Clicked on Share Proposal Button");
	}
	
	//Get Current Date
		public static void getCurrentDateValue(){
			SimpleDateFormat f=new SimpleDateFormat("dd/MM/YYYY");
			Date d=new Date();
			 expectedCreationDateValue=f.format(d);
			System.out.println(expectedCreationDateValue);
		}
	
	//Set Email
		public static void setEmail(String SheetName, int rowNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteEmail = (TestCaseData[rowNum][3].toString().trim());
			clearTextfield(By.xpath(emailShareProposal_Xpath));
			enterText(By.xpath(emailShareProposal_Xpath), shareQuoteEmail);
			
			System.out.println("Data Entered for Quotation Share Email: " + shareQuoteEmail);
			logger.log(LogStatus.PASS, "Data Entered for Quotation Share Email: " + shareQuoteEmail);
			
		}
		
		//Set SMS
		public static void setSms(String SheetName, int rowNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteSms = (TestCaseData[rowNum][5].toString().trim());
			clearTextfield(By.xpath(phoneShareProposalXpath));
			enterText(By.xpath(phoneShareProposalXpath), shareQuoteSms);
			
			System.out.println("Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
			logger.log(LogStatus.PASS, "Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
		}
		
		//Click on Email Slider
		public static void SetSmsSlider(){
			clickElement(By.xpath(smsSliderShareProposalXpath));
		}
		
		//Click on Send Button
		public static void clickOnSendAndSaveButton() throws Exception{
			Thread.sleep(5000);
			clickElement(By.xpath(saveAndShareProposalButton_Xpath));
			System.out.println("Clicked on Save and Share Proposal Button");
			logger.log(LogStatus.PASS, "Clicked on Save and Share Proposal Button");
		}
		
	// Close Share Quotation Poup Box
	public static void closePopUp() throws Exception {
		Thread.sleep(2000);
		Fluentwait(By.xpath(closeShareProposalPopupBox_Xpath));
		clickElement(By.xpath(closeShareProposalPopupBox_Xpath));

		System.out.println("Closed Share Quoation Poup");
		logger.log(LogStatus.PASS, "Closed Share Quotation popup");

	}
	
	//Read Email
		public static void readText() throws Exception{
			Thread.sleep(2000);
			 String txt1=driver.findElement(By.xpath(txtvalue1_ShareProposal_Xpath)).getAttribute("innerHTML");
			 String txt2=driver.findElement(By.xpath(txtvalue2_ShareProposal_Xpath)).getAttribute("innerHTML");
			 String txt3=txt1.concat(" ").concat(txt2);
			System.out.println("Email Dispalyed in Shared Quotation Popup: " + txt3);
			logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + txt3);
		}

	//Function for Set Details of Share Quotation inside Popup in Quotation Page
		public static void setDetailsOfShareQuotation(String SheetName, int rowNum, int colNum1, int colNum2) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteOnlyByEmail = (TestCaseData[rowNum][colNum1].toString().trim());
			String shareQuoteOnlyBySms = (TestCaseData[rowNum][colNum2].toString().trim());
			
			//String shareQuoteOnlyByEmail="Yes";
			//String shareQuoteOnlyBySms="Yes";
			if(shareQuoteOnlyByEmail.equalsIgnoreCase("Yes") && shareQuoteOnlyBySms.equalsIgnoreCase("No")){
				setEmail(SheetName, rowNum);
				SetSmsSlider();
				clickOnSendAndSaveButton();
				readText();
				
			}
			else{
				setEmail(SheetName, rowNum);
				setSms(SheetName, rowNum);
				clickOnSendAndSaveButton();
				readText();
				
			}
			
			DataVerificationShareQuotationPage.getCurrentDateValue();
		}
		
		//Click on Quotation Tracker
		public static void clickOnDraftHistory() throws Exception{
			Thread.sleep(2000);
			Fluentwait(By.xpath(draftHistory_Xpath));
			clickElement(By.xpath(draftHistory_Xpath));
			System.out.println("Select Draft History");
			logger.log(LogStatus.PASS, "Select Draft History");
		}
		
		//Function for Verify the Data in Draft History
		public static void readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldBeAvailable(String shteetName1, int CustomerNameColNum, int rowNum, String ProductName) throws Exception{
			Thread.sleep(5000);
			
			List<WebElement> CreadtionDatelist=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[3]"));
			List<WebElement> productList=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[2]"));
			List<WebElement> CustomerNameList=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[1]"));
			
			int i=0;
			int j=0;
			
			String ExpectedValueOfdates=expectedCreationDateValue;
			//String ExpectedValueOfdates="18/12/2018";
			
			String[][] TestCaseData = BaseClass.excel_Files(shteetName1);
			String ExcpectedValueOfCustomerNames = TestCaseData[rowNum][CustomerNameColNum].toString().trim();
			
			//String ExpecetedValueOfproducts="Cancer Mediclaim";
			String ExpecetedValueOfproducts=ProductName;
			
			//String ExcpectedValueOfCustomerNames="Ashish Singh";
			
			System.out.println("Expected Data From Shared Proposal Page - Creation Date: "+ExpectedValueOfdates+","+"Product Names:"+","+ExpecetedValueOfproducts+"Customer Names:"+ExcpectedValueOfCustomerNames);
			logger.log(LogStatus.PASS, "Expected Data From Shared Proposal Page - Creation Date: "+ExpectedValueOfdates+","+"Product Names:"+","+ExpecetedValueOfproducts+",Customer Names:"+ExcpectedValueOfCustomerNames);
			
			
			for(WebElement dates:CreadtionDatelist){
				
				String ActualValueOfdates=dates.getText();
				String AcutalValueOfproducts=productList.get(i).getText();
				String AcutalValueOfCustomerNames=CustomerNameList.get(j).getText();
				
				System.out.println("Actual Data From Shared Proposal Page - Creation Date: "+ActualValueOfdates+","+"Product Names: "+AcutalValueOfproducts+",Customer Names: "+AcutalValueOfCustomerNames);
				logger.log(LogStatus.PASS, "Actual Data From Shared Proposal Page - Creation Date: "+ActualValueOfdates+","+"Product Names: "+AcutalValueOfproducts+",Customer Names: "+AcutalValueOfCustomerNames);
				
				if(ActualValueOfdates.equalsIgnoreCase(ExpectedValueOfdates) && AcutalValueOfproducts.equalsIgnoreCase(ExpecetedValueOfproducts) && AcutalValueOfCustomerNames.equalsIgnoreCase(ExcpectedValueOfCustomerNames)){
                 	System.out.println("Verified the Actual and Expected Shared Proposal Data Inside Draft History and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);
	                logger.log(LogStatus.PASS, "Verified the Actual and Expected Shared Proposal Data Inside Draft History and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);			
				break;
				}
				else{
					Assert.fail("Shared Proposal Data Inside Draft History not Verified and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);
				}
					
			}
			
		}
		
	// Function for Verify the Data in Draft History
	public static void readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldNotBeAvailable(String shteetName1,
			int CustomerNameColNum, int rowNum, String ProductName) throws Exception {
		Thread.sleep(5000);

		List<WebElement> CreadtionDatelist = driver.findElements(By.xpath(
				"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[3]"));
		List<WebElement> productList = driver.findElements(By.xpath(
				"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[2]"));
		List<WebElement> CustomerNameList = driver.findElements(By.xpath(
				"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[1]"));

		int i = 0;
		int j = 0;

		String ExpectedValueOfdates=expectedCreationDateValue;
		//String ExpectedValueOfdates = "18/12/2018";

		String[][] TestCaseData = BaseClass.excel_Files(shteetName1);
		String ExcpectedValueOfCustomerNames = TestCaseData[rowNum][CustomerNameColNum].toString().trim();

		// String ExpecetedValueOfproducts="Cancer Mediclaim";
		String ExpecetedValueOfproducts = ProductName;

		// String ExcpectedValueOfCustomerNames="Ashish Singh";

		System.out.println("Expected Data From Shared Proposal Page - Creation Date: " + ExpectedValueOfdates + ","
				+ "Product Names:" + "," + ExpecetedValueOfproducts + "Customer Names:"
				+ ExcpectedValueOfCustomerNames);
		logger.log(LogStatus.PASS,
				"Expected Data From Shared Proposal Page - Creation Date: " + ExpectedValueOfdates + ","
						+ "Product Names:" + "," + ExpecetedValueOfproducts + ",Customer Names:"
						+ ExcpectedValueOfCustomerNames);

		for (WebElement dates : CreadtionDatelist) {

			String ActualValueOfdates = dates.getText();
			String AcutalValueOfproducts = productList.get(i).getText();
			String AcutalValueOfCustomerNames = CustomerNameList.get(j).getText();

			System.out.println("Actual Data From Shared Proposal Page - Creation Date: " + ActualValueOfdates + ","
					+ "Product Names: " + AcutalValueOfproducts + ",Customer Names: " + AcutalValueOfCustomerNames);
			logger.log(LogStatus.PASS,
					"Actual Data From Shared Proposal Page - Creation Date: " + ActualValueOfdates + ","
							+ "Product Names: " + AcutalValueOfproducts + ",Customer Names: "
							+ AcutalValueOfCustomerNames);

			if((!ExpectedValueOfdates.equals(ActualValueOfdates)) 
					|| (!ExpecetedValueOfproducts.equals(AcutalValueOfproducts))
					|| (!ExcpectedValueOfCustomerNames.equals(AcutalValueOfCustomerNames)))
			{
				System.out.println("Verfied Share Porposal data is exit in Draft History");
				break;
			}
			else{
				System.out.println("Share Porposal data is still available in Draft History");
			}
            
		}

	}
		
		//Function For verify Data in Draft History
		public static void verifyDataInDraftHistory(String SheetName1,int CustomerColNum, int rowNum,String ProductName) throws Exception{
			DataVerificationShareQuotationPage.clickOnHaburgerMenu();
			clickOnDraftHistory();
			scrollup();
			readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldBeAvailable(SheetName1,CustomerColNum,rowNum,ProductName);
			
		}
		
	// Function For verify Data in Draft History
	public static void verifyDataShouldNotAvailableInDraftHistory(String SheetName1, int CustomerColNum, int rowNum, String ProductName)
			throws Exception {
		DataVerificationShareQuotationPage.clickOnHaburgerMenu();
		clickOnDraftHistory();
		scrollup();
		readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldNotBeAvailable(SheetName1, CustomerColNum, rowNum,ProductName);

	}
		
		//Function for click on Resume Policy from Draft History
		public static void clickOnResumePolicy(){
			clickElement(By.xpath(resumeProposalLink_Xpath));
			System.out.println("Clicked on Resume Proposal");
			logger.log(LogStatus.PASS, "Clicked on Resume Proposal");
		}
		
		public static void clickOnNextButtonFromProposarDetailsPage() throws Exception{
			Thread.sleep(5000);
			//waitForElement(By.id(Button_NextProposer_id));
			clickElement(By.id(Button_NextProposer_id));
			System.out.println("Clicked On Next Button in Proposer Detail Page and Moved to Next Page");
			logger.log(LogStatus.PASS, "Clicked On Next Button in Proposer Detail Page and Moved to Next Page");
		}
		
		public static void clickOnNextButtonFromInsuredDetailsPage(){
			waitForElement(By.xpath(nextButtonSuperMediclaim_Xpath));
			clickElement(By.xpath(nextButtonSuperMediclaim_Xpath));
			System.out.println("Clicked On Next Button in Insured Detail Page and Moved to Next Page");
			logger.log(LogStatus.PASS, "Clicked On Next Button in Insured Detail Page and Moved to Next Page");
		}
		
		public static void verifyPageTitle(){
			String expectedTitle="Religare Health Insurance";
			String acutalTitle=driver.getTitle();
			Assert.assertEquals(expectedTitle, acutalTitle);
			System.out.println("Application Title is verified and GUID is Reusable for multiple Times");
			logger.log(LogStatus.PASS, "Application Title is verified and GUID is Reusable for multiple Times");
		}
	
	//**********************************************************************************************	
		public static void setDetailsOfShareProposalPoupBox(int rowNum) throws Exception{
		clickOnShareProposal();
		setDetailsOfShareQuotation("ShareProposal", rowNum, 6, 7);
		closePopUp();
	}
	
	// ***************** SuperMediclaim Cancer ****************************
	// Verify Data for Share Quotation In Quotation Tracker Function for SuperMedclaim
	public static void verifyDataInOfShareProposalInDraftHistory_ForSuperMediclaimCancer(int rowNum) throws Exception {
		verifyDataInDraftHistory("SuperMediclaimCancer_TestData", 2, rowNum, "Cancer Mediclaim");

	}
	
	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCancer(int rowNum) throws Exception {
		verifyDataShouldNotAvailableInDraftHistory("SuperMediclaimCancer_TestData", 2, rowNum, "Cancer Mediclaim");

	}

	// Verify Data for Share Proposal In Draft History and Punch the Policy on Click Resume Policy
	public static void verifyDataInOfShareProposalInDrafthistoryAndPunchThePolicyOnClickResumePolicy_ForSuperMediclaimCancer(int rowNum)
			throws Exception {
		verifyDataInDraftHistory("SuperMediclaimCancer_TestData", 2, rowNum, "Cancer Mediclaim");
		clickOnResumePolicy();
		switchToNewTab();
		clickOnNextButtonFromProposarDetailsPage();
		clickOnNextButtonFromInsuredDetailsPage();

	}
	
	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForSuperMediclaimCancer(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal("SuperMediclaimCancer_TestData", rowNum);

	}
		
}
