package com.org.faveo.Base;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.relevantcodes.extentreports.LogStatus;

public class ReadDataFromEmail extends BaseClass implements AccountnSettingsInterface {

	// ********* Mail Verification Functions
	// *************************************************
	public static void openEmail() {
		Properties prop = readProperties();
		String url = prop.getProperty("MailUrl");
		// System.out.println("Property Value:"+ str);
		driver.get(url);
		System.out.println("Open Mail Id");
		logger.log(LogStatus.PASS, "Open Mail Id");

		String userName = prop.getProperty("mailUserName");
		waitForElement(By.xpath(userName_Xpath));
		enterText(By.xpath(userName_Xpath), userName);
		System.out.println("Data Entered for Mail User Name:  " + userName);
		logger.log(LogStatus.PASS, "Data Entered for Mail User Name:  " + userName);

		String Password = prop.getProperty("mailPassword");
		waitForElement(By.xpath(password_Xpath));
		enterText(By.xpath(password_Xpath), Password);
		System.out.println("Data Entered for Mail Password:  " + Password);
		logger.log(LogStatus.PASS, "Data Entered for Mail Password:  " + Password);

		waitForElement(By.xpath(signIN_Xpath));
		clickElement(By.xpath(signIN_Xpath));
		System.out.println("Clicked on Sign-In button");
		logger.log(LogStatus.PASS, "Clicked on Sign-In button");

		waitForElement(By.xpath(shareQuotationFolder_Xpath));
		clickElement(By.xpath(shareQuotationFolder_Xpath));

		System.out.println("Moved to Shared Proposal Email Folder");
		logger.log(LogStatus.PASS, "Moved to Shared Proposal Email Folder");

	}

	// Delete Button in Email
	public static void clickOndeleteButtonInEmail() {
		waitForElement(By.xpath(deleteButton_In_Mail_Xpath));
		clickElement(By.xpath(deleteButton_In_Mail_Xpath));
		System.out.println("Clicked on Delete Button");
		logger.log(LogStatus.PASS, "Clicked on Delete Button");

	}

	public static void openAndDeleteOldEmail() {
		openEmail();
		clickOndeleteButtonInEmail();

	}
		
	// Proposer Name
	public static void readAndVerifyPropserName(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Name = TestCaseData[Rownum][2].toString().trim();
		WebElement e1 = driver.findElement(By.xpath(popserName_In_MailBody_Xpath));
		String propserName = e1.getText();
		if (propserName.contains(Name)) {
			System.out.println("Propser Name Verified: " + Name);
		} else {
			Assert.fail("Propser Name is not available inside Email Body: " + propserName);
		}
	}
			
	// Proposer Name
	public static void readAndVerifyPropserName_ForSharePorposal(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Name = TestCaseData[Rownum][2].toString().trim();
		String[] Name1=Name.split(" ");
		System.out.println(Name1[0]+ Name1[1]);
		WebElement e1 = driver.findElement(By.xpath(popserName_In_MailBody_Xpath));
		String propserName = e1.getText();
		
		for(int i=0; i<=Name1.length; i++){
			if (propserName.contains(Name1[i])) {
				break;
			} else {
				Assert.fail("Propser Name is not available inside Email Body: " + propserName);
			}
		}
		System.out.println("Propser Name Verified: " + Name);
	}

	// Total Number of Members
	public static void readAndVerifyTotalNumberOfMemebers(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TotalMember = (TestCaseData[Rownum][Colnum].toString().trim());
		Thread.sleep(2000);
		WebElement e1 = driver.findElement(By.xpath(totalNumberOfMembers_In_MailBody_Xpath));
		String numOfMember = e1.getText();
		if (numOfMember.contains(TotalMember)) {
			System.out.println("Total Number of Members are Verified: " + TotalMember);
		} else {
			Assert.fail("Total Number of Members are not Verified in Email Body: " + numOfMember);
		}
	}
			
	// Sum Insured
	public static void readAndVerifySumInsured(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String sumInsuredValue = (TestCaseData[Rownum][Colnum].toString().trim());

		WebElement e1 = driver.findElement(By.xpath(sumInsured_In_MailBody_Xpath));
		String sumInsured = e1.getText();
		if (sumInsured.contains(sumInsuredValue)) {
			System.out.println("Sum Insured is Verified in Email Body: " + sumInsuredValue);
		} else {
			Assert.fail("Sum Insured is not Verified in Email Body: " + sumInsured);
		}
	}
			
	// Premium
	public static void readAndVerifyPremium() throws Exception {
		// String premiumValue=AddonsforProducts.ExpectedpremiumAmount;
		// String premiumValue="831";
		String premiumValue = AddonsforProducts.ExpectedpremiumAmount;
		WebElement e1 = driver.findElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		pointToElement(e1);
		String sumInsured1 = e1.getText();
		String sumInsured = sumInsured1.replaceAll("\\W", "");

		String sumInsured2 = sumInsured.substring(0, sumInsured.indexOf('f'));
		// System.out.println(str);

		if (premiumValue.contains(sumInsured2)) {
			System.out.println("Total Premium is Verified: " + sumInsured2);
		} else {
			Assert.fail("Total Premium is not Verified: " + premiumValue);
		}
	}
			
	// Tenure
	public static void readAndVerifyTenure(String SheetName, int Rownum, int ColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TenureValue = (TestCaseData[Rownum][ColNum].toString().trim());

		WebElement e1 = driver.findElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		pointToElement(e1);
		String sumInsured = e1.getText();

		String[] str = sumInsured.split(" ");
		String Num = str[2];
		String year = str[3];
		String tenure = Num.concat(" ");
		String tenure1 = tenure.concat(year);
		System.out.println(tenure1);

		if (TenureValue.equalsIgnoreCase(tenure1)) {
			System.out.println("Tenure is Verified: " + TenureValue);
		} else {
			Assert.fail("Tenure is not Verified: " + tenure1);
		}
	}

	// Function for Click on Refresh Button Until New Email Not Displayed
	public static void clickOnRefreshButtonUntilNewemailNotDisplayed() throws Exception {
		boolean flag = true;

		while (flag) {
			try {
				if (driver
						.findElement(By
								.xpath("//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]//div[@id='divVw']//div[@id='divLV']//div[@id='divSubject']"))
						.isDisplayed()) {
					System.out.println("Pass");
					break;
				}
			} catch (Exception e) {
				Thread.sleep(10000);
				driver.navigate().refresh();
				waitForElement(By.xpath(shareQuotationFolder_Xpath));
				clickElement(By.xpath(shareQuotationFolder_Xpath));
			}
		}
	}

	public static void readAndVerifyAgeGroupMembers(String SheetName, int RowNum, int ColNumOfNumOfMembers,
			int ColNumOfAgeGroup1, int ColNumOfAgeGroup2, int ColNumOfAgeGroup3, int ColNumOfAgeGroup4,
			int ColNumOfAgeGroup5, int ColNumOfAgeGroup6) throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String NoOfMembers = (TestCaseData[RowNum][ColNumOfNumOfMembers].toString().trim());
		String AgeGroupOfMemeber1 = (TestCaseData[RowNum][ColNumOfAgeGroup1].toString().trim());
		String AgeGroupOfMemeber2 = (TestCaseData[RowNum][ColNumOfAgeGroup2].toString().trim());
		String AgeGroupOfMemeber3 = (TestCaseData[RowNum][ColNumOfAgeGroup3].toString().trim());
		String AgeGroupOfMemeber4 = (TestCaseData[RowNum][ColNumOfAgeGroup4].toString().trim());
		String AgeGroupOfMemeber5 = (TestCaseData[RowNum][ColNumOfAgeGroup5].toString().trim());
		String AgeGroupOfMemeber6 = (TestCaseData[RowNum][ColNumOfAgeGroup6].toString().trim());

		String str = driver.findElement(By.xpath(groupMembers_In_MailBody_Xpath)).getText();
		String[] agegroup = str.split(",");

		l1: for (String str1 : agegroup) {
			System.out.println(str1);

			/*
			 * String AgeGroupOfMemeber1_Mail=agegroup[0]; String
			 * AgeGroupOfMemeber2_Mail=agegroup[1]; String
			 * AgeGroupOfMemeber3_Mail=agegroup[2]; String
			 * AgeGroupOfMemeber4_Mail=agegroup[3]; String
			 * AgeGroupOfMemeber5_Mail=agegroup[4]; String
			 * AgeGroupOfMemeber6_Mail=agegroup[5];
			 */

			String AgeGroupOfMemeber1_Mail = str1;
			String AgeGroupOfMemeber2_Mail = str1;
			String AgeGroupOfMemeber3_Mail = str1;
			String AgeGroupOfMemeber4_Mail = str1;
			String AgeGroupOfMemeber5_Mail = str1;
			String AgeGroupOfMemeber6_Mail = str1;

			switch (NoOfMembers) {
			case "1":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)) {
					System.out.println("AgeGroup is verified for Member 1: " + AgeGroupOfMemeber1_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "2":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)) {
					System.out.println("AgeGroup is verified for Member 1 and 2: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "3":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2 and 3: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "4":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3 and 4: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " " + AgeGroupOfMemeber4_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not verified");
				}
				break;
			case "5":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
						&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3, 4 and 5: " + AgeGroupOfMemeber1_Mail
							+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
							+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "6":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
						&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)
						&& AgeGroupOfMemeber6.equalsIgnoreCase(AgeGroupOfMemeber6_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3, 4, 5 and 6: " + AgeGroupOfMemeber1_Mail
							+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
							+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail + " " + AgeGroupOfMemeber6_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;

			}
		}

	}

	// Clicked on Buy Now Button from Email Link
	public static void clickOnBuyNowEmailLink() {
		WebElement e1 = driver.findElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
		pointToElement(e1);
		waitForElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
		clickElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
		System.out.println("Clicked on Buy Now Link Button from Email");
		logger.log(LogStatus.PASS, "Clicked on Buy Now Link Button from Email");

	}
	
	//Read and Verify Data in Email Body
		public static void readAndVerifyDataInEmailBody(String SheetName, int Rownum) throws Exception{
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserName(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebers(SheetName, Rownum, 5);
			readAndVerifyAgeGroupMembers(SheetName,Rownum,5,6,7,8,9,10,11);
			readAndVerifySumInsured(SheetName, Rownum, 13);
			readAndVerifyPremium();
			readAndVerifyTenure(SheetName, Rownum, 14);
		}
		
	// Read and Verify Data in Email Body
	public static void readAndVerifyDataInEmailBody_ShareProposal(String SheetName, int Rownum) throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserName_ForSharePorposal(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebers(SheetName, Rownum, 5);
		readAndVerifyAgeGroupMembers(SheetName, Rownum, 5, 6, 7, 8, 9, 10, 11);
		readAndVerifySumInsured(SheetName, Rownum, 13);
		readAndVerifyPremium();
		readAndVerifyTenure(SheetName, Rownum, 14);
	}
	
	// Verify Proposal Generated GUID Should not be Reusable for Super Mediclaim Cancer
	public static void OpenEmailAndClickOnBuyNowButton(int rowNum) throws Exception {
		openEmail();
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		clickOnBuyNowEmailLink();
		switchToNewTab();
		Thread.sleep(2000);

	}
	
}
