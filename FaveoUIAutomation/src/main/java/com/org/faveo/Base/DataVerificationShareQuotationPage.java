package com.org.faveo.Base;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.QuotationPage;
import com.relevantcodes.extentreports.LogStatus;

public class DataVerificationShareQuotationPage extends BaseClass implements AccountnSettingsInterface {
	
	public static String email;
	public static String mobile;
	static String expectedCreationDateValue;

	//Click on Share Quotation Button
	public static void clickOnShareQuotationButton(){
		clickElement(By.xpath(shareQuotation_Xpath));
		System.out.println("Clicked on Share Quotation Button");
		logger.log(LogStatus.PASS, "Clicked on Share Quotation Button");
	}
	
	//Set Email
	public static void setEmail(String SheetName, int rowNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String shareQuoteEmail = (TestCaseData[rowNum][3].toString().trim());
		clearTextfield(By.xpath(emailShareQuotation_Xpath));
		enterText(By.xpath(emailShareQuotation_Xpath), shareQuoteEmail);
		
		System.out.println("Data Entered for Quotation Share Email: " + shareQuoteEmail);
		logger.log(LogStatus.PASS, "Data Entered for Quotation Share Email: " + shareQuoteEmail);
		
	}
	
	//Set SMS
	public static void setSms(String SheetName, int rowNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String shareQuoteSms = (TestCaseData[rowNum][5].toString().trim());
		clearTextfield(By.xpath(SmsQuotation_Xpath));
		enterText(By.xpath(SmsQuotation_Xpath), shareQuoteSms);
		
		System.out.println("Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
		logger.log(LogStatus.PASS, "Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
	}
	
	//Click on Email Slider
	public static void SetEmailSlider(){
		clickElement(By.xpath(EmailSliderShareQuotation_Xpath));
	}
	
	//Click on Sms Slider
	public static void SetSmsSlider(){
		clickElement(By.xpath(SmsSliderShareQuotation_Xpath));
	}
	
	//Click on Send Button
	public static void clickOnSendButton() throws Exception{
		Thread.sleep(5000);
		clickElement(By.xpath("//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']//button[text()='Send']"));
		System.out.println("Clicked on Send Button");
		logger.log(LogStatus.PASS, "Clicked on Send Button");
	}
	
	//Close Share Quotation Poup Box
	public static void closePopUp() throws Exception{
		Thread.sleep(2000);
		Fluentwait(By.xpath(closeShareQuotationPoupBox));
		clickElement(By.xpath(closeShareQuotationPoupBox));
		
		System.out.println("Closed Share Quoation Poup");
		logger.log(LogStatus.PASS, "Closed Share Quotation popup");
		
	}
	
	//Read Email
	public static void readEmail() throws Exception{
		Thread.sleep(2000);
		 email=driver.findElement(By.xpath(emailTextValueInPopupBox)).getAttribute("innerHTML");
		System.out.println("Email Dispalyed in Shared Quotation Popup: " + email);
		logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + email);
	}
	
	//Read Mobile
	public static void readSms() throws Exception{
		Thread.sleep(2000);
		 mobile=driver.findElement(By.xpath(SmsTextValueInPopupBox)).getAttribute("innerHTML");
		System.out.println("Mobile Number Dispalyed in Shared Quotation Popup: " + mobile);
		logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + mobile);
	}
	
	//Get Current Date
	public static void getCurrentDateValue(){
		SimpleDateFormat f=new SimpleDateFormat("dd/MM/YYYY");
		Date d=new Date();
		 expectedCreationDateValue=f.format(d);
		System.out.println(expectedCreationDateValue);
	}
	
	//Function for Set Details of Share Quotation inside Popup in Quotation Page
	public static void setDetailsOfShareQuotation(String SheetName, int rowNum, int colNum1, int colNum2) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String shareQuoteOnlyByEmail = (TestCaseData[rowNum][colNum1].toString().trim());
		String shareQuoteOnlyBySms = (TestCaseData[rowNum][colNum2].toString().trim());
		
		//String shareQuoteOnlyByEmail="Yes";
		//String shareQuoteOnlyBySms="Yes";
		if(shareQuoteOnlyByEmail.equalsIgnoreCase("Yes") && shareQuoteOnlyBySms.equalsIgnoreCase("No")){
			setEmail(SheetName, rowNum);
			SetSmsSlider();
			clickOnSendButton();
			readEmail();
			
		}
		else if(shareQuoteOnlyByEmail.equalsIgnoreCase("No") && shareQuoteOnlyBySms.equalsIgnoreCase("Yes")){
			SetEmailSlider();
			setSms(SheetName, rowNum);
			clickOnSendButton();
			readSms();
		}
		else{
			setEmail(SheetName, rowNum);
			setSms(SheetName, rowNum);
			clickOnSendButton();
			readEmail();
			readSms();
			
		}
		
		getCurrentDateValue();
	}
	
	//Function for Click on Dashboard
	public static void clickOnDashboard(){
		Fluentwait(By.xpath(Dashboard_logoImage_Xpath));
		clickElement(By.xpath(Dashboard_logoImage_Xpath));
		System.out.println("Moved to Dashboard Page");
		logger.log(LogStatus.PASS, "Moved to Dashboard Page");
	}
	
	//Click on Hamburger Menu
	public static void clickOnHaburgerMenu() throws Exception{
		Thread.sleep(5000);
		Fluentwait(By.xpath(hamburgerMenu_Xpath));
		clickElement(By.xpath(hamburgerMenu_Xpath));
		System.out.println("Clicked on Hamburger Menu");
		logger.log(LogStatus.PASS, "Clicked on Hamburger Menu");
	}
	
	//Click on Quotation Tracker
	public static void clickOnQuotationTracker() throws Exception{
		Thread.sleep(2000);
		Fluentwait(By.xpath(quotationTracker_Xpath));
		clickElement(By.xpath(quotationTracker_Xpath));
		System.out.println("Select Quotation Tracker");
		logger.log(LogStatus.PASS, "Select Quotation Tracker");
	}
	
	//Select Filters
	public static void setFilterByDays(String SheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Days = (TestCaseData[rowNum][colNum].toString().trim());
		
		Thread.sleep(2000);
		scrollup();
		List<WebElement> list=driver.findElements(By.xpath("//div[contains(@class,'Check_buttons')]//button"));
	
		for(WebElement options:list){
			if(options.getText().equalsIgnoreCase(Days)){
				options.click();
			}
			else if(options.getText().equalsIgnoreCase(Days)){
				options.click();
			}
			else if(options.getText().equalsIgnoreCase(Days)){
				options.click();
			}
		}
		
		
		System.out.println("Days Selected: "+ Days);
		logger.log(LogStatus.PASS, "Days Selected: "+ Days);
	}
	
	//Select Filter by Search Text Box
	public static void setFilterBySearchTextBoxParameters(){
		clickElement(By.xpath("//div[contains(@class,'padding0')]//ul[contains(@class,'month_year_by')]//preceding-sibling::a"));
		List<WebElement> list=driver.findElements(By.xpath("//div[contains(@class,'padding0')]//ul[contains(@class,'month_year_by')]//preceding-sibling::a//following-sibling::ul//li//a"));
		Iterator<WebElement> itr=list.iterator();
		while(itr.hasNext()){
			WebElement option=itr.next();
			String str=option.getText();
			
			if(str.equalsIgnoreCase("By Email")){
				option.click();
				System.out.println("Filter By Email Selected");
				break;
			}
		}
		enterText(By.xpath(searchByEmailTextBox_Xpath), "rhiclqctech@religare.com");
		System.out.println("Enter Email Id in Search Text box: " + "rhiclqctech@religare.com");
		logger.log(LogStatus.PASS, "Enter Email Id in Search Text box: " + "rhiclqctech@religare.com");
		
		clickElement(By.xpath(Quotation_searchButton_Xpath));
		System.out.println("Clicked on Search Button");
		logger.log(LogStatus.PASS, "Clicked on Search Button");
	}
	
	//Set and Read Details in Quotation Tracker Table
	public static void VerifyDetailsShouldBeAvailableInQuotationTable() throws Exception{
		Thread.sleep(3000);
		List<WebElement> emailList=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[5]"));
		List<WebElement> premiumAmount=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[2]"));
		List<WebElement> status=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[8]"));
		List<WebElement> CreationDate=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[3]"));
	   
		String expectedEmail=email;
		String expectedPremium= AddonsforProducts.ExpectedpremiumAmount;
		String expectedCreationDateValue1=expectedCreationDateValue;
		
		System.out.println("Expected Data in Quotation Details Page - Email: "+expectedEmail+","+"Premium:"+","+expectedPremium+",Creation Date:"+expectedCreationDateValue1);
		logger.log(LogStatus.PASS, "Expected Data in Quotation Details Page - Email: "+expectedEmail+","+"Premium:"+","+expectedPremium+",Creation Date:"+expectedCreationDateValue1);
		int j=0;
		int k=0;
		int l=0;
			
		for(WebElement emailOptions: emailList){
		String email=emailOptions.getText();
		
		String premiumAmountVal=premiumAmount.get(j).getText();
		String premiumAmountVal1=premiumAmountVal.replaceAll("\\W", "");
		
		String StatusValue=status.get(k).getText();
		String creationDateValue=CreationDate.get(l).getText();
		
		System.out.println("Acutal Data in quotation Tracker - Email : "+email+","+"Premium:"+","+premiumAmountVal1+",Creation Date:"+creationDateValue);
		logger.log(LogStatus.PASS, "Acutal Data in quotation Tracker - Email: "+email+","+"Premium:"+","+premiumAmountVal1+",Creation Date:"+creationDateValue);
		
		if(email.equalsIgnoreCase(expectedEmail) && premiumAmountVal1.equalsIgnoreCase(expectedPremium) && creationDateValue.equalsIgnoreCase(expectedCreationDateValue1) && StatusValue.equalsIgnoreCase("Shared")){
		System.out.println("Verified the Share Quotation Data Inside Quotation Tracker Value of email:" +expectedEmail +"," +" Premium Amount: "+ expectedPremium+ ","+ "Creation Date: "+expectedCreationDateValue1 + "," + "Status Value: "+StatusValue);
		break;
			}
			else{
				Assert.fail("Shared Quotation Values Email:" + email +","+ "Premium Amount: "+premiumAmountVal1+","+"Creation Date Value: "+ creationDateValue + " "+"are not available inside Quotation Tracker");
			}
		}
		
	}
	
	
	// Set and Read Details in Quotation Tracker Table
	public static void VerifyDetailsShouldBeNotAvailableInQuotationTable(String ProductName) throws Exception {
		Thread.sleep(3000);
		List<WebElement> emailList = driver
				.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[5]"));
		List<WebElement> premiumAmount = driver
				.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[2]"));
		List<WebElement> productNameList = driver
				.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[1]"));
		List<WebElement> CreationDate = driver
				.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[3]"));

		
		 String expectedEmail=email; 
		 String expectedPremium=AddonsforProducts.ExpectedpremiumAmount;
		 String expectedCreationDateValue1=expectedCreationDateValue;
		 

		//String expectedEmail = "rhiclqctech@religre.com";
		//String expectedPremium = "87";
	    //String expectedCreationDateValue1 = "13/12/2019";
		String expctedProductName = ProductName;

		int j = 0;
		int k = 0;
		int l = 0;

		for (WebElement emailOptions : emailList) {
			String Acutalemail = emailOptions.getText();

			String premiumAmountVal = premiumAmount.get(j).getText();
			String ActualpremiumAmountVal1 = premiumAmountVal.replaceAll("\\W", "");

			String ActualStatusValue = productNameList.get(k).getText();
			String ActualcreationDateValue = CreationDate.get(l).getText();

			if ((!expectedEmail.equals(Acutalemail)) || (!expectedPremium.equals(ActualpremiumAmountVal1))
					|| (!expectedCreationDateValue1.equals(ActualcreationDateValue))
					|| (!expctedProductName.equals(ActualStatusValue))) {
				System.out.println("Verified the Share Quotation Data is not avilable in Quotation Tracker");
				break;
			} else {
				System.out.println("Share Quotation Data is still avilable in Quotation Tracker");
				break;
			}
		}

	}
	
	
	public static void verifyPageTitle(){
		String expectedTitle="Religare Health Insurance";
		String acutalTitle=driver.getTitle();
		Assert.assertEquals(expectedTitle, acutalTitle);
		System.out.println("Application Title is verified and GUID is Reusable for multiple Times");
		logger.log(LogStatus.PASS, "Application Title is verified and GUID is Reusable for multiple Times");
	}
	
	// Function for VerifyData in Quotation Tracker
	public static void verifyDataInQuotationTracker(String SheetName, int rowNum, int colNum) throws Exception {
		clickOnHaburgerMenu();
		clickOnQuotationTracker();
		setFilterByDays(SheetName, rowNum, colNum);
		setFilterBySearchTextBoxParameters();
		VerifyDetailsShouldBeAvailableInQuotationTable();

	}
	
	// Function for VerifyData in Quotation Tracker
	public static void verifyDataShouldBeNotAvailableInQuotationTracker(String SheetName, int rowNum, int colNum,
			String ProductName) throws Exception {
		Thread.sleep(5000);
		clickOnHaburgerMenu();
		clickOnQuotationTracker();
		setFilterByDays(SheetName, rowNum, colNum);
		setFilterBySearchTextBoxParameters();
		VerifyDetailsShouldBeNotAvailableInQuotationTable(ProductName);

	}
	
	public static void setDataOfSharequotationPopupBox(int rowNum) throws Exception{
		setDetailsOfShareQuotation("ShareQuotation", rowNum, 6, 7);
		closePopUp();
	}
	
	//***************************************************************************************
	
	//Enter Quotation Data for Super Mediclaim Cancer
	public static void QuotationPageJourney(int Rownum) throws Exception
	{
		// Reading Proposer Name from Excel
		QuotationPage.ProposerName("SuperMediclaimCancer_TestData", Rownum, 2);
					
		// Reading Emailfrom Excel Sheet
		QuotationPage.ProposerEmail("SuperMediclaimCancer_TestData", Rownum, 3);

		// Reading Mobile Number from Excel
		QuotationPage.ProposerMobile("SuperMediclaimCancer_TestData", Rownum, 4);
		
		// Enter The Value of Total members present in policy
		QuotationPage.SelectNoOfMembersInSuperMediClaim("SuperMediclaimCancer_TestData", Rownum, 5);
		
		// Select Age of Member 1 from Excel
		QuotationPage.setAgeDeatilsforMemebrsInSuperMediclaim("SuperMediclaimCancer_TestData", Rownum,6,7,8,9,10,11);
		
		// Read the Value of Suminsured
		QuotationPage.SumInsured("SuperMediclaimCancer_TestData", Rownum, 13);
		
		// Reading the value of Tenure from Excel
		QuotationPage.setTenureForSuperMedicliamCancer("SuperMediclaimCancer_TestData", Rownum, 14);
		
		// Scroll Window Up
		BaseClass.scrollup();

		// NCB Super Addon Selection
		AddonsforProducts.setPaymentFrequency("SuperMediclaimCancer_TestData", Rownum, 15);
		
		AddonsforProducts.readPremiumFromFirstPage();
		
		clickOnShareQuotationButton();
			
	}
	
	//Verify Data for Share Quotation In Quotation Tracker Function for SuperMedclaim
	public static void verifyDataInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(int rowNum) throws Exception{
		/*setDetailsOfShareQuotation("ShareQuotation", rowNum, 6, 7);
		closePopUp();*/
		//setDataOfSharequotationPopupBox(rowNum);
		verifyDataInQuotationTracker("ShareQuotation", rowNum, 8);
	}
	
	//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForSuperMediclaimCancer(int rowNum) throws Exception{
		/*setDetailsOfShareQuotation("ShareQuotation", rowNum, 6, 7);
		closePopUp();*/
		//setDataOfSharequotationPopupBox(rowNum);
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody("SuperMediclaimCancer_TestData", rowNum);
	}
	
	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(int rowNum) throws Exception{
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"Cancer Mediclaim");
	}
	
	
}
