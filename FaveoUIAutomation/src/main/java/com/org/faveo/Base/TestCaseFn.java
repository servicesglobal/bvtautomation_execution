package com.org.faveo.Base;

import com.org.faveo.Base.BaseClass;

public class TestCaseFn extends BaseClass
{

	public static void TestCaseName(String TestCaseSheet,int Rownum, String ProductName) throws Exception
	{
		String[][] TestCase = BaseClass.excel_Files(TestCaseSheet);
		String TestCaseName = (TestCase[Rownum][0].toString().trim() + " - " + TestCase[Rownum][1].toString().trim());
		logger = extent.startTest(ProductName + TestCaseName);
		System.out.println(ProductName + TestCaseName);
	}
	
}
