package com.org.faveo.Base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class HealthInsuranceDropDown extends BaseClass implements AccountnSettingsInterface {
	
	public static void CareWithNCBPolicy()
	{
		
		// Click on Health Insurance
					try 
					{
						Fluentwait1(By.xpath(Health_Insurance_xppath));
						Thread.sleep(5000);
						clickElement(By.xpath(Health_Insurance_xppath));
					} catch (Exception e) 
					{
						Fluentwait1(By.xpath(Health_Insurance_xppath));
						clickElement(By.xpath(Health_Insurance_xppath));
/*						logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");*/
					}
					try {
						waitForElements(By.xpath(care_xpath));
						mousehover(By.xpath(care_xpath));
					} catch (Exception e) {
						waitForElements(By.xpath(care_xpath));
						mousehover(By.xpath(care_xpath));
/*						logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
*/					}
					
					try 
					{
						waitForElements(By.xpath(CareWithNCB_xpath));
						clickElement(By.xpath(CareWithNCB_xpath));
					} 
					catch (Exception e) {
						logger.log(LogStatus.FAIL, "Unable to Click on Care (with NCB) listed in Care Product");
					}
	}
	
	public static void SuperSaverPolicy(){
		
		// Click on Health Insurance
		try {
			Fluentwait1(By.xpath(Health_Insurance_xppath));
			clickElement(By.xpath(Health_Insurance_xppath));
		} catch (Exception e) {
			System.out.println("Unable to Click on Health Insurance Dropdown from Dashboard");
			//logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");
		}
		try {
			waitForElements(By.xpath(care_xpath));
			mousehover(By.xpath(care_xpath));
		} catch (Exception e) {
			//logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
		}
		try {
			waitForElements(By.xpath(CareSuperSaver_xpath));
			clickElement(By.xpath(CareSuperSaver_xpath));
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Unable to Click on Care (with NCB) listed in Care Product");
		}
	}
	
	public static void CareSmartSelectPolicy()
	{
		//Click on Health Insurance
				try{
					Fluentwait1(By.xpath(Health_Insurance_xppath));
				clickElement(By.xpath(Health_Insurance_xppath));
				}
				catch(Exception e){
					logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");
				}
				try{
				waitForElements(By.xpath(care_xpath));
				mousehover(By.xpath(care_xpath));
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
				}
				try{
				waitForElements(By.xpath(CareSmartSelect_xpath));
				clickElement(By.xpath(CareSmartSelect_xpath));
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Unable to Click on Care (with NCB) listed in Care Product");
				}
	}
	
	public static void CareGlobalPolicy()
	{
		//Click on Health Insurance
				try{
				waitForElement(By.xpath(Health_Insurance_xppath));
				clickElement(By.xpath(Health_Insurance_xppath));
				}
				catch(Exception e){
					logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");
				}
				try{
				Fluentwait(By.xpath(care_xpath));
				mousehover(By.xpath(care_xpath));
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
				}
				try{
				waitForElements(By.xpath(CareGlobal_xpath));
				clickElement(By.xpath(CareGlobal_xpath));
				}
				catch(Exception e)
				{
					System.out.println("Unable to Click on Care Global listed in Care Product");
					//logger.log(LogStatus.FAIL, "Unable to Click on Care Global listed in Care Product");
				}
	}

	public static void CareFreedomPolicy(){
		//Click on Health Insurance
		try{
			Fluentwait1(By.xpath(Health_Insurance_xppath));
		clickElement(By.xpath(Health_Insurance_xppath));
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");
		}
		try{
		waitForElements(By.xpath(care_xpath));
		mousehover(By.xpath(care_xpath));
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
		}
		try{
		waitForElements(By.xpath(CareFreedom_xpath));
		clickElement(By.xpath(CareFreedom_xpath));
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Unable to Click on Care Freedom listed in Care Product");
		}

	}
	
	public static void CareHNIPolicy(){
		//Click on Health Insurance
				try{
					Fluentwait1(By.xpath(Health_Insurance_xppath));
				clickElement(By.xpath(Health_Insurance_xppath));
				}
				catch(Exception e){
					logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");
				}
				try{
				waitForElements(By.xpath(care_xpath));
				mousehover(By.xpath(care_xpath));
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
				}
				try{
				waitForElements(By.xpath(CareHNI_xpath));
				clickElement(By.xpath(CareHNI_xpath));
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Unable to Click on Care HNI listed in Care Product");
				}
	}
	
	public static void AssureDropDown(){
		try{
		clickElement(By.xpath(FixedBenefitInsurance_xpath));
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, "Unable to Click on Fixed Benefit Insurance from Dashboard");
		}
		
		try{
			waitForElements(By.xpath(Assure_xpath));
			clickElement(By.xpath(Assure_xpath));
		}catch(Exception e){
			logger.log(LogStatus.FAIL, "Unable to Click on Assure listed in Fixed Benefit Insurance");
		}
	}
	
	
	public static void SecureDropDown(){
		try{
			Fluentwait1(By.xpath(FixedBenefitInsurance_xpath));
		clickElement(By.xpath(FixedBenefitInsurance_xpath));
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, "Unable to Click on Fixed Benefit Insurance from Dashboard");
		}
		
		try{
			waitForElements(By.xpath(Secure_xpath));
			clickElement(By.xpath(Secure_xpath));
		}catch(Exception e){
			logger.log(LogStatus.FAIL, "Unable to Click on Assure listed in Fixed Benefit Insurance");
		}
	}
	
	
	public static void EnhanceDropDown()
	{
		try{
			Thread.sleep(6000);
			Fluentwait1(By.xpath(Health_Insurance_xppath));
		clickElement(By.xpath(Health_Insurance_xppath));
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance from Dashboard");
		}
		
		try{
			waitForElements(By.xpath(Enhance_xpath));
			clickElement(By.xpath(Enhance_xpath));
		}catch(Exception e){
			logger.log(LogStatus.FAIL, "Unable to Click on Enhance listed in Health Insurance");
		}
	}
	
	public static void PosCareWithNCBPolicy()
	{
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Click on Health Insurance
					try {
						Fluentwait1(By.xpath(Health_Insurance_xppath));
						clickElement(By.xpath(Health_Insurance_xppath));
					} catch (Exception e) {
						logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");
					}
					try {
						Fluentwait(By.xpath(care_xpath));
						//waitForElements(By.xpath(care_xpath));
						mousehover(By.xpath(care_xpath));
					} catch (Exception e) {
						logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
					}
					try {
						Fluentwait(By.xpath(Pos_care_with_NCB_xpath));
						//waitForElements(By.xpath(care_with_NCB_xpath));
						clickElement(By.xpath(Pos_care_with_NCB_xpath));
					} catch (Exception e) {
						logger.log(LogStatus.FAIL, "Unable to Click on PosCare (with NCB) listed in Care Product");
					}
	}
	
	public static void PosCareFreedom()
	{
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Click on Health Insurance
					try {
						Fluentwait1(By.xpath(Health_Insurance_xppath));
						clickElement(By.xpath(Health_Insurance_xppath));
					} catch (Exception e) {
						logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");
					}
					try {
						Fluentwait(By.xpath(care_xpath));
						//waitForElements(By.xpath(care_xpath));
						mousehover(By.xpath(care_xpath));
					} catch (Exception e) {
						logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
					}
					try {
						Fluentwait(By.xpath(PosCare_Freedom_xpath));
						//waitForElements(By.xpath(care_with_NCB_xpath));
						clickElement(By.xpath(PosCare_Freedom_xpath));
					} catch (Exception e) {
						logger.log(LogStatus.FAIL, "Unable to Click on PosCare (with NCB) listed in Care Product");
					}
	}
	

public static void CareSenior(){
		
		// Click on Health Insurance
		try {
			Fluentwait1(By.xpath(Health_Insurance_xppath));
			clickElement(By.xpath(Health_Insurance_xppath));
		} catch (Exception e) {
			System.out.println("Unable to Click on Health Insurance Dropdown from Dashboard");
			//logger.log(LogStatus.FAIL, "Unable to Click on Health Insurance Dropdown from Dashboard");
		}
		try {
			waitForElements(By.xpath(care_xpath));
			mousehover(By.xpath(care_xpath));
		} catch (Exception e) {
			//logger.log(LogStatus.FAIL, "Unable to Click on Care Product listed in Health Insurance Dropdown");
		}
		try {
			waitForElements(By.xpath(CareSenior_Xpath));
			clickElement(By.xpath(CareSenior_Xpath));
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Unable to Click on Care (with NCB) listed in Care Product");
		}
	}

//------------ products for Super Mediclaim Dropdown ---------------------------------

//Function for select Super Mediclaim with Cancer
	public static void superMediClaimWithCancer() {
     try{
		/*Fluentwait1(By.xpath(Health_Insurance_xppath));
		clickElement(By.xpath(Health_Insurance_xppath));
		logger.log(LogStatus.PASS, "Selected Health Insurance");
     }
     catch(Exception e){
    	 System.out.println("Unable to Click on Health Insurance Dropdown from Dashboard");
    	 logger.log(LogStatus.PASS, "Unable to Click on Health Insurance Dropdown from Dashboard");
     }
     try{
    	waitForElements(By.xpath(superMediClaim_Xpath));
		mousehover(By.xpath(superMediClaim_Xpath));
   
     }
     catch(Exception e){
    	 System.out.println("Unable to Select Super Medilaim");
     }
		try {
		waitForElements(By.xpath(superMediClaim_Cancer_Xpath));
		clickElement(By.xpath(superMediClaim_Cancer_Xpath));*/
        Fluentwait1(By.xpath(Health_Insurance_xppath));
 		clickElement(By.xpath(Health_Insurance_xppath));
 		logger.log(LogStatus.PASS, "Selected Health Insurance");
 		
 		waitForElements(By.xpath(superMediClaim_Xpath));
		mousehover(By.xpath(superMediClaim_Xpath));
		
		waitForElements(By.xpath(superMediClaim_Cancer_Xpath));
		clickElement(By.xpath(superMediClaim_Cancer_Xpath));
		logger.log(LogStatus.PASS, "Selected Super Mediclaim Cancer");
    	 
		} catch (Exception e) {
			//logger.log(LogStatus.FAIL, "Unable to Click on Care (with NCB) listed in Care Product");
			System.out.println("Unable to click on Super Mediclaim Cancer");
			logger.log(LogStatus.FAIL, "Unable to Select Super Mediclaim Cancer");
		}
		
	}

	
}
