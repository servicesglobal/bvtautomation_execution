package com.org.faveo.Base;

import org.openqa.selenium.By;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class TravelInsuranceDropDown extends BaseClass implements AccountnSettingsInterface{


		public static void ExploreTravelPolicy()
		{
			
			//Click on Explore Listed in Travel Insurance Dropdown
			try{
				/*clickbyHover(By.xpath(TravelInsurance_xpath_Dashboard));
				Fluentwait1(By.xpath(TravelInsurance_xpath_Dashboard));*/
				Thread.sleep(6000);
				clickElement(By.xpath(TravelInsurance_xpath_Dashboard));
				}
				catch(Exception e)
			{
					logger.log(LogStatus.FAIL, "Unable to Click on Travel Insurance from Dashboard");
				}
				
				try{
					waitForElements(By.xpath(Explore_xpath));
					clickElement(By.xpath(Explore_xpath));
				}catch(Exception e){
					/*logger.log(LogStatus.FAIL, "Unable to Click on Explore listed in Travel Insurance");*/
				}					
		
		}
		
	
}


